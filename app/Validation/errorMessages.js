export const isRequired = fieldName => `${fieldName} is required`;

export const validEmail = () => `Not a valid email`;

export const mustMatch = otherFieldName => fieldName =>
  `${fieldName} must match ${otherFieldName}`;

export const minLength = length => fieldName =>
  `${fieldName} must be at least ${length} characters`;

export const exactLength = length => fieldName =>
  `${fieldName} must be of ${length} characters`;

export const invalidColorCode = fieldName =>
  `${fieldName} is not a valid color code`;

export const invalidSocialUrl = fieldName => 'Not a valid url.';

export const spaceNotAllowed = fieldName => 'Spaces not allowed.';
