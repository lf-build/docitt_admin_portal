import validator from 'validator';
import * as ErrorMessages from './errorMessages';

export const required = text => {
  if (text && text.trim()) {
    return null;
  }
  return ErrorMessages.isRequired;
};

export const validEmail = text => {
  if (text && validator.isEmail(text)) {
    return null;
  }
  return ErrorMessages.validEmail;
};

export const mustMatch = (field, fieldName) => (text, state) =>
  state[field] === text ? null : ErrorMessages.mustMatch(fieldName);

export const minLength = length => text =>
  text.length >= length ? null : ErrorMessages.minLength(length);

export const exactLength = length => text =>
  text.trim().length === length ? null : ErrorMessages.exactLength(length);

export const validColorCode = text =>
  text.charAt(0) === '#' ? null : ErrorMessages.invalidColorCode;

export const validateUrl = url => {
  if (url.length > 0) {
    url.match(
      /(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/,
    );

    if (RegExp.$3.indexOf('youtu') > -1) {
      return null;
    }
    if (RegExp.$3.indexOf('vimeo') > -1) {
      return null;
    }
    return ErrorMessages.invalidSocialUrl;
  }

  return null;
};

export const checkSpace = text =>
  /\s/.test(text) ? ErrorMessages.spaceNotAllowed : null;
