/**
 * Created by sigma on 4/2/19.
 */
import {
  FETCH_LOGO,
  UPLOAD_LOGO,
  UPDATE_DEFAULT_LOGO,
  UPDATE_CURRENT_LOGO,
  UPDATE_DEFAULT_ASSETS,
  UPDATE_CURRENT_ASSETS,
  FETCH_IMAGES,
  UPDATE_FILES_ASSETS,
  DISABLE_BUTTON,
  ENABLE_BUTTON,
  REMOVE_BACKGROUND_IMAGE,
  ADD_NEWLY_UPLOADED_IMAGE,
  BACKGROUND_IMAGE_UPDATE,
  REMOVE_VIDEO_POSTER_IMAGE,
  VIDEO_POSTER_IMAGE_UPDATE,
  BACKGROUND_STATUS_UPDATE,
  VIDEO_STATUS_UPDATE,
  VIDEO_URL_UPDATE,
  SAVE_IMAGES,
  RESET_CURRENT_STATE,
  LOGO_REVERT_DEFAULT,
  IMAGE_REVERT_DEFAULT,
  UPDATE_DEFAULT_ICON,
  UPDATE_CURRENT_ICON,
  FETCH_ICONS,
  UPDATE_ALL_ICONS,
  SELECTED_ICONS,
  SAVE_ICONS,
  CLEAR_ICONS,
  REVERT_DEFAULT_ICON,
  API_ERROR,
} from './constants';

export function fetchAllLogos() {
  return {
    type: FETCH_LOGO,
  };
}

export function uploadLogo(logo, badge, favicon) {
  return {
    type: UPLOAD_LOGO,
    logoFile: logo,
    badgeFile: badge,
    faviconFile: favicon
  };
}

export function updateDefaultLogo(logo) {
  return {
    type: UPDATE_DEFAULT_LOGO,
    logo: logo
  };
}

export function updateDefaultAssets(assets) {
  return {
    type: UPDATE_DEFAULT_ASSETS,
    asset: assets
  };
}

export function updateDefaultIcon(icon) {
  return {
    type: UPDATE_DEFAULT_ICON,
    icon: icon
  };
}

export function updateCurrentLogo(logo) {
  return {
    type: UPDATE_CURRENT_LOGO,
    logo: logo
  };
}

export function updateCurrentAssets(assets) {
  return {
    type: UPDATE_CURRENT_ASSETS,
    asset: assets
  };
}

export function updateCurrentIcon(icons) {
  return {
    type: UPDATE_CURRENT_ICON,
    icon: icons
  };
}

export function addAllFilesInAssetManager(allFiles) {
  return {
    type: UPDATE_FILES_ASSETS,
    file: allFiles
  };
}

export function fetchAllImages() {
  return {
    type: FETCH_IMAGES
  };
}

export function disableButton() {
  return {
    type: DISABLE_BUTTON
  };
}

export function enableButton() {
  return {
    type: ENABLE_BUTTON
  };
}

export function removeBackgroundImage() {
  return {
    type: REMOVE_BACKGROUND_IMAGE
  };
}

export function addNewlyUploadedImage(uploadedFile) {
  return {
    type: ADD_NEWLY_UPLOADED_IMAGE,
    file: uploadedFile
  };
}

export function addBackgroundImage (file) {
  return {
    type: BACKGROUND_IMAGE_UPDATE,
    backgroundImage: file
  }
}

export function removeVideoPoster() {
  return {
    type: REMOVE_VIDEO_POSTER_IMAGE
  };
}

export function addVideoPosterImage(file) {
  return {
    type: VIDEO_POSTER_IMAGE_UPDATE,
    posterImage: file
  }
}

export function updateVideoUrl(link) {
  return {
    type: VIDEO_URL_UPDATE,
    videoLink: link
  }
}

export function changeBackgroundStatus() {
  return {
    type: BACKGROUND_STATUS_UPDATE
  }
}

export function changeVideoStatus() {
  return {
    type: VIDEO_STATUS_UPDATE
  }
}

export function saveImages() {
  return {
    type: SAVE_IMAGES
  }
}

export function clearAssets () {
  return {
    type: RESET_CURRENT_STATE
  }
}

export function apiFailure(err) {
  return {
    type: API_ERROR,
    error: err,
  };
}

export function revertLogoDefault () {
  return {
    type: LOGO_REVERT_DEFAULT
  }
}

export function revertImageDefault () {
  return {
    type: IMAGE_REVERT_DEFAULT
  }
}

export function fetchIcons () {
  return {
    type: FETCH_ICONS
  }
}

export function updateAllIcon (iconsList) {
  return {
    type: UPDATE_ALL_ICONS,
    icons: iconsList
  }
}

export function iconsChanged (icon) {
  return {
    type: SELECTED_ICONS,
    selectedIcon: icon
  }
}

export function saveIcons () {
  return {
    type: SAVE_ICONS
  }
}

export function clearIcons () {
  return {
    type: CLEAR_ICONS
  }
}

export function revertIconDefault () {
  return {
    type: REVERT_DEFAULT_ICON
  }
}