/**
 * Created by sigma on 4/2/19.
 */
import {fromJS} from 'immutable';
import {
  UPDATE_DEFAULT_LOGO,
  UPDATE_CURRENT_LOGO,
  UPDATE_DEFAULT_ASSETS,
  UPDATE_CURRENT_ASSETS,
  UPDATE_FILES_ASSETS,
  DISABLE_BUTTON,
  ENABLE_BUTTON,
  REMOVE_BACKGROUND_IMAGE,
  ADD_NEWLY_UPLOADED_IMAGE,
  BACKGROUND_IMAGE_UPDATE,
  REMOVE_VIDEO_POSTER_IMAGE,
  VIDEO_POSTER_IMAGE_UPDATE,
  BACKGROUND_STATUS_UPDATE,
  VIDEO_URL_UPDATE,
  VIDEO_STATUS_UPDATE,
  RESET_CURRENT_STATE,
  UPDATE_DEFAULT_ICON,
  UPDATE_CURRENT_ICON,
  UPDATE_ALL_ICONS,
  SELECTED_ICONS,
  CLEAR_ICONS,
  API_ERROR,
} from './constants';

export const initialState = fromJS({
  errorMessage: '',
  currentAppliedLogo: {},
  currentAppliedAssets: {},
  currentAppliedIcons: {},
  defaultLogo: {},
  defaultIcons: {},
  defaultAsset: {},
  allFiles: [],
  allIcon: [],
  illustration: [],
  videoPoster: [],
  videoUrl: "",
  changedIcon: {},
  videoEnabled: false,
  backgroundImageEnabled: false,
  buttonDisable: false,
});

function graphicReducer(state = initialState, action) {
  switch (action.type) {
    case API_ERROR:
      return state.setIn(['errorMessage'], fromJS(action.error));

    case UPDATE_FILES_ASSETS:
      return state.setIn(['allFiles'], action.file);

    case UPDATE_DEFAULT_LOGO:
      return state.setIn(['defaultLogo'], action.logo);

    case UPDATE_CURRENT_LOGO:
      return state.setIn(['currentAppliedLogo'], action.logo);

    case UPDATE_DEFAULT_ASSETS:
      return state.setIn(['defaultAsset'], action.asset);

    case UPDATE_CURRENT_ASSETS:
      return state.setIn(['currentAppliedAssets'], action.asset)
        .setIn(['illustration'], [{
          preview: action.asset.welcomeScreenBackgroundImageUrl,
          name: action.asset.welcomeScreenBackgroundImageName
        }])
        .setIn(['videoPoster'], [{
          preview: action.asset.welcomScreenImageUrl,
          name: action.asset.welcomScreenImageName
        }])
        .setIn(['videoUrl'], action.asset.welcomScreenVideoUrl)
        .setIn(['videoEnabled'], action.asset.isWelcomScreenVideoOn)
        .setIn(['backgroundImageEnabled'], action.asset.isWelcomeScreenBackgroundImageOn);

    case UPDATE_DEFAULT_ICON:
      return state.setIn(['defaultIcons'], action.icon);

    case UPDATE_CURRENT_ICON:
      return state.setIn(['currentAppliedIcons'], action.icon)
        .setIn(['changedIcon'], action.icon);

    case UPDATE_ALL_ICONS:
      return state.setIn(['allIcon'], action.icons);

    case ENABLE_BUTTON:
      return state.setIn(['buttonDisable'], false);

    case DISABLE_BUTTON:
      return state.setIn(['buttonDisable'], true);

    case REMOVE_BACKGROUND_IMAGE:
      return state.setIn(['illustration'], []);

    case BACKGROUND_IMAGE_UPDATE:
      return state.setIn(['illustration'], action.backgroundImage);

    case ADD_NEWLY_UPLOADED_IMAGE:
      const presentFiles = state.get('allFiles');
      presentFiles.push(action.file)
      return state.setIn(['allFiles'],
        presentFiles);

    case REMOVE_VIDEO_POSTER_IMAGE:
      return state.setIn(['videoPoster'], []);

    case VIDEO_POSTER_IMAGE_UPDATE:
      return state.setIn(['videoPoster'], action.posterImage);

    case VIDEO_URL_UPDATE:
      return state.setIn(['videoUrl'], action.videoLink);

    case BACKGROUND_STATUS_UPDATE:
      return state.setIn(['backgroundImageEnabled'], !state.getIn(['backgroundImageEnabled']));

    case VIDEO_STATUS_UPDATE:
      return state.setIn(['videoEnabled'], !state.getIn(['videoEnabled']));

    case RESET_CURRENT_STATE:
      const currentAsset = state.getIn(['currentAppliedAssets']);
      return state.setIn(['illustration'], [{
        preview: currentAsset.welcomeScreenBackgroundImageUrl,
        name: currentAsset.welcomeScreenBackgroundImageName,
      }])
        .setIn(['videoPoster'], [{
          preview: currentAsset.welcomScreenImageUrl,
          name: currentAsset.welcomScreenImageName,
        }])
        .setIn(['videoUrl'], currentAsset.welcomScreenVideoUrl)
        .setIn(['videoEnabled'], currentAsset.isWelcomScreenVideoOn)
        .setIn(['backgroundImageEnabled'], currentAsset.isWelcomeScreenBackgroundImageOn);

    case SELECTED_ICONS:
      return state.setIn(['changedIcon'], state.get('allIcon').find(icon => icon.name === action.selectedIcon));

    case CLEAR_ICONS:
      return state.setIn(['changedIcon'], state.get('currentAppliedIcons'));

    default:
      return state;
  }
}

export default graphicReducer;