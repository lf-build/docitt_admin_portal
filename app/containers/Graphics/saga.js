/**
 * Created by sigma on 4/2/19.
 */

import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { getRequest, postRequest, fileUploadRequest } from 'utils/request';
import { showMessageSuccess, showErrorMessage } from 'components/ErrorMessage';
import {
  FETCH_LOGO,
  UPLOAD_LOGO,
  FETCH_IMAGES,
  SAVE_IMAGES,
  LOGO_REVERT_DEFAULT,
  IMAGE_REVERT_DEFAULT,
  FETCH_ICONS,
  SAVE_ICONS,
  REVERT_DEFAULT_ICON,
} from './constants';
import {
  apiFailure,
  updateDefaultLogo,
  updateCurrentLogo,
  updateDefaultAssets,
  updateCurrentAssets,
  updateDefaultIcon,
  updateCurrentIcon,
  addAllFilesInAssetManager,
  disableButton,
  enableButton,
  addNewlyUploadedImage,
  updateAllIcon,
} from './actions';
import {
  defaultLogo,
  currentLogo,
  currentAppliedAssets,
  illustration,
  videoPoster,
  videoUrl,
  videoEnabled,
  backgroundImageEnabled,
  defaultAsset,
  changedIcon,
  defaultIcons,
} from './selectors';

export function* fetchAllFiles() {
  const base = yield select(serviceUrl('assetManager'));
  const url = '/theme/css';

  try {
    const allFiles = yield call(getRequest, base, url);
    yield put(addAllFilesInAssetManager(allFiles));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchDefaultLogo() {
  const base = yield select(serviceUrl('theme'));
  const url = '/logo/default';

  try {
    const logoDefault = yield call(getRequest, base, url);
    yield put(updateDefaultLogo(logoDefault));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchDefaultIcons() {
  const base = yield select(serviceUrl('theme'));
  const url = '/iconstyle/default';

  try {
    const assetDefault = yield call(getRequest, base, url);
    yield put(updateDefaultIcon(assetDefault));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchDefaultAssets() {
  const base = yield select(serviceUrl('theme'));
  const url = '/asset/default';

  try {
    const assetsDefault = yield call(getRequest, base, url);
    yield put(updateDefaultAssets(assetsDefault));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchCurrentLogo() {
  const base = yield select(serviceUrl('theme'));
  const url = '/logo/current';

  try {
    const logoCurrent = yield call(getRequest, base, url);
    yield put(updateCurrentLogo(logoCurrent));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchCurrentAssets() {
  const base = yield select(serviceUrl('theme'));
  const url = '/asset/current';

  try {
    const assetsCurrent = yield call(getRequest, base, url);
    yield put(updateCurrentAssets(assetsCurrent));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchCurrentIcons() {
  const base = yield select(serviceUrl('theme'));
  const url = '/iconstyle/current';

  try {
    const iconCurrent = yield call(getRequest, base, url);
    yield put(updateCurrentIcon(iconCurrent));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* fetchAllIcons() {
  const base = yield select(serviceUrl('theme'));
  const url = '/iconstyle/all';

  try {
    const iconCurrent = yield call(getRequest, base, url);
    yield put(updateAllIcon(iconCurrent));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* uploadLogo(payload) {
  yield put(disableButton());
  const assetManagerBase = yield select(serviceUrl('assetManager'));
  const themeUrlBase = yield select(serviceUrl('theme'));
  const url = '/theme/css';
  const themeAddLogoUrl = '/logo';
  const generateCssUrl = '/theme/generate/css';

  const currentLogoDetails = yield select(currentLogo());

  let logoFileName;
  let badgeFileName;
  let faviconFileName = '';
  try {
    if (payload.logoFile) {
      const logoFileUpload = yield call(
        fileUploadRequest,
        assetManagerBase,
        url,
        payload.logoFile,
      );
      yield put(addNewlyUploadedImage(logoFileUpload));
      logoFileName = logoFileUpload.fileName;
    } else {
      logoFileName = currentLogoDetails.logoName;
    }

    if (payload.badgeFile) {
      const badgeFileUpload = yield call(
        fileUploadRequest,
        assetManagerBase,
        url,
        payload.badgeFile,
      );
      yield put(addNewlyUploadedImage(badgeFileUpload));
      badgeFileName = badgeFileUpload.fileName;
    } else {
      badgeFileName = currentLogoDetails.badgeLogoName;
    }

    if (payload.faviconFile) {
      const faviconFileUpload = yield call(
        fileUploadRequest,
        assetManagerBase,
        url,
        payload.faviconFile,
      );
      yield put(addNewlyUploadedImage(faviconFileUpload));
      faviconFileName = faviconFileUpload.fileName;
    } else {
      faviconFileName = currentLogoDetails.favIconName;
    }

    const newLogoPayload = {
      logoName: logoFileName,
      badgeLogoName: badgeFileName,
      favIconName: faviconFileName,
      isDefault: false,
      isApplied: true,
    };

    const newCurrentLogo = yield call(
      postRequest,
      themeUrlBase,
      themeAddLogoUrl,
      newLogoPayload,
    );
    yield put(updateCurrentLogo(newCurrentLogo));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Logos saved and applied successfully.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export function* revertLogoDefault() {
  yield put(disableButton());
  const themeUrlBase = yield select(serviceUrl('theme'));
  const themeAddLogoUrl = '/logo';
  const generateCssUrl = '/theme/generate/css';

  const defaultLogoDetails = yield select(defaultLogo());
  try {
    const newLogoPayload = {
      logoName: defaultLogoDetails.logoName,
      badgeLogoName: defaultLogoDetails.badgeLogoName,
      favIconName: defaultLogoDetails.favIconName,
      isDefault: true,
      isApplied: true,
    };

    const newCurrentLogo = yield call(
      postRequest,
      themeUrlBase,
      themeAddLogoUrl,
      newLogoPayload,
    );
    yield put(updateCurrentLogo(newCurrentLogo));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Logos reverted to default.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export function* uploadImages() {
  yield put(disableButton());
  const assetManagerBase = yield select(serviceUrl('assetManager'));
  const themeUrlBase = yield select(serviceUrl('theme'));
  const url = '/theme/css';
  const themeAddAssetUrl = '/asset';
  const generateCssUrl = '/theme/generate/css';

  const currentAssetDetails = yield select(currentAppliedAssets());

  const illustrationImage = yield select(illustration());
  const illustrationImageName = illustrationImage[0].name;

  const videoImage = yield select(videoPoster());
  const videoImageName = videoImage[0].name;

  const enteredVideoUrl = yield select(videoUrl());

  const videoEnabledStatus = yield select(videoEnabled());

  const backgroundImageEnabledStatus = yield select(backgroundImageEnabled());

  try {
    if (
      currentAssetDetails.welcomeScreenBackgroundImageName !==
      illustrationImageName
    ) {
      const BackgroundFileUpload = yield call(
        fileUploadRequest,
        assetManagerBase,
        url,
        illustrationImage[0],
      );
      yield put(addNewlyUploadedImage(BackgroundFileUpload));
    }

    if (currentAssetDetails.welcomScreenImageName !== videoImageName) {
      const videoFileUpload = yield call(
        fileUploadRequest,
        assetManagerBase,
        url,
        videoImage[0],
      );
      yield put(addNewlyUploadedImage(videoFileUpload));
    }

    const newLogoPayload = {
      welcomeScreenBackgroundImageName: illustrationImageName,
      isWelcomeScreenBackgroundImageOn: backgroundImageEnabledStatus,
      welcomScreenVideoUrl: enteredVideoUrl,
      isWelcomScreenVideoOn: videoEnabledStatus,
      welcomScreenImageName: videoImageName,
      isDefault: false,
      isApplied: true,
    };

    const newCurrentAsset = yield call(
      postRequest,
      themeUrlBase,
      themeAddAssetUrl,
      newLogoPayload,
    );
    yield put(updateCurrentAssets(newCurrentAsset));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Images saved and applied successfully.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export function* revertImageDefault() {
  yield put(disableButton());
  const themeUrlBase = yield select(serviceUrl('theme'));
  const themeAddAssetUrl = '/asset';
  const generateCssUrl = '/theme/generate/css';

  const defaultAssetDetails = yield select(defaultAsset());
  try {
    const newAssetPayload = {
      welcomeScreenBackgroundImageName:
        defaultAssetDetails.welcomeScreenBackgroundImageName,
      isWelcomeScreenBackgroundImageOn:
        defaultAssetDetails.isWelcomeScreenBackgroundImageOn,
      welcomScreenVideoUrl: defaultAssetDetails.welcomScreenVideoUrl,
      isWelcomScreenVideoOn: defaultAssetDetails.isWelcomScreenVideoOn,
      welcomScreenImageName: defaultAssetDetails.welcomScreenImageName,
      isDefault: true,
      isApplied: true,
    };

    const newCurrentAsset = yield call(
      postRequest,
      themeUrlBase,
      themeAddAssetUrl,
      newAssetPayload,
    );
    yield put(updateCurrentAssets(newCurrentAsset));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Images reverted to default.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export function* saveIcon() {
  yield put(disableButton());
  const themeUrlBase = yield select(serviceUrl('theme'));
  const themeAddLogoUrl = '/iconstyle';
  const generateCssUrl = '/theme/generate/css';

  const changedIcons = yield select(changedIcon());
  try {
    const newIconPayload = {
      name: changedIcons.name,
      isDefault: changedIcons.isDefault,
      isApplied: true,
    };

    const newCurrentIcon = yield call(
      postRequest,
      themeUrlBase,
      themeAddLogoUrl,
      newIconPayload,
    );
    yield put(updateCurrentIcon(newCurrentIcon));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Icons saved and applied successfully.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export function* revertDefaultIcon() {
  yield put(disableButton());
  const themeUrlBase = yield select(serviceUrl('theme'));
  const themeAddLogoUrl = '/iconstyle';
  const generateCssUrl = '/theme/generate/css';

  const defaultIcon = yield select(defaultIcons());
  try {
    const newIconPayload = {
      name: defaultIcon.name,
      isDefault: true,
      isApplied: true,
    };

    const newCurrentIcon = yield call(
      postRequest,
      themeUrlBase,
      themeAddLogoUrl,
      newIconPayload,
    );
    yield put(updateCurrentIcon(newCurrentIcon));
    yield call(postRequest, themeUrlBase, generateCssUrl, {});
    yield put(enableButton());
    yield call(showMessageSuccess, 'Icons reverted to default.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(enableButton());
    yield put(apiFailure(err));
  }
}

export default function* makeApiCalls() {
  yield all([
    takeLatest(FETCH_LOGO, fetchDefaultLogo),
    takeLatest(FETCH_LOGO, fetchCurrentLogo),
    takeLatest(FETCH_LOGO, fetchAllFiles),
    takeLatest(UPLOAD_LOGO, uploadLogo),
    takeLatest(FETCH_IMAGES, fetchDefaultAssets),
    takeLatest(FETCH_IMAGES, fetchCurrentAssets),
    takeLatest(SAVE_IMAGES, uploadImages),
    takeLatest(LOGO_REVERT_DEFAULT, revertLogoDefault),
    takeLatest(IMAGE_REVERT_DEFAULT, revertImageDefault),
    takeLatest(FETCH_ICONS, fetchDefaultIcons),
    takeLatest(FETCH_ICONS, fetchCurrentIcons),
    takeLatest(FETCH_ICONS, fetchAllIcons),
    takeLatest(SAVE_ICONS, saveIcon),
    takeLatest(REVERT_DEFAULT_ICON, revertDefaultIcon),
  ]);
}
