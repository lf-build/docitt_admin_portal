/**
 * Created by sigma on 15/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { showErrorMessage } from 'components/ErrorMessage';

import './graphic.css';
import '@trendmicro/react-radio/dist/react-radio.min.css';

import FileUploader from 'components/FileUploader';
import Tabs from 'components/Tabs';
import Pane from 'components/Pane';
import Tooltip from 'components/ToolTip';
import Button from 'components/Button';
import Switch from 'components/SwitchView';
import TextView from 'components/TextView';
import { run, ruleRunner } from 'Validation/ruleRunner';
import { validateUrl } from 'Validation/rules';
import { RadioButton, RadioGroup } from '@trendmicro/react-radio';
import saga from './saga';
import reducer from './reducer';
import {
  fetchAllLogos,
  uploadLogo,
  fetchAllImages,
  removeBackgroundImage,
  addBackgroundImage,
  removeVideoPoster,
  addVideoPosterImage,
  updateVideoUrl,
  disableButton,
  enableButton,
  changeBackgroundStatus,
  changeVideoStatus,
  saveImages,
  clearAssets,
  revertLogoDefault,
  revertIconDefault,
  revertImageDefault,
  fetchIcons,
  iconsChanged,
  saveIcons,
  clearIcons,
} from './actions';
import {
  uploadedFiles,
  buttonStatus,
  illustration,
  videoPoster,
  videoUrl,
  videoEnabled,
  backgroundImageEnabled,
  currentAppliedAssets,
  currentLogo,
  currentAppliedIcons,
  changedIcon,
  allIcons,
} from './selectors';

const fieldValidations = [
  ruleRunner('videoUrl', 'Not a valid url.', validateUrl),
];

class Graphics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logo: [],
      badge: [],
      favicon: [],
      openBackgroundDialog: false,
      openWelcomeVideoDialog: false,
      errorMessage: '',
      showErrors: false,
      validationErrors: {},
    };
  }

  componentDidMount() {
    this.props.fetchAllLogos();
    this.props.fetchAllImages();
    this.props.fetchIcons();
  }

  checkIfFilePresent = fileName =>
    this.props.uploadedFiles.findIndex(file => file.fileName === fileName);

  createUniqueFileName = fileName => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return `${s4() +
      s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}${fileName.substring(
      fileName.lastIndexOf('.'),
    )}`;
  };

  fileUploadHandler = files => {
    const checkPresent = this.checkIfFilePresent(files[0].name);

    if (checkPresent === -1) {
      const name = this.createUniqueFileName(files[0].name);
      // Instantiate copy of file, giving it new name.
      files[0] = new File([files[0]], name, { type: files[0].type });

      this.setState({
        logo: files.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      });
    } else {
      showErrorMessage(`File with name ${files[0].name} already exist.`);
    }
  };

  removeFileHandler = fileIndex => {
    const logo = [...this.state.logo];
    logo.splice(fileIndex, 1);
    this.setState({ logo });
  };

  fileBadgeUploadHandler = files => {
    const checkPresent = this.checkIfFilePresent(files[0].name);
    if (checkPresent === -1) {
      const name = this.createUniqueFileName(files[0].name);
      // Instantiate copy of file, giving it new name.
      files[0] = new File([files[0]], name, { type: files[0].type });

      this.setState({
        badge: files.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      });
    } else {
      showErrorMessage(`File with name ${files[0].name} already exist.`);
    }
  };

  removeBadgeFileHandler = fileIndex => {
    const badge = [...this.state.badge];
    badge.splice(fileIndex, 1);
    this.setState({ badge });
  };

  fileFaviconUploadHandler = files => {
    const checkPresent = this.checkIfFilePresent(files[0].name);
    if (checkPresent === -1) {
      const name = this.createUniqueFileName(files[0].name);
      // Instantiate copy of file, giving it new name.
      files[0] = new File([files[0]], name, { type: files[0].type });

      this.setState({
        favicon: files.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      });
    } else {
      showErrorMessage(`File with name ${files[0].name} already exist.`);
    }
  };

  removeFaviconFileHandler = fileIndex => {
    const favicon = [...this.state.favicon];
    favicon.splice(fileIndex, 1);
    this.setState({ favicon });
  };

  fileBackgroundUploadHandler = files => {
    const checkPresent = this.checkIfFilePresent(files[0].name);
    if (checkPresent === -1) {
      const name = this.createUniqueFileName(files[0].name);
      // Instantiate copy of file, giving it new name.
      files[0] = new File([files[0]], name, { type: files[0].type });

      const updatedPayload = files.map(file =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        }),
      );
      this.props.addBackgroundImage(updatedPayload);
    } else {
      showErrorMessage(`File with name ${files[0].name} already exist.`);
    }
  };

  removeBackgroundFileHandler = () => {
    this.props.removeBackgroundImage();
  };

  fileVideoPosterUploadHandler = files => {
    const checkPresent = this.checkIfFilePresent(files[0].name);
    if (checkPresent === -1) {
      const name = this.createUniqueFileName(files[0].name);
      // Instantiate copy of file, giving it new name.
      files[0] = new File([files[0]], name, { type: files[0].type });

      const updatedPayload = files.map(file =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        }),
      );
      this.props.addVideoPosterImage(updatedPayload);
    } else {
      showErrorMessage(`File with name ${files[0].name} already exist.`);
    }
  };

  removeVideoPosterHandler = () => {
    this.props.removeVideoPoster();
  };

  clearLogo = clearFor => {
    if (this.state[clearFor].length) {
      this.setState({ [clearFor]: [] });
    }
  };

  clearLogos = () => {
    const logoList = ['logo', 'badge', 'favicon'];
    logoList.map(logoType => this.clearLogo(logoType));
  };

  triggerOpenDialog = dialogFor => {
    this.refs[dialogFor].showDialog();
  };

  handleFieldChanged = field => e => {
    this.props.updateVideoUrl(e.target.value);
    this.validateSocialLink(field, e.target.value);
  };

  validateSocialLink = (field, value) => {
    const fieldValues = {
      videoUrl: value,
    };
    this.state.validationErrors = run(fieldValues, fieldValidations);
    if (
      (Object.keys(this.state.validationErrors).length === 0 &&
        this.state.validationErrors.constructor === Object) === false
    ) {
      this.setState({ showErrors: true });
      this.props.disableButton();
    } else {
      this.setState({ showErrors: false });
      this.props.enableButton();
    }
  };

  errorFor = field => this.state.validationErrors[field] || '';

  backgroundImageStatusChange = () => {
    this.props.changeBackgroundStatus();
  };

  videoStatusChanged = () => {
    this.props.changeVideoStatus();
  };

  saveLogoSubmit = () => {
    if (
      this.state.logo.length > 0 ||
      this.state.badge.length > 0 ||
      this.state.favicon.length > 0
    ) {
      this.props.uploadLogo(
        this.state.logo[0],
        this.state.badge[0],
        this.state.favicon[0],
      );
    }
  };

  saveImageSubmit = () => {
    if (
      this.props.currentAppliedAssets.welcomeScreenBackgroundImageUrl !=
        this.props.illustration[0].preview ||
      this.props.currentAppliedAssets.welcomScreenImageUrl !=
        this.props.videoPoster[0].preview ||
      this.props.currentAppliedAssets.welcomScreenVideoUrl !=
        this.props.videoUrl ||
      this.props.currentAppliedAssets.isWelcomScreenVideoOn !=
        this.props.videoEnabled ||
      this.props.currentAppliedAssets.isWelcomeScreenBackgroundImageOn !=
        this.props.backgroundImageEnabled
    ) {
      this.props.saveImages();
    }
  };

  resetImage = () => {
    this.props.clearAssets();
  };

  revertDefaultImage = () => {
    if (!this.props.currentAppliedAssets.isDefault) {
      this.props.revertImageDefault();
    }
  };

  revertDefaultLogo = () => {
    if (!this.props.currentLogo.isDefault) {
      this.props.revertLogoDefault();
    }
  };

  revertDefaultIcon = () => {
    if (!this.props.currentAppliedIcons.isDefault) {
      this.props.revertIconDefault();
    }
  };

  onIconChange = icon => {
    this.props.iconsChanged(icon);
  };

  resetIcon = () => {
    this.props.clearIcons();
  };

  saveIconSubmit = icon => {
    this.props.saveIcons();
  };

  render() {
    const {
      buttonStatus,
      illustration,
      videoPoster,
      videoUrl,
      videoEnabled,
      backgroundImageEnabled,
      currentAppliedAssets,
      currentLogo,
      allIcons,
      currentAppliedIcons,
      changedIcon,
    } = this.props;
    return (
      <div className="dashboard clearfix">
        <div className="headertitle">Graphics</div>
        <div className="padd clearfix">
          <div className="revert clearfix">
            <p className="pull-left">Upload brand logos, images and icons</p>
          </div>

          <div className="graphics clearfix">
            <div className="clmnL">
              <h3>Custom</h3>
            </div>
            <div className="clmnR">
              <h3>Details</h3>
            </div>
            <Tabs selected={0}>
              <Pane label="Logos">
                <div>
                  <div className="pull-right revert-to-default">
                    <span className="blue ">Default</span>
                    <Switch
                      valueChanged={this.revertDefaultLogo}
                      value={currentLogo.isDefault}
                    />
                  </div>
                  <div className="clearfix">
                    <div className="clmna">
                      <div className="type-title">
                        Logo Type
                        <Tooltip
                          position="top"
                          message="The tenant logo which appears on the Header panel."
                        >
                          <span className="df df-info" />
                        </Tooltip>
                      </div>
                      <div className="upload">
                        <FileUploader
                          allowedFileType=".svg"
                          files={this.state.logo}
                          onFileUploadHandler={this.fileUploadHandler}
                          onRemoveFileHandler={this.removeFileHandler}
                        >
                          <span className="df df-upload"></span></FileUploader></div>
                      <small>
                        300 x 600 pixels (SVG)
                        <br />
                        Max Size 4 Mb.
                      </small>
                      <span
                        onClick={() => this.clearLogo('logo')}
                        className="df df-trash pull-right"
                      />
                    </div>
                    <div className="clmnb">
                      <div className="type-title">
                        Badge Logo
                        <Tooltip
                          position="top"
                          message="Tenant Icon that appears on the push notifications, specifically on mobile and tablets, or affiliate network."
                        >
                          <span className="df df-info" />
                        </Tooltip>
                      </div>
                      <div className="upload">
                        <FileUploader
                          allowedFileType=".svg"
                          files={this.state.badge}
                          onFileUploadHandler={this.fileBadgeUploadHandler}
                          onRemoveFileHandler={this.removeBadgeFileHandler}
                        >
                          <span className="df df-upload"></span></FileUploader>
                      </div>
                      <small>
                        100 x 300 pixels (SVG)
                        <br />
                        Max Size 4 Mb.
                      </small>
                      <span
                        onClick={() => this.clearLogo('badge')}
                        className="df df-trash pull-right"
                      />
                    </div>
                    <div className="clmnc">
                      <div className="type-title">
                        Favicon
                        <Tooltip
                          position="top"
                          message="Icon of the tenant on the address bar."
                        >
                          <span className="df df-info" />
                        </Tooltip>
                      </div>
                      <div className="upload">
                        <FileUploader
                          allowedFileType=".ico"
                          files={this.state.favicon}
                          onFileUploadHandler={this.fileFaviconUploadHandler}
                          onRemoveFileHandler={this.removeFaviconFileHandler}
                        >
                          <span className="df df-upload"></span></FileUploader>
                      </div>
                      <small>
                        100 x 100 pixels (ICO)
                        <br />
                        Max Size 2 Mb.
                      </small>
                      <span
                        onClick={() => this.clearLogo('favicon')}
                        className="df df-trash pull-right"
                      />
                    </div>
                  </div>
                  <div className="btnpreview mtop40 text-right">
                    <Button
                      type="button"
                      buttonSize="btn-sm"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.clearLogos}
                      text="CLEAR"
                    />
                    <Button
                      type="button"
                      buttonSize="btn-sm btn-primary"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.saveLogoSubmit}
                      text="SAVE"
                    />
                  </div>
                </div>
              </Pane>

              <Pane label="Images">
                <div className="images">
                  <div className="pull-right revert-to-default">
                    <span className="blue ">Default</span>
                    <Switch
                      name="imageRevert"
                      valueChanged={this.revertDefaultImage}
                      value={currentAppliedAssets.isDefault}
                    />
                  </div>
                  <div className="clearfix">
                    <div className="col-lg-10 col-md-12 col-sm-12 col-xs-12 no-pad">
                      <div className="type-title">
                        Welcome Screen (Background)
                        <Tooltip
                          position="top"
                          message="Welcome screen Background image."
                        >
                          <span className="df df-info" />
                        </Tooltip>
                      </div>
                      <table width="100%" cellPadding="0">
                        <tbody>
                          <tr>
                            <td className="upload" valign="bottom">
                              <FileUploader
                              maxFileSize={6000000}
                              openDialog={this.state.openBackgroundDialog}
                                allowedFileType=".jpeg, .jpg, .png, .svg"
                                files={illustration}
                                onFileUploadHandler={
                                  this.fileBackgroundUploadHandler
                                }
                                onRemoveFileHandler={
                                  this.removeBackgroundFileHandler
                                }
                                ref="openBackgroundDialog"
                              >
                                <span className="df df-upload" />
                              </FileUploader>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <small className="pull-left clearfix">
                        1300 x 600 pixels (PNG, JPEG, SVG)
                        <br />
                        Max Size 6 Mb.
                      </small>
                      <div className="pull-right">
                        Illustration off &nbsp;
                        <div className="onoffswitch">
                          <Switch
                            name="backgroundSwitch"
                            valueChanged={this.backgroundImageStatusChange}
                            value={backgroundImageEnabled}
                          />
                        </div>
                        <span className="vline">&nbsp;</span> Replace &nbsp;<span
                        onClick={() => this.triggerOpenDialog('openBackgroundDialog')}
                        className="df df-upload"></span><span
                        className="vline">&nbsp;</span><span
                        onClick={this.removeBackgroundFileHandler}
                        className="df df-trash"></span>
                      </div>
                    </div>
                    <div className="spacer">&nbsp;</div>
                    <div className="col-lg-6 col-md-7 col-sm-12 col-xs-12 no-pad">
                      <div className="type-title">
                        Welcome Screen (Video)
                        <Tooltip position="top" message="Welcome screen video.">
                          <span className="df df-info" />
                        </Tooltip>
                      </div>
                      <div className="video-block upload clearfix">
                        <FileUploader
                          maxFileSize={6000000}
                          openDialog={this.state.openWelcomeVideoDialog}
                          allowedFileType=".jpeg, .jpg, .png, .svg"
                          files={videoPoster}
                          onFileUploadHandler={
                            this.fileVideoPosterUploadHandler
                          }
                          onRemoveFileHandler={this.removeVideoPosterHandler}
                          ref="openWelcomeVideoDialog"
                        >
                          <span className="df df-upload" />
                        </FileUploader>
                      </div>
                      <div className="pull-right">
                        Video off&nbsp;
                        <div className="onoffswitch">
                          <Switch
                            name="videoSwitch"
                            valueChanged={this.videoStatusChanged}
                            value={videoEnabled}
                          />
                        </div> <span
                          onClick={() =>
                            this.triggerOpenDialog('openWelcomeVideoDialog')
                          }
                          className="df df-upload"
                        /><span className="vline">&nbsp;</span><span
                          onClick={this.removeVideoPosterHandler}
                          className="df df-trash"></span>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-5 col-sm-12 col-xs-12 replacevideo">
                      <h5>Replace Video</h5>
                      <TextView
                        name="videoUrl"
                        placeholder="URL (http://)"
                        showError={this.state.showErrors}
                        type="text"
                        text={videoUrl}
                        onFieldChanged={this.handleFieldChanged('videoUrl')}
                        errorText={this.errorFor('videoUrl')}
                      />
                      <br />
                      <div className="social-icons">Add Links from<a
                          onClick={() =>
                            window.open('https://www.youtube.com/', '_blank')
                          }
                          href="javascript:void(0)"
                          className="df df-soc-utube"
                          aria-hidden="true"
                        >{' '}</a><a
                          onClick={() =>
                            window.open('https://vimeo.com', '_blank')
                          }
                          href="javascript:void(0)"
                          className="df df-soc-vimo"
                          aria-hidden="true"
                        >{' '}</a>
                      </div>
                    </div>
                  </div>
                  <div className="btnpreview mtop40 text-right col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <Button
                      type="button"
                      buttonSize="btn-sm"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.resetImage}
                      text="CLEAR"
                    />
                    <Button
                      type="button"
                      buttonSize="btn-sm btn-primary"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.saveImageSubmit}
                      text="SAVE"
                    />
                  </div>
                </div>
              </Pane>

              <Pane label="Icons">
                <div className="radio-button">
                  <div className="pull-right revert-to-default">
                    <span className="blue ">Default</span>
                    <Switch
                      name="iconRevert"
                      valueChanged={this.revertDefaultIcon}
                      value={currentAppliedIcons.isDefault}
                    />
                  </div>
                  <RadioGroup
                    name="library"
                    value={changedIcon.name}
                    depth={3}
                    onChange={value => {
                      this.onIconChange(value);
                    }}
                  >
                    {allIcons.map(item => (
                      <div className="icon-styles">
                        <p>{item.name}<Tooltip position="top" message={`${item.name}`}>
                            <span className="df df-info" />
                          </Tooltip></p><RadioButton key={item.id} value={item.name}>
                          <div className="icons">
                            <i className="df df-inc-emp" />
                            <i className="df df-inc-child" />
                            <i className="df df-inc-selfemp" />
                            <i className="df df-inc-mpay" />
                            <i className="df df-inc-rental" />
                            <i className="df df-inc-ssecurity" />
                            <i className="df df-inc-dividend" />
                            <i className="df df-inc-other" />
                            <i className="df df-purchase" />
                            <i className="df df-refinance" />
                            <i className="df df-handcoins" />
                            <i className="df df-banking" />
                            <i className="df df-calculator" />
                            <i className="df df-dollar" />
                          </div>
                        </RadioButton>
                      </div>
                    ))}
                  </RadioGroup>

                  <div className="btnpreview mtop40 text-right col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <Button
                      type="button"
                      buttonSize="btn-sm"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.resetIcon}
                      text="CLEAR"
                    />
                    <Button
                      type="button"
                      buttonSize="btn-sm btn-primary"
                      buttonType="btn-outline"
                      disabled={buttonStatus}
                      clickHandler={this.saveIconSubmit}
                      text="SAVE"
                    />
                  </div>
                </div>
              </Pane>
            </Tabs>
          </div>
        </div>
      </div>
    );
  }
}

Graphics.propTypes = {
  fetchAllLogos: PropTypes.func,
  uploadLogo: PropTypes.func,
  fetchAllImages: PropTypes.func,
  removeBackgroundImage: PropTypes.func,
  removeVideoPoster: PropTypes.func,
  addBackgroundImage: PropTypes.func,
  addVideoPosterImage: PropTypes.func,
  updateVideoUrl: PropTypes.func,
  disableButton: PropTypes.func,
  enableButton: PropTypes.func,
  changeBackgroundStatus: PropTypes.func,
  changeVideoStatus: PropTypes.func,
  saveImages: PropTypes.func,
  clearAssets: PropTypes.func,
  revertLogoDefault: PropTypes.func,
  revertImageDefault: PropTypes.func,
  fetchIcons: PropTypes.func,
  iconsChanged: PropTypes.func,
  saveIcons: PropTypes.func,
  clearIcons: PropTypes.func,
  revertIconDefault: PropTypes.func,
  uploadedFiles: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  illustration: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  videoPoster: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  currentAppliedAssets: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  currentLogo: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  currentAppliedIcons: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  changedIcon: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  allIcons: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  videoUrl: PropTypes.string,
  buttonStatus: PropTypes.bool,
  backgroundImageEnabled: PropTypes.bool,
  videoEnabled: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  uploadedFiles: uploadedFiles(),
  buttonStatus: buttonStatus(),
  illustration: illustration(),
  videoPoster: videoPoster(),
  videoUrl: videoUrl(),
  backgroundImageEnabled: backgroundImageEnabled(),
  videoEnabled: videoEnabled(),
  currentAppliedAssets: currentAppliedAssets(),
  currentLogo: currentLogo(),
  currentAppliedIcons: currentAppliedIcons(),
  changedIcon: changedIcon(),
  allIcons: allIcons(),
});

const mapDispatchToProps = dispatch => ({
  fetchAllLogos: () => {
    dispatch(fetchAllLogos());
  },
  fetchAllImages: () => {
    dispatch(fetchAllImages());
  },
  removeBackgroundImage: () => {
    dispatch(removeBackgroundImage());
  },
  removeVideoPoster: () => {
    dispatch(removeVideoPoster());
  },
  addBackgroundImage: file => {
    dispatch(addBackgroundImage(file));
  },
  addVideoPosterImage: file => {
    dispatch(addVideoPosterImage(file));
  },
  updateVideoUrl: link => {
    dispatch(updateVideoUrl(link));
  },
  disableButton: () => {
    dispatch(disableButton());
  },
  enableButton: () => {
    dispatch(enableButton());
  },
  changeBackgroundStatus: () => {
    dispatch(changeBackgroundStatus());
  },
  changeVideoStatus: () => {
    dispatch(changeVideoStatus());
  },
  saveImages: () => {
    dispatch(saveImages());
  },
  clearAssets: () => {
    dispatch(clearAssets());
  },
  revertLogoDefault: () => {
    dispatch(revertLogoDefault());
  },
  revertIconDefault: () => {
    dispatch(revertIconDefault());
  },
  revertImageDefault: () => {
    dispatch(revertImageDefault());
  },
  uploadLogo: (logo, badge, favicon) => {
    dispatch(uploadLogo(logo, badge, favicon));
  },
  fetchIcons: () => {
    dispatch(fetchIcons());
  },
  iconsChanged: iconName => {
    dispatch(iconsChanged(iconName));
  },
  saveIcons: () => {
    dispatch(saveIcons());
  },
  clearIcons: () => {
    dispatch(clearIcons());
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'graphics', reducer });
const withSaga = injectSaga({ key: 'graphics', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Graphics);

