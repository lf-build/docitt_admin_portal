/**
 * Created by sigma on 4/2/19.
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGraphics = state => state.get('graphics', initialState);

const currentLogo = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('currentAppliedLogo'));

const defaultLogo = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('defaultLogo'));

const defaultAsset = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('defaultAsset'));

const currentAppliedAssets = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('currentAppliedAssets'));

const uploadedFiles = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('allFiles'));

const buttonStatus = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('buttonDisable'));

const illustration = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('illustration'));

const videoPoster = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('videoPoster'));

const videoUrl = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('videoUrl'));

const videoEnabled = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('videoEnabled'));

const backgroundImageEnabled = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('backgroundImageEnabled'));

const defaultIcons = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('defaultIcons'));

const currentAppliedIcons = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('currentAppliedIcons'));

const changedIcon = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('changedIcon'));

const allIcons = () =>
  createSelector(selectGraphics, graphicsState => graphicsState.get('allIcon'));

export {
  selectGraphics,
  defaultLogo,
  currentLogo,
  defaultAsset,
  uploadedFiles,
  buttonStatus,
  illustration,
  videoPoster,
  videoUrl,
  videoEnabled,
  backgroundImageEnabled,
  currentAppliedAssets,
  defaultIcons,
  currentAppliedIcons,
  changedIcon,
  allIcons,
};