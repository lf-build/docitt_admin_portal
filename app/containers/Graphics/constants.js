/**
 * Created by sigma on 4/2/19.
 */
export const FETCH_LOGO = 'lendfoundry/graphic/FETCH_LOGO';
export const UPLOAD_LOGO = 'lendfoundry/graphic/UPLOAD_LOGO';
export const UPDATE_DEFAULT_LOGO = 'lendfoundry/graphic/UPDATE_DEFAULT_LOGO';
export const UPDATE_CURRENT_LOGO = 'lendfoundry/graphic/UPDATE_CURRENT_LOGO';
export const UPDATE_DEFAULT_ASSETS = 'lendfoundry/graphic/UPDATE_DEFAULT_ASSETS';
export const UPDATE_CURRENT_ASSETS = 'lendfoundry/graphic/UPDATE_CURRENT_ASSETS';
export const FETCH_IMAGES = 'lendfoundry/graphic/FETCH_IMAGES';
export const GET_ALL_FILES = 'lendfoundry/graphic/GET_ALL_FILES';
export const UPDATE_FILES_ASSETS = 'lendfoundry/graphic/UPDATE_FILES_ASSETS';
export const DISABLE_BUTTON = 'lendfoundry/graphic/DISABLE_BUTTON';
export const ENABLE_BUTTON = 'lendfoundry/graphic/ENABLE_BUTTON';
export const REMOVE_BACKGROUND_IMAGE = 'lendfoundry/graphic/REMOVE_BACKGROUND_IMAGE';
export const ADD_NEWLY_UPLOADED_IMAGE = 'lendfoundry/graphic/ADD_NEWLY_UPLOADED_IMAGE';
export const BACKGROUND_IMAGE_UPDATE = 'lendfoundry/graphic/BACKGROUND_IMAGE_UPDATE';
export const BACKGROUND_STATUS_UPDATE = 'lendfoundry/graphic/BACKGROUND_STATUS_UPDATE';
export const REMOVE_VIDEO_POSTER_IMAGE = 'lendfoundry/graphic/REMOVE_VIDEO_POSTER_IMAGE';
export const VIDEO_POSTER_IMAGE_UPDATE = 'lendfoundry/graphic/VIDEO_POSTER_IMAGE_UPDATE';
export const VIDEO_URL_UPDATE = 'lendfoundry/graphic/VIDEO_URL_UPDATE';
export const VIDEO_STATUS_UPDATE = 'lendfoundry/graphic/VIDEO_STATUS_UPDATE';
export const SAVE_IMAGES = 'lendfoundry/graphic/SAVE_IMAGES';
export const RESET_CURRENT_STATE = 'lendfoundry/graphic/RESET_CURRENT_STATE';
export const LOGO_REVERT_DEFAULT = 'lendfoundry/graphic/LOGO_REVERT_DEFAULT';
export const IMAGE_REVERT_DEFAULT = 'lendfoundry/graphic/IMAGE_REVERT_DEFAULT';
export const FETCH_ICONS = 'lendfoundry/graphic/FETCH_ICONS';
export const UPDATE_DEFAULT_ICON = 'lendfoundry/graphic/UPDATE_DEFAULT_ICON';
export const UPDATE_CURRENT_ICON = 'lendfoundry/graphic/UPDATE_CURRENT_ICON';
export const UPDATE_ALL_ICONS = 'lendfoundry/graphic/UPDATE_ALL_ICONS';
export const SELECTED_ICONS = 'lendfoundry/graphic/SELECTED_ICONS';
export const SAVE_ICONS = 'lendfoundry/graphic/SAVE_ICONS';
export const CLEAR_ICONS = 'lendfoundry/graphic/CLEAR_ICONS';
export const REVERT_DEFAULT_ICON = 'lendfoundry/graphic/REVERT_DEFAULT_ICON';
export const API_ERROR = 'lendfoundry/graphic/API_ERROR';