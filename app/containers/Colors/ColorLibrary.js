/**
 * Created by sigma on 2/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { RadioButton, RadioGroup } from '@trendmicro/react-radio';
import '@trendmicro/react-radio/dist/react-radio.min.css';

class ColorLibrary extends React.Component {
  editHandler = value => {
    this.props.editHandler(value);
  };

  render() {
    return (
      <RadioGroup
        name="library"
        value={this.props.getAppliedColorPalette}
        depth={3}
        onChange={value => {
          this.props.onLibraryColorChange(value);
        }}
      >
        <div className="library-colors">
          {this.props.libraryColorPalette.map(item => (
            <div className="library-list" key={item.id}>
              <RadioButton value={item.name}>
                <ul>
                  {item.colors.map(colorPalette => (
                    <li
                      className={
                        colorPalette.name === 'Tertiary' ? 'midcolor' : ''
                      }
                      key={`${item.id}` + `${colorPalette.name}`}
                      style={{ background: `${colorPalette.code}` }}
                    />
                  ))}
                </ul>
                <div className="colorname">{item.name}</div>
              </RadioButton>
              {item.isSystemDefined ? (
                ''
              ) : (
                <div
                  className="edit-link"
                  onClick={() => this.editHandler(item)}
                >
                  Edit
                </div>
              )}
            </div>
          ))}
        </div>
      </RadioGroup>
    );
  }
}

ColorLibrary.propTypes = {
  getAppliedColorPalette: PropTypes.string,
  libraryColorPalette: PropTypes.array,
  onLibraryColorChange: PropTypes.func,
};
export default ColorLibrary;
