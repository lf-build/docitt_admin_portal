/**
 * Created by sigma on 20/12/18.
 */
import {
  FETCH_ALL_COLOR_PALETTE,
  UPDATE_CUSTOM_PALETTE,
  SAVE_COLOR_PALETTE,
  SAVE_PREVIEW_COLOR_PALETTE,
  ADD_CUSTOM_COLOR_PALETTE_LIBRARY,
  CHANGE_APPLIED_COLOR,
  APPLY_PALETTE_COLOR,
  STORE_LIBRARY_COLOR_PALETTES,
  APPLIED_LIBRARY_PALETTE,
  DEFAULT_COLOR_PALETTE,
  CURRENT_SELECTED_PALETTE,
  UPDATE_EDIT_COLOR_PALETTE,
  SAVE_EDITED_COLOR_PALETTE,
  UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY,
} from './constants';

export function fetchAllColorPalette() {
  return {
    type: FETCH_ALL_COLOR_PALETTE,
  };
}

export function updateShade(shadeName, colorCode) {
  return {
    type: UPDATE_CUSTOM_PALETTE,
    shadeName,
    colorCode,
  };
}

export function updatePreviewShade(shadeName, colorCode) {
  return {
    type: SAVE_PREVIEW_COLOR_PALETTE,
    shadeName,
    colorCode,
  };
}

export function updateEditShade(shadeName, colorCode) {
  return {
    type: UPDATE_EDIT_COLOR_PALETTE,
    shadeName,
    colorCode,
  };
}

export function saveCustomPalette(payload) {
  return {
    type: SAVE_COLOR_PALETTE,
    requestPayload: payload,
  };
}

export function saveEditedPalette(payload) {
  return {
    type: SAVE_EDITED_COLOR_PALETTE,
    requestPayload: payload,
  };
}

export function updateLibrary(payload) {
  return {
    type: ADD_CUSTOM_COLOR_PALETTE_LIBRARY,
    response: payload,
  };
}

export function updateExistingLibrary(payload) {
  return {
    type: UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY,
    response: payload,
  };
}

export function changeAppliedColor(theme) {
  return {
    type: CHANGE_APPLIED_COLOR,
    theme,
  };
}
export function applyColorPalette() {
  return {
    type: APPLY_PALETTE_COLOR,
  };
}

export function updateLibraryColorPalette(allColorPalettePresent) {
  return {
    type: STORE_LIBRARY_COLOR_PALETTES,
    value: allColorPalettePresent,
  };
}

export function appliedLibraryColorPalette(allColorPalettePresent) {
  return {
    type: APPLIED_LIBRARY_PALETTE,
    value: allColorPalettePresent,
  };
}

export function defaultLibraryColorPalette(allColorPalettePresent) {
  return {
    type: DEFAULT_COLOR_PALETTE,
    value: allColorPalettePresent,
  };
}

export function currentLibraryColorPalette(allColorPalettePresent) {
  return {
    type: CURRENT_SELECTED_PALETTE,
    value: allColorPalettePresent,
  };
}
