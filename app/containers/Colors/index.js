import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import './color.css';
import 'react-confirm-alert/src/react-confirm-alert.css';
import TextView from 'components/TextView';
import Button from 'components/Button';
import ColorPicker from 'components/Colorpicker';
import Switch from 'components/SwitchView';
import Tabs from 'components/Tabs';
import Pane from 'components/Pane';
import PopUp from 'components/PopUp';
import { confirmAlert } from 'react-confirm-alert';
import { run, ruleRunner } from 'Validation/ruleRunner';
import {
  required,
  exactLength,
  validColorCode,
  checkSpace,
} from 'Validation/rules';
import ColorLibrary from './ColorLibrary';
import Preview from './Preview';
import {
  updateShade,
  updatePreviewShade,
  saveCustomPalette,
  changeAppliedColor,
  applyColorPalette,
  fetchAllColorPalette,
  updateEditShade,
  saveEditedPalette,
} from './actions';
import {
  customColorPalette,
  getEditColorPalette,
  previewColorPalette,
  libraryColorPalette,
  getAppliedColorPalette,
  getCurrentSelectedPalette,
  revertToDefaultValue,
  defaultColorPaletteName,
} from './selectors';
import saga from './saga';
import reducer from './reducer';

const fieldValidations = [
  ruleRunner(
    'primaryOne',
    'Primary One',
    required,
    validColorCode,
    exactLength(7),
  ),
  ruleRunner(
    'primaryTwo',
    'Primary Two',
    required,
    validColorCode,
    exactLength(7),
  ),
  ruleRunner(
    'secondaryOne',
    'Secondary One',
    required,
    validColorCode,
    exactLength(7),
  ),
  ruleRunner(
    'secondaryTwo',
    'Secondary Two',
    required,
    validColorCode,
    exactLength(7),
  ),
  ruleRunner('tertiary', 'Tertiary', required, validColorCode, exactLength(7)),
];

const colorPaletteNameValidation = [
  ruleRunner('colorPaletteName', 'Color Palette Name', required, checkSpace),
];

class Colors extends React.Component {
  constructor(props) {
    super(props);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleFieldChanged = this.handleFieldChanged.bind(this);
    this.handlePreview = this.handlePreview.bind(this);
    this.handleSubmitClicked = this.handleSubmitClicked.bind(this);
    this.changeDefaultLibraryColor = this.changeDefaultLibraryColor.bind(this);
    this.revertToDefault = this.revertToDefault.bind(this);
    this.applyColorPalette = this.applyColorPalette.bind(this);
    this.revertColorDefault = this.revertColorDefault.bind(this);
    this.errorFor = this.errorFor.bind(this);
    this.state = {
      showErrors: false,
      validationErrors: {},
      editShowErrors: false,
      editValidationErrors: {},
      popupVisibility: false,
      popupTitle: '',
      contentEdited: false,
    };
  }

  componentDidMount() {
    this.props.fetchAllColorPalette();
  }

  handleColorChange(color, shade) {
    this.props.updateShade(shade, color.hex);
    this.validateColorCode(shade, color.hex);
  }

  handleFieldChanged(field) {
    return e => {
      this.props.updateShade(field, e.target.value);
      this.validateColorCode(field, e.target.value);
    };
  }

  validateColorCode(field, colorCode) {
    const fieldValues = {
      primaryOne: field === 'primaryOne' ? colorCode : this.props.primaryOne,
      primaryTwo: field === 'primaryTwo' ? colorCode : this.props.primaryTwo,
      secondaryOne:
        field === 'secondaryOne' ? colorCode : this.props.secondaryOne,
      secondaryTwo:
        field === 'secondaryTwo' ? colorCode : this.props.secondaryTwo,
      tertiary: field === 'tertiary' ? colorCode : this.props.tertiary,
    };
    this.state.validationErrors = run(fieldValues, fieldValidations);
    if (
      (Object.keys(this.state.validationErrors).length === 0 &&
        this.state.validationErrors.constructor === Object) === false
    ) {
      this.setState({ showErrors: true });
    } else {
      this.setState({ showErrors: false });
    }
  }

  editHandleColorChange = (color, shade) => {
    this.props.updateEditShade(shade, color.hex);
    this.editValidateColorCode(shade, color.hex);
  };

  editHandleFieldChanged = field => e => {
    this.props.updateEditShade(field, e.target.value);
    this.editValidateColorCode(field, e.target.value);
  };

  editValidateColorCode = (field, colorCode) => {
    const fieldValues = {
      primaryOne:
        field === 'primaryOne' ? colorCode : this.props.editPrimaryOne,
      primaryTwo:
        field === 'primaryTwo' ? colorCode : this.props.editPrimaryTwo,
      secondaryOne:
        field === 'secondaryOne' ? colorCode : this.props.editSecondaryOne,
      secondaryTwo:
        field === 'secondaryTwo' ? colorCode : this.props.editSecondaryTwo,
      tertiary: field === 'tertiary' ? colorCode : this.props.editTertiary,
    };
    this.state.editValidationErrors = run(fieldValues, fieldValidations);
    if (
      (Object.keys(this.state.editValidationErrors).length === 0 &&
        this.state.editValidationErrors.constructor === Object) === false
    ) {
      this.setState({ editShowErrors: true, contentEdited: true });
    } else {
      this.setState({ editShowErrors: false, contentEdited: true });
    }
  };

  handlePreview() {
    const colorList = [
      'primaryOne',
      'primaryTwo',
      'secondaryOne',
      'secondaryTwo',
      'tertiary',
    ];
    colorList.map(colorShade =>
      this.props.updatePreviewShade(colorShade, this.props[colorShade]),
    );
  }

  handleSubmitClicked() {
    const fieldValues = {
      colorPaletteName: this.props.colorPaletteName,
    };
    this.state.validationErrors = run(fieldValues, colorPaletteNameValidation);
    this.setState({ showErrors: true });
    if (
      this.props.libraryColorPalette.some(
        e => e.name.toLowerCase() === this.props.colorPaletteName.toLowerCase(),
      )
    ) {
      this.state.validationErrors = {
        colorPaletteName: 'This Palette name is already taken.',
      };
    }
    if (
      (Object.keys(this.state.validationErrors).length === 0 &&
        this.state.validationErrors.constructor === Object) === false
    )
      return null;
    const payload = {
      name: this.props.colorPaletteName,
      colors: [
        {
          name: 'Primary1',
          code: this.props.primaryOne,
        },
        {
          name: 'Primary2',
          code: this.props.primaryTwo,
        },
        {
          name: 'Secondary1',
          code: this.props.secondaryOne,
        },
        {
          name: 'Secondary2',
          code: this.props.secondaryTwo,
        },
        {
          name: 'Tertiary',
          code: this.props.tertiary,
        },
      ],
    };
    this.props.saveCustomPalette(payload);
    this.setState({ showErrors: false });
    return true;
  }

  editHandleSubmitClicked = () => {
    if (this.state.contentEdited) {
      const payload = {
        name: this.props.editColorPaletteName,
        colors: [
          {
            name: 'Primary1',
            code: this.props.editPrimaryOne,
          },
          {
            name: 'Primary2',
            code: this.props.editPrimaryTwo,
          },
          {
            name: 'Secondary1',
            code: this.props.editSecondaryOne,
          },
          {
            name: 'Secondary2',
            code: this.props.editSecondaryTwo,
          },
          {
            name: 'Tertiary',
            code: this.props.editTertiary,
          },
        ],
      };
      this.props.saveEditedPalette(payload);
    }
    this.hidePopup();
  };

  errorFor(field) {
    return this.state.validationErrors[field] || '';
  }

  editErrorFor(field) {
    return this.state.editValidationErrors[field] || '';
  }

  changeDefaultLibraryColor(value) {
    this.props.changeAppliedColor(value);
    const newPreviewColor = this.props.libraryColorPalette.find(
      c => c.name === value,
    );
    newPreviewColor.colors.map(colorName => {
      switch (colorName.name) {
        case 'Primary1':
          this.props.updatePreviewShade('primaryOne', colorName.code);
          break;
        case 'Primary2':
          this.props.updatePreviewShade('primaryTwo', colorName.code);
          break;
        case 'Secondary1':
          this.props.updatePreviewShade('secondaryOne', colorName.code);
          break;
        case 'Secondary2':
          this.props.updatePreviewShade('secondaryTwo', colorName.code);
          break;
        case 'Tertiary':
          this.props.updatePreviewShade('tertiary', colorName.code);
          break;
      }
    });
  }

  revertColorDefault() {
    if (!this.props.revertToDefaultValue) {
      this.changeDefaultLibraryColor(this.props.defaultColorPaletteName);
      this.props.applyColorPalette();
    }
  }

  revertToDefault() {
    this.changeDefaultLibraryColor(this.props.defaultColorPaletteName);
    // this.props.applyColorPalette();
  }

  applyColorPalette() {
    if (
      this.props.getCurrentSelectedPalette === this.props.getAppliedColorPalette
    )
      return null;
    this.props.applyColorPalette();
  }

  paletteEdit = clickedValue => {
    this.props.updateEditShade('name', clickedValue.name);
    this.props.updateEditShade('isActivated', clickedValue.isApplied);
    clickedValue.colors.map(colorShade => {
      switch (colorShade.name) {
        case 'Primary1':
          this.props.updateEditShade('primaryOne', colorShade.code);
          break;
        case 'Primary2':
          this.props.updateEditShade('primaryTwo', colorShade.code);
          break;
        case 'Secondary1':
          this.props.updateEditShade('secondaryOne', colorShade.code);
          break;
        case 'Secondary2':
          this.props.updateEditShade('secondaryTwo', colorShade.code);
          break;
        case 'Tertiary':
          this.props.updateEditShade('tertiary', colorShade.code);
          break;
      }
    });
    this.setState({
      popupTitle: `Palette Name >> ${clickedValue.name}`,
      popupVisibility: true,
    });
  };

  cancelEdit = () => {
    if (this.state.contentEdited) {
      confirmAlert({
        title: 'Are you sure you want to cancel?',
        message: 'All your changes will not be save.',
        buttons: [
          {
            label: 'No',
          },
          {
            label: 'Yes',
            onClick: () => this.hidePopup(),
          },
        ],
      });
    } else {
      this.hidePopup();
    }
  };

  hidePopup = () => {
    this.setState({
      popupVisibility: false,
      editShowErrors: false,
      contentEdited: false,
    });
  };

  render() {
    const { popupVisibility, popupTitle } = this.state;
    return (
      <div className="dashboard clearfix">
        <div className="headertitle">Colors</div>
        <div className="padd clearfix">
          <div className="revert clearfix">
            <p className="pull-left">
              Create a custom palette yourself or select from our existing
              library
            </p>
            <div className="pull-right">
              <span className="blue ">Default</span>
              <Switch
                valueChanged={this.revertColorDefault}
                value={this.props.revertToDefaultValue}
              />
            </div>
          </div>

          <div className="clearfix colors">
            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 no-pad">
              <Tabs selected={0}>
                <Pane label="Custom">
                  <div>
                    <ul className="colorswatch clearfix">
                      <li>
                        <ColorPicker
                          defaultColor={this.props.primaryOne}
                          shade="primaryOne"
                          onColorChanged={this.handleColorChange}
                        />
                        <h3>
                          Primary <br />
                          One
                        </h3>
                        <TextView
                          name="primaryOne"
                          showError={this.state.showErrors}
                          type="text"
                          text={this.props.primaryOne}
                          onFieldChanged={this.handleFieldChanged('primaryOne')}
                          errorText={this.errorFor('primaryOne')}
                        />
                      </li>
                      <li>
                        <ColorPicker
                          defaultColor={this.props.primaryTwo}
                          shade="primaryTwo"
                          onColorChanged={this.handleColorChange}
                        />
                        <h3>
                          Primary <br />
                          Two
                        </h3>
                        <TextView
                          name="primaryTwo"
                          showError={this.state.showErrors}
                          type="text"
                          text={this.props.primaryTwo}
                          onFieldChanged={this.handleFieldChanged('primaryTwo')}
                          errorText={this.errorFor('primaryTwo')}
                        />
                      </li>
                      <li>
                        <ColorPicker
                          defaultColor={this.props.secondaryOne}
                          shade="secondaryOne"
                          onColorChanged={this.handleColorChange}
                        />
                        <h3>
                          Secondary
                          <br />
                          One
                        </h3>
                        <TextView
                          name="secondaryOne"
                          showError={this.state.showErrors}
                          type="text"
                          text={this.props.secondaryOne}
                          onFieldChanged={this.handleFieldChanged(
                            'secondaryOne',
                          )}
                          errorText={this.errorFor('secondaryOne')}
                        />
                      </li>
                      <li>
                        <ColorPicker
                          defaultColor={this.props.secondaryTwo}
                          shade="secondaryTwo"
                          onColorChanged={this.handleColorChange}
                        />
                        <h3>
                          Secondary
                          <br /> Two
                        </h3>
                        <TextView
                          name="secondaryTwo"
                          showError={this.state.showErrors}
                          type="text"
                          text={this.props.secondaryTwo}
                          onFieldChanged={this.handleFieldChanged(
                            'secondaryTwo',
                          )}
                          errorText={this.errorFor('secondaryTwo')}
                        />
                      </li>
                      <li>
                        <ColorPicker
                          defaultColor={this.props.tertiary}
                          shade="tertiary"
                          onColorChanged={this.handleColorChange}
                        />
                        <h3>Tertiary</h3>
                        <TextView
                          name="tertiary"
                          showError={this.state.showErrors}
                          type="text"
                          text={this.props.tertiary}
                          onFieldChanged={this.handleFieldChanged('tertiary')}
                          errorText={this.errorFor('tertiary')}
                        />
                      </li>
                    </ul>
                    <TextView
                      name="colorPaletteName"
                      placeholder="Enter Custom palette name !"
                      showError={this.state.showErrors}
                      type="text"
                      text={this.props.colorPaletteName}
                      onFieldChanged={this.handleFieldChanged(
                        'colorPaletteName',
                      )}
                      errorText={this.errorFor('colorPaletteName')}
                    />
                    <div className="btnpreview">
                      <Button
                        type="button"
                        buttonSize="btn-sm"
                        buttonType="btn-outline"
                        disabled={this.state.showErrors}
                        clickHandler={this.handlePreview}
                        text="PREVIEW"
                      />
                      <Button
                        type="button"
                        buttonSize="btn-sm"
                        buttonType="btn-outline"
                        disabled={this.state.showErrors}
                        clickHandler={this.handleSubmitClicked}
                        text="SAVE"
                      />
                    </div>
                  </div>
                </Pane>
                <Pane label="Library">
                  <div className="tab-pane clearfix">
                    <ColorLibrary
                      getAppliedColorPalette={this.props.getAppliedColorPalette}
                      onLibraryColorChange={this.changeDefaultLibraryColor}
                      libraryColorPalette={this.props.libraryColorPalette}
                      editHandler={this.paletteEdit}
                    />
                    <div className="btnpreview">
                      <Button
                        type="button"
                        buttonSize="btn-sm"
                        buttonType="btn-outline"
                        disabled={this.state.showErrors}
                        clickHandler={this.revertToDefault}
                        text="Clear"
                      />
                      <Button
                        type="button"
                        buttonSize="btn-sm"
                        disabled={this.state.showErrors}
                        clickHandler={this.applyColorPalette}
                        text="APPLY PALETTE"
                      />
                    </div>
                  </div>
                </Pane>
              </Tabs>
            </div>
            <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12 preview">
              <h3>Preview</h3>
              <Preview
                primaryOne={this.props.previewPrimaryOne}
                primaryTwo={this.props.previewPrimaryTwo}
                secondaryOne={this.props.previewSecondaryOne}
                secondaryTwo={this.props.previewSecondaryTwo}
                tertiary={this.props.previewTertiary}
              />
            </div>

            <PopUp visible={popupVisibility} title={popupTitle}>
              <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ptop10">
                <h3>Palette</h3>
                <ul className="colorswatch clearfix">
                  <li>
                    <ColorPicker
                      defaultColor={this.props.editPrimaryOne}
                      shade="primaryOne"
                      onColorChanged={this.editHandleColorChange}
                    />
                    <h3>
                      Primary <br />
                      One
                    </h3>
                    <TextView
                      name="primaryOne"
                      showError={this.state.editShowErrors}
                      type="text"
                      text={this.props.editPrimaryOne}
                      onFieldChanged={this.editHandleFieldChanged('primaryOne')}
                      errorText={this.editErrorFor('primaryOne')}
                    />
                  </li>
                  <li>
                    <ColorPicker
                      defaultColor={this.props.editPrimaryTwo}
                      shade="primaryTwo"
                      onColorChanged={this.editHandleColorChange}
                    />
                    <h3>
                      Primary <br />
                      Two
                    </h3>
                    <TextView
                      name="primaryTwo"
                      showError={this.state.editShowErrors}
                      type="text"
                      text={this.props.editPrimaryTwo}
                      onFieldChanged={this.editHandleFieldChanged('primaryTwo')}
                      errorText={this.editErrorFor('primaryTwo')}
                    />
                  </li>
                  <li>
                    <ColorPicker
                      defaultColor={this.props.editSecondaryOne}
                      shade="secondaryOne"
                      onColorChanged={this.editHandleColorChange}
                    />
                    <h3>
                      Secondary
                      <br />
                      One
                    </h3>
                    <TextView
                      name="secondaryOne"
                      showError={this.state.editShowErrors}
                      type="text"
                      text={this.props.editSecondaryOne}
                      onFieldChanged={this.editHandleFieldChanged(
                        'secondaryOne',
                      )}
                      errorText={this.editErrorFor('secondaryOne')}
                    />
                  </li>
                  <li>
                    <ColorPicker
                      defaultColor={this.props.editSecondaryTwo}
                      shade="secondaryTwo"
                      onColorChanged={this.editHandleColorChange}
                    />
                    <h3>
                      Secondary
                      <br /> Two
                    </h3>
                    <TextView
                      name="secondaryTwo"
                      showError={this.state.editShowErrors}
                      type="text"
                      text={this.props.editSecondaryTwo}
                      onFieldChanged={this.editHandleFieldChanged(
                        'secondaryTwo',
                      )}
                      errorText={this.editErrorFor('secondaryTwo')}
                    />
                  </li>
                  <li>
                    <ColorPicker
                      defaultColor={this.props.editTertiary}
                      shade="tertiary"
                      onColorChanged={this.editHandleColorChange}
                    />
                    <h3>Tertiary</h3>
                    <TextView
                      name="tertiary"
                      showError={this.state.editShowErrors}
                      type="text"
                      text={this.props.editTertiary}
                      onFieldChanged={this.editHandleFieldChanged('tertiary')}
                      errorText={this.editErrorFor('tertiary')}
                    />
                  </li>
                </ul>
                <div className="mtop0 btnpreview">
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    buttonType="btn-outline"
                    disabled={this.state.editShowErrors}
                    clickHandler={this.cancelEdit}
                    text="Cancel"
                  />
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    disabled={this.state.editShowErrors}
                    clickHandler={this.editHandleSubmitClicked}
                    text="Save"
                  />
                </div>
              </div>
              <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 preview border-left">
                <h3>Preview</h3>
                <Preview
                  primaryOne={this.props.editPrimaryOne}
                  primaryTwo={this.props.editPrimaryTwo}
                  secondaryOne={this.props.editSecondaryOne}
                  secondaryTwo={this.props.editSecondaryTwo}
                  tertiary={this.props.editTertiary}
                />
              </div>
            </PopUp>
          </div>
        </div>
      </div>
    );
  }
}

Colors.propTypes = {
  primaryOne: PropTypes.string,
  primaryTwo: PropTypes.string,
  secondaryOne: PropTypes.string,
  secondaryTwo: PropTypes.string,
  tertiary: PropTypes.string,
  colorPaletteName: PropTypes.string,
  previewPrimaryOne: PropTypes.string,
  previewPrimaryTwo: PropTypes.string,
  previewSecondaryOne: PropTypes.string,
  previewSecondaryTwo: PropTypes.string,
  previewTertiary: PropTypes.string,
  editPrimaryOne: PropTypes.string,
  editPrimaryTwo: PropTypes.string,
  editSecondaryOne: PropTypes.string,
  editSecondaryTwo: PropTypes.string,
  editTertiary: PropTypes.string,
  editColorPaletteName: PropTypes.string,
  getAppliedColorPalette: PropTypes.string,
  getCurrentSelectedPalette: PropTypes.string,
  defaultColorPaletteName: PropTypes.string,
  revertToDefaultValue: PropTypes.bool,
  libraryColorPalette: PropTypes.array,
  updateShade: PropTypes.func,
  updatePreviewShade: PropTypes.func,
  saveCustomPalette: PropTypes.func,
  changeAppliedColor: PropTypes.func,
  applyColorPalette: PropTypes.func,
  fetchAllColorPalette: PropTypes.func,
  updateEditShade: PropTypes.func,
  saveEditedPalette: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  primaryOne: customColorPalette('primaryOne'),
  primaryTwo: customColorPalette('primaryTwo'),
  secondaryOne: customColorPalette('secondaryOne'),
  secondaryTwo: customColorPalette('secondaryTwo'),
  tertiary: customColorPalette('tertiary'),
  colorPaletteName: customColorPalette('colorPaletteName'),
  previewPrimaryOne: previewColorPalette('primaryOne'),
  previewPrimaryTwo: previewColorPalette('primaryTwo'),
  previewSecondaryOne: previewColorPalette('secondaryOne'),
  previewSecondaryTwo: previewColorPalette('secondaryTwo'),
  previewTertiary: previewColorPalette('tertiary'),
  editPrimaryOne: getEditColorPalette('primaryOne'),
  editPrimaryTwo: getEditColorPalette('primaryTwo'),
  editSecondaryOne: getEditColorPalette('secondaryOne'),
  editSecondaryTwo: getEditColorPalette('secondaryTwo'),
  editTertiary: getEditColorPalette('tertiary'),
  editColorPaletteName: getEditColorPalette('name'),
  libraryColorPalette: libraryColorPalette(),
  getAppliedColorPalette: getAppliedColorPalette(),
  getCurrentSelectedPalette: getCurrentSelectedPalette(),
  revertToDefaultValue: revertToDefaultValue(),
  defaultColorPaletteName: defaultColorPaletteName(),
});

const mapDispatchToProps = dispatch => ({
  updateShade: (shadeName, colorCode) => {
    dispatch(updateShade(shadeName, colorCode));
  },
  updatePreviewShade: (shadeName, colorCode) => {
    dispatch(updatePreviewShade(shadeName, colorCode));
  },
  updateEditShade: (shadeName, colorCode) => {
    dispatch(updateEditShade(shadeName, colorCode));
  },
  saveCustomPalette: payload => {
    dispatch(saveCustomPalette(payload));
  },
  saveEditedPalette: payload => {
    dispatch(saveEditedPalette(payload));
  },
  changeAppliedColor: payload => {
    dispatch(changeAppliedColor(payload));
  },
  applyColorPalette: () => {
    dispatch(applyColorPalette());
  },
  fetchAllColorPalette: () => {
    dispatch(fetchAllColorPalette());
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'color', reducer });
const withSaga = injectSaga({ key: 'color', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Colors);

