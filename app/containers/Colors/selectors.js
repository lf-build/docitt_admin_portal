/**
 * Created by sigma on 20/12/18.
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectColor = state => state.get('color', initialState);

const allCustomColorPalette = () =>
  createSelector(selectColor, colorState =>
    colorState.get('customColorPalette'),
  );

const customColorPalette = shadeType =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['customColorPalette', shadeType]),
  );

const getEditColorPalette = shadeType =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['editColorPalette', shadeType]),
  );

const previewColorPalette = shadeType =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['previewColorPalette', shadeType]),
  );

const libraryColorPalette = () =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['libraryColorPalette']).toJS(),
  );

const getAppliedColorPalette = () =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['appliedLibraryPalette', 'name']),
  );

const getCurrentSelectedPalette = () =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['currentSelectedPalette', 'name']),
  );

const revertToDefaultValue = () =>
  createSelector(
    selectColor,
    colorState =>
      colorState.getIn(['currentSelectedPalette', 'name']) ===
      colorState.getIn(['defaultColorPalette', 'name']),
  );

const defaultColorPaletteName = () =>
  createSelector(selectColor, colorState =>
    colorState.getIn(['defaultColorPalette', 'name']),
  );

export {
  selectColor,
  allCustomColorPalette,
  customColorPalette,
  previewColorPalette,
  libraryColorPalette,
  getAppliedColorPalette,
  revertToDefaultValue,
  defaultColorPaletteName,
  getCurrentSelectedPalette,
  getEditColorPalette,
};
