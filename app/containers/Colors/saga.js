import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { postRequest, getRequest, putRequest } from 'utils/request';
import { getAppliedColorPalette, getEditColorPalette } from './selectors';
import { showMessageSuccess, showErrorMessage } from 'components/ErrorMessage';
import {
  SAVE_COLOR_PALETTE,
  APPLY_PALETTE_COLOR,
  FETCH_ALL_COLOR_PALETTE,
  SAVE_EDITED_COLOR_PALETTE,
} from './constants';
import {
  updateLibrary,
  updateExistingLibrary,
  updateLibraryColorPalette,
  appliedLibraryColorPalette,
  defaultLibraryColorPalette,
  currentLibraryColorPalette,
} from './actions';

/**
 * request/response handler
 */
export function* getAllColorPalette() {
  const base = yield select(serviceUrl('theme'));
  const url = '/colorpallet/all';

  try {
    const allColorPalettePresent = yield call(getRequest, base, url);
    yield put(updateLibraryColorPalette(allColorPalettePresent));
    const appliedColorPalette = allColorPalettePresent.find(c => c.isApplied);
    const defaultColorPalette = allColorPalettePresent.find(c => c.isDefault);
    yield put(appliedLibraryColorPalette(appliedColorPalette));
    yield put(currentLibraryColorPalette(appliedColorPalette.name));
    yield put(defaultLibraryColorPalette(defaultColorPalette.name));
  } catch (err) {
    yield call(showErrorMessage,err);
    // yield put(updateFailure(err));
  }
}

export function* storeCustomColor(payload) {
  const base = yield select(serviceUrl('theme'));
  const url = '/colorpallet/';

  try {
    const addedColorPalette = yield call(
      postRequest,
      base,
      url,
      payload.requestPayload,
    );
    yield put(updateLibrary(addedColorPalette));
    yield call(showMessageSuccess, 'Color palette added successfully.');
  } catch (err) {
    yield call(showErrorMessage, err);
    // yield put(updateFailure(err));
  }
}

export function* storeEditedColorPalette(payload) {
  const base = yield select(serviceUrl('theme'));
  const getEditColorPaletteStatus = yield select(
    getEditColorPalette('isActivated'),
  );
  const url = '/colorpallet/';
  const generateCssUrl = '/theme/generate/css';

  try {
    const updatedColorPalette = yield call(
      postRequest,
      base,
      url,
      payload.requestPayload,
    );
    yield put(updateExistingLibrary(updatedColorPalette));
    if (getEditColorPaletteStatus) {
      yield call(postRequest, base, generateCssUrl, {});
      yield call(
        showMessageSuccess,
        'Color palette updated and applied successfully.',
      );
    } else {
      yield call(showMessageSuccess, 'Color palette updated successfully.');
    }
  } catch (err) {
    yield call(showErrorMessage,err);
    // yield put(updateFailure(err));
  }
}

export function* applyColorPalette() {
  const base = yield select(serviceUrl('theme'));
  const appliedPaletteName = yield select(getAppliedColorPalette());
  const url = `/colorpallet/apply/${appliedPaletteName}`;
  const generateCssUrl = '/theme/generate/css';

  try {
    yield call(putRequest, base, url);
    yield put(currentLibraryColorPalette(appliedPaletteName));
    yield call(postRequest, base, generateCssUrl, {});
    yield call(showMessageSuccess, 'Color palette applied successfully.');
  } catch (err) {
    yield call(showErrorMessage, err);
    // yield put(updateFailure(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* saveCustomColor() {
  yield all([
    takeLatest(SAVE_COLOR_PALETTE, storeCustomColor),
    takeLatest(APPLY_PALETTE_COLOR, applyColorPalette),
    takeLatest(FETCH_ALL_COLOR_PALETTE, getAllColorPalette),
    takeLatest(SAVE_EDITED_COLOR_PALETTE, storeEditedColorPalette),
  ]);
}
