/**
 * Created by sigma on 20/12/18.
 */
export const FETCH_ALL_COLOR_PALETTE =
  'lendfoundry/Colors/FETCH_ALL_COLOR_PALETTE';
export const UPDATE_CUSTOM_PALETTE = 'lendfoundry/Colors/UPDATE_CUSTOM_PALETTE';
export const SAVE_COLOR_PALETTE = 'lendfoundry/Colors/SAVE_COLOR_PALETTE';
export const ADD_CUSTOM_COLOR_PALETTE_LIBRARY =
  'lendfoundry/Colors/ADD_CUSTOM_COLOR_PALETTE_LIBRARY';
export const SAVE_PREVIEW_COLOR_PALETTE =
  'lendfoundry/Colors/SAVE_PREVIEW_COLOR_PALETTE';
export const CHANGE_APPLIED_COLOR = 'lendfoundry/Colors/CHANGE_APPLIED_COLOR';
export const APPLY_PALETTE_COLOR = 'lendfoundry/Colors/APPLY_PALETTE_COLOR';
export const STORE_LIBRARY_COLOR_PALETTES =
  'lendfoundry/Colors/STORE_LIBRARY_COLOR_PALETTES';
export const APPLIED_LIBRARY_PALETTE =
  'lendfoundry/Colors/APPLIED_LIBRARY_PALETTE';
export const DEFAULT_COLOR_PALETTE = 'lendfoundry/Colors/DEFAULT_COLOR_PALETTE';
export const CURRENT_SELECTED_PALETTE =
  'lendfoundry/Colors/CURRENT_SELECTED_PALETTE';
export const UPDATE_EDIT_COLOR_PALETTE =
  'lendfoundry/Colors/UPDATE_EDIT_COLOR_PALETTE';
export const SAVE_EDITED_COLOR_PALETTE =
  'lendfoundry/Colors/SAVE_EDITED_COLOR_PALETTE';
export const UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY =
  'lendfoundry/Colors/UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY';
