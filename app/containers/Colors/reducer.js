/**
 * Created by sigma on 20/12/18.
 */
import { fromJS } from 'immutable';
import {
  UPDATE_CUSTOM_PALETTE,
  SAVE_PREVIEW_COLOR_PALETTE,
  ADD_CUSTOM_COLOR_PALETTE_LIBRARY,
  CHANGE_APPLIED_COLOR,
  STORE_LIBRARY_COLOR_PALETTES,
  APPLIED_LIBRARY_PALETTE,
  DEFAULT_COLOR_PALETTE,
  CURRENT_SELECTED_PALETTE,
  UPDATE_EDIT_COLOR_PALETTE,
  UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY,
} from './constants';

export const initialState = fromJS({
  customColorPalette: {
    primaryOne: '#59595c',
    primaryTwo: '#234873',
    secondaryOne: '#354daf',
    secondaryTwo: '#408afd',
    tertiary: '#00c0c2',
    colorPaletteName: '',
  },
  previewColorPalette: {
    primaryOne: '#59595c',
    primaryTwo: '#234873',
    secondaryOne: '#354daf',
    secondaryTwo: '#408afd',
    tertiary: '#00c0c2',
  },
  editColorPalette: {
    name: '',
    primaryOne: '',
    primaryTwo: '',
    secondaryOne: '',
    secondaryTwo: '',
    tertiary: '',
    isActivated: false,
  },
  libraryColorPalette: [
    {
      name: 'Frost',
      colors: [
        {
          name: 'Primary1',
          code: '#004062',
        },
        {
          name: 'Primary2',
          code: '#417596',
        },
        {
          name: 'Secondary1',
          code: '#5198de',
        },
        {
          name: 'Secondary2',
          code: '#5dafff',
        },
        {
          name: 'Tertiary',
          code: '#00ceff',
        },
      ],
      isDefault: true,
      isSystemDefined: true,
      isApplied: true,
      createdBy: 'loanofficer@oc.com',
      createdOn: '2018-11-26T13:34:17.7146989-05:00',
      updatedBy: 'sangeetha.b+oc_01@sigmainfo.net',
      updatedOn: '2018-11-28T05:59:37.1123688-05:00',
      appliedBy: 'sangeetha.b+bo-0_01@sigmainfo.net',
      appliedOn: '2018-11-29T01:35:38.477958-05:00',
      id: '5bfc3ca954db810001c6e148',
    },
  ],
  appliedLibraryPalette: {},
  defaultColorPalette: {
    name: '',
  },
  currentSelectedPalette: {
    name: '',
  },
});

function colorReducer(state = initialState, action) {
  switch (action.type) {
    case STORE_LIBRARY_COLOR_PALETTES:
      return state.setIn(['libraryColorPalette'], fromJS(action.value));

    case APPLIED_LIBRARY_PALETTE:
      return state.set('appliedLibraryPalette', fromJS(action.value));

    case DEFAULT_COLOR_PALETTE:
      return state.setIn(['defaultColorPalette', 'name'], action.value);

    case CURRENT_SELECTED_PALETTE:
      return state.setIn(['currentSelectedPalette', 'name'], action.value);

    case UPDATE_CUSTOM_PALETTE:
      return state.setIn(
        ['customColorPalette', action.shadeName],
        action.colorCode,
      );

    case SAVE_PREVIEW_COLOR_PALETTE:
      return state.setIn(
        ['previewColorPalette', action.shadeName],
        action.colorCode,
      );

    case UPDATE_EDIT_COLOR_PALETTE:
      return state.setIn(
        ['editColorPalette', action.shadeName],
        action.colorCode,
      );

    case ADD_CUSTOM_COLOR_PALETTE_LIBRARY:
      return state.set(
        'libraryColorPalette',
        state.get('libraryColorPalette').push(fromJS(action.response)),
      );

    case UPDATE_CUSTOM_COLOR_PALETTE_LIBRARY:
      const updatedLibrary = state
        .get('libraryColorPalette')
        .toJS()
        .map(item => {
          if (item.name === action.response.name) {
            item.colors[0].code = action.response.colors[0].code;
            item.colors[1].code = action.response.colors[1].code;
            item.colors[2].code = action.response.colors[2].code;
            item.colors[3].code = action.response.colors[3].code;
            item.colors[4].code = action.response.colors[4].code;
          }
          return item;
        });
      return state.set('libraryColorPalette', fromJS(updatedLibrary));

    case CHANGE_APPLIED_COLOR:
      const selectedPaletteIndex = state
        .getIn(['libraryColorPalette'])
        .findIndex(c => c.get('name') === action.theme);

      return state.setIn(
        ['appliedLibraryPalette'],
        state.getIn(['libraryColorPalette', selectedPaletteIndex]),
      );

    default:
      return state;
  }
}

export default colorReducer;
