/**
 * Created by sigma on 27/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';
import Logo from 'components/Logo';
function Preview(props) {
  return (
    <div>
      <div className="header clearfix">
        <div className="pull-left">
          <Logo icon="grey" />
        </div>
        <div className="pull-right" style={{ color: props.secondaryOne }}>
          James Henderson <i className="df df-angle-down" />
        </div>
      </div>
      <div className="leftpanel col-lg-3 col-md-4 col-sm-12 col-xs-12">
        <div className="title" style={{ color: props.secondaryOne }}>
          PROGRESS
        </div>
        <ul>
          <li className="current" style={{ background: props.primaryOne }}>
            1<br />
            Profile
          </li>
          <li className="bar" style={{ background: props.tertiary }} />
          <li style={{ color: props.primaryOne }}>
            2<br />
            Assets
          </li>
          <li style={{ color: props.primaryOne }}>
            3<br />
            Income
          </li>
          <li style={{ color: props.primaryOne }}>
            4<br />
            Declarations
          </li>
          <li style={{ color: props.primaryOne }}>
            5<br />
            Summary
          </li>
        </ul>
      </div>

      <div className="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div className="rightpanel clearfix">
          <h3 style={{ color: props.primaryOne }}>Loan Details</h3>
          <p>Please give us some details about the loan</p>
          <div className="form-group">
            <label className="form-label">Purchase Price ($)</label>
            <span style={{ color: props.primaryOne }}>$650,000</span>
          </div>
          <div className="clearfix">
            <div className="form-group col">
              <label className="form-label">Down Payment ($)</label>
              <span style={{ color: props.primaryOne }}>$0</span>
            </div>
            <div
              className="mid"
              style={{ borderLeft: `2px solid ${props.secondaryOne}` }}
            >
              &nbsp;
            </div>
            <div className="form-group col">
              <label className="form-label" />
              <span style={{ color: props.primaryOne }}>(%)</span>
            </div>
          </div>
          <div className="form-group">
            <label className="form-label">Loan Amount</label>
            <span style={{ color: props.primaryOne }}>$0</span>
          </div>
          <div className="pull-left backbtn">
            <span className="df df-back" /> &nbsp; BACK
          </div>
          <div className="pull-right">
            <button
              type="button"
              className="btn btn-xs"
              style={{ background: props.secondaryOne }}
            >
              Next
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
Preview.propTypes = {
  primaryOne: PropTypes.string.isRequired,
  primaryTwo: PropTypes.string.isRequired,
  secondaryOne: PropTypes.string.isRequired,
  secondaryTwo: PropTypes.string.isRequired,
  tertiary: PropTypes.string.isRequired,
};
export default Preview;
