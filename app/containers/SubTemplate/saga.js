/**
 * Created by sigma on 25/2/19.
 */
import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { postRequest, getRequest, putRequest } from 'utils/request';
import { subTemplate } from './selectors';
import { showErrorMessage } from 'components/ErrorMessage';

import {
  FETCH_ALL_TEMPLATE,
  ADD_NEW_SUB_TEMPLATE,
  UPDATE_TEMPLATE,
} from './constants';

import {
  apiFailure,
  storeTemplateList,
  storeNewTemplate,
  storeNewlyAddedTemplate,
} from './actions';

export function* getAllTemplate() {
  const base = yield select(serviceUrl('template'));
  const url = '/all';

  try {
    const allTemplate = yield call(getRequest, base, url);
    yield put(storeTemplateList(allTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* addNewTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  try {
    const newlyAddedTemplate = yield call(postRequest, base, url, updatedValue.newTemplate);
    yield put(storeNewlyAddedTemplate(newlyAddedTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* updateTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  const allSubTemplates = yield select(subTemplate());

  let payload = updatedValue.originalTemplate
  payload.body = updatedValue.body
  payload.isActive = updatedValue.status

  let pervAvailTemp = allSubTemplates.filter(e => e.name === payload.name)
  pervAvailTemp.sort((a, b) => parseFloat(a.version) - parseFloat(b.version));
  const highestVersion = pervAvailTemp.pop();
  const newVersion = (parseFloat(highestVersion.version) + 0.1).toFixed(1);
  payload.version = newVersion;

  try {
    //Check if the new template is active then deactivate previous activated template
    if (updatedValue.status) {
      const previousActivatedTemplate = allSubTemplates.filter(e => e.isActive && e.name === payload.name)
      if (previousActivatedTemplate.length > 0) {
        const deactivateUrl = [previousActivatedTemplate[0].name, previousActivatedTemplate[0].version, previousActivatedTemplate[0].format, 'deactivate'].join("/");
        yield call(postRequest, base, "/" + deactivateUrl, {})
      }
    }
    const newlyAddedTemplate = yield call(postRequest, base, url, payload);
    yield put(storeNewTemplate(newlyAddedTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* templateEndpoints() {
  yield all([
    takeLatest(FETCH_ALL_TEMPLATE, getAllTemplate),
    takeLatest(ADD_NEW_SUB_TEMPLATE, addNewTemplate),
    takeLatest(UPDATE_TEMPLATE, updateTemplate),
  ]);
}