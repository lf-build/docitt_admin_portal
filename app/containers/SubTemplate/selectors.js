/**
 * Created by sigma on 25/2/19.
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTemplate = state => state.get('subtemplate', initialState);

const templates = () =>
  createSelector(selectTemplate, templateState => templateState.getIn(['templateList']));

const subTemplate = () =>
  createSelector(selectTemplate, templateState => templateState.getIn(['subTemplateList']));

export {
  templates,
  subTemplate,
}