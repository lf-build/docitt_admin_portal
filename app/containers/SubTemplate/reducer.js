/**
 * Created by sigma on 25/2/19.
 */
import { fromJS } from 'immutable';
import {
  UPDATE_ALL_TEMPLATE,
  ADD_NEW_EMAIL_TEMPLATE,
  STORE_NEW_TEMPLATE,
  ADD_NEW_SUB_TEMPLATE,
  SELECT_TEMPLATE_STATUS,
  API_ERROR,
} from './constants';

export const initialState = fromJS({
  buttonEnable: true,
  templateList: [],
  subTemplateList: [],
  errorMessage: '',
});

function templateReducer(state = initialState, action) {
  switch (action.type) {
    case API_ERROR:
      return state.setIn(['errorMessage'], fromJS(action.error));

    case UPDATE_ALL_TEMPLATE:
      const selectedSubTemplate = action.list
        .filter(item => !item.isSystem)
        .map(template => {
          template.isSelected = false;
          return template;
        });
      return state.setIn(['subTemplateList'], selectedSubTemplate);

    case SELECT_TEMPLATE_STATUS:
      const templatedSelected = state.getIn(['subTemplateList']).map(item => {

        if (item.id === action.templateId) item.isSelected = true;
        else if (item.isSelected === true) item.isSelected = false;
        return item;
      });
      return state.setIn(['subTemplateList'], templatedSelected);

    case STORE_NEW_TEMPLATE:
      const templateList = state.get('subTemplateList').slice();
      templateList.splice(templateList.length , 0, action.newTemplate)
      return state.setIn(['subTemplateList'], templateList);

    default:
      return state;
  }
}

export default templateReducer;
