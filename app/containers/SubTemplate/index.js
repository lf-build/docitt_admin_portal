/**
 * Created by sigma on 13/2/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import 'react-confirm-alert/src/react-confirm-alert.css';
import 'react-quill/dist/quill.snow.css';
import './sub-template.css';
import { confirmAlert } from 'react-confirm-alert';
import Tooltip from 'components/ToolTip';
import Button from 'components/Button';
import TextView from 'components/TextView';
import Select from 'react-select';
import DataTable from 'components/DataTable';
import PopUp from 'components/PopUp';
import ReactQuill from 'react-quill';
import { run, ruleRunner } from 'Validation/ruleRunner';
import { required, checkSpace } from 'Validation/rules';
import { tenantVariables } from 'containers/Configuration/selectors';
import saga from './saga';
import reducer from './reducer';
import {
  fetchTemplateList,
  storeNewTemplate,
  updateSelectedTemplate,
} from './actions';

import {  templates,  subTemplate } from './selectors';

const subTemplateNameRule = [
  ruleRunner('subTemplateName', 'Sub Template Name', required, checkSpace),
];

const CustomToolbar = ( { onClickRaw } ) => (
  <div id="custom-toolbar">
    <button onClick={onClickRaw}>[ Source ]</button>
  </div>
);

class SubTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      popupVisibility: false,
      selectDisbaled: false,
      addingNewTemplate: false,
      updatingTemplate: false,
      showErrors: true,
      showEmailRaw: false,
      rawEmailHtml: '',
      popupTitle: '',
      subTemplateText: '',
      subTemplateName: '',
      subTemplatePlainText: '',
      rowId: '',
      blurIndex: 0,
      testCase: '',
      options: [
        { value: 'Header', label: 'Header' },
        { value: 'Footer', label: 'Footer' },
        { value: 'Signature', label: 'Signature' },
        { value: 'CustomerService', label: 'Customer Service' },
        { value: 'ContactInformation', label: 'Contact Information' },
        { value: 'LegalDepartment', label: 'Legal Department' },
        { value: 'Custom', label: 'Custom' },
      ],
      selectedOption: { value: 'Header', label: 'Header' },
      validationErrors: {},
    };
    this.quillRef = null;
    this.reactQuillRef = null;
  }

  componentDidMount() {
    this.props.fetchTemplateList();
  }

  modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ color: [] }, { background: [] }],
      [{ list: 'ordered' }],
      [{ align: [] }],
      [{ indent: '-1' }, { indent: '+1' }],
      ['link', 'image'],
    ],
  };

  formats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'color',
    'background',
    'list',
    'bullet',
    'align',
    'indent',
    'link',
    'image',
  ];

  addNewSubTemplate = () => {
    this.setState({
      popupVisibility: true,
      addingNewTemplate: true,
      popupTitle: 'Add new sub-template',
      selectDisbaled: false,
      // showErrors: false,
      showEmailRaw: false,
    });
  };

  handleClickShowRawEmail = value  => {
    const isEditingRaw = this.state.showEmailRaw;
    this.setState({ showEmailRaw: !isEditingRaw });
    this.syncViewsEmail(isEditingRaw);
  };

  syncViewsEmail = fromRaw => {
    if (fromRaw) this.setState({ subTemplateText: this.state.rawEmailHtml });
    else this.setState({ rawEmailHtml: this.state.subTemplateText });
  };

  handleChangeEmailRaw(html) {
    this.setState({ rawEmailHtml: html });
  }

  rowClickHandler = rowData => {
    this.props.updateSelectedTemplate(rowData.rowId);
  };

  handleChange = (content, delta, source, editor) => {
    // to get plain text from template body
    const text = editor.getText(content);
    this.setState({
      subTemplateText: content,
      subTemplatePlainText: text.trim(),
    });
    if(this.state.testCase === ''){
      this.setState({
        testCase: text.trim()
      })
    }
    if (
      this.state.subTemplateName !== '' &&
      this.state.subTemplatePlainText !== ''
    ) {
      this.setState({
        showErrors: false,
      });
    } else {
      this.setState({
        showErrors: true,
      });
    }
  };

  handleFocus = previousRange => {
    this.setState({ blurIndex: previousRange.index });
  };

  cancelTemplate = () => {
    if ((this.state.subTemplateName !== '' && this.state.subTemplatePlainText!==''&& this.state.testCase.length !== 1 && this.state.testCase === this.state.subTemplatePlainText) ||
      (this.state.subTemplateName === '' &&
      this.state.subTemplatePlainText === '')
    ) {
      this.hidePopup();
    } else {
      confirmAlert({
        title: 'Are you sure you want to cancel?', // Title dialog
        message: 'All your changes will not be save?', // Message dialog
        childrenElement: () => <div>Custom UI</div>, // Custom UI or Component
        buttons: [
          {
            label: 'No',
          },
          {
            label: 'Yes',
            onClick: () => this.hidePopup(),
          },
        ],
      });
    }
  };

  hidePopup = () => {
    this.setState({
      popupVisibility: false,
      showErrors: true,
      popupTitle: '',
      subTemplateText: '',
      subTemplateName: '',
      subTemplatePlainText: '',
      validationErrors: {},
      testCase: '',
      selectedOption: { value: 'Header', label: 'Header' },
    });
  };

  saveTemplate = () => {
    if (this.state.addingNewTemplate) {
      this.setState({ showErrors: true });
      if (
        this.props.subTemplate.some(
          e =>
            e.name.toLowerCase() === this.state.subTemplateName.toLowerCase(),
        )
      ) {
        this.state.validationErrors = {
          subTemplateName: 'This Template Name is already present.',
        };
      }
      if (
        (Object.keys(this.state.validationErrors).length === 0 &&
          this.state.validationErrors.constructor === Object) === false
      )
        return null;
      // storing a new template

      const payload = {
        name: this.state.subTemplateName,
        templateType: this.state.selectedOption.value,
        body: this.state.showEmailRaw
          ? this.state.rawEmailHtml
          : this.state.subTemplateText,
        title: '',
        format: 'Html',
        version: '1.0',
        isActive: true,
        isSystem: false,
      };
      this.props.storeNewTemplate(payload);
      this.setState({
        popupVisibility: false,
        addingNewTemplate: false,
        showErrors: false,
        popupTitle: '',
        subTemplateText: '',
        subTemplateName: '',
        selectedOption: { value: 'Header', label: 'Header' },
      });
    }

    if (this.state.updatingTemplate) {
      // storing a new template
      const pervAvailTemp = this.props.subTemplate.filter(
        e => e.name === this.state.subTemplateName,
      );
      pervAvailTemp.sort(
        (a, b) => parseFloat(a.version) - parseFloat(b.version),
      );
      const highestVersion = pervAvailTemp.pop();

      const newVersion = (parseFloat(highestVersion.version) + 0.1).toFixed(1);
      const payload = {
        name: this.state.subTemplateName,
        templateType: this.state.selectedOption.value,
        body: this.state.showEmailRaw
          ? this.state.rawEmailHtml
          : this.state.subTemplateText,
        title: '',
        format: 'Html',
        version: newVersion,
        isActive: true,
        isSystem: false,
      };
      this.props.storeNewTemplate(payload);

      this.setState({
        popupVisibility: false,
        updatingTemplate: false,
        popupTitle: '',
        subTemplateText: '',
        subTemplateName: '',
        selectedOption: { value: 'Header', label: 'Header' },
      });
    }
  };
  rowSelected = rowData => {
    const bodyHtml = rowData.body;
    const selectedOption = this.state.options.filter(
      e => e.value === rowData.templateType,
    );

    this.setState({
      updatingTemplate: true,
      popupVisibility: true,
      popupTitle: `${rowData.templateType} >> ${rowData.name} >> ${
        rowData.version
      }`,
      subTemplateText: bodyHtml,
      subTemplateName: rowData.name,
      selectedOption: selectedOption[0],
      rowId: rowData.rowId,
      selectDisbaled: true,
      showErrors: false,
      showEmailRaw: false,
      version: rowData.version,
    });
  };

  handleFieldChanged =  value  => e => {
    this.setState({ subTemplateName: e.target.value });
    this.validateField(e.target.value);
  };

  validateField = value => {
    const fieldValues = {
      subTemplateName: value,
    };
    this.state.validationErrors = run(fieldValues, subTemplateNameRule);
    if (
      (Object.keys(this.state.validationErrors).length === 0 &&
        this.state.subTemplatePlainText !== '' &&
        this.state.validationErrors.constructor === Object) === false
    ) {
      this.setState({ showErrors: true });
    } else {
      this.setState({ showErrors: false });
    }
  };

  changSubTemplateType = value => {
    this.setState({ selectedOption: value });
  };

  errorFor(field) {
    return this.state.validationErrors[field] || '';
  }

  formatDate = (timeStamp) => {
    var fullDate =  new Date(timeStamp)
    var month = ('0' + (fullDate.getMonth() + 1)).slice(-2);
    var date = ('0' + fullDate.getDate()).slice(-2);
    var year = fullDate.getFullYear();
    return month + '.' + date + '.' + year;
  }

  render() {
    const { subTemplate } = this.props;
    const {
      popupVisibility,
      popupTitle,
      subTemplateText,
      showErrors,
      subTemplateName,
      selectDisbaled,
    } = this.state;

    const header = [
      { header: '#', name: 'id' },
      { header: 'Type', name: 'templateType', searchable: true, sortable: true  },
      { header: 'Sub-Template Name', name: 'name', searchable: true, sortable: true},
      { header: 'Version', name: 'version' },
      { header: 'Created On', name: 'updatedDate' },
    ];

    const data = subTemplate.map((template, index) => ({
      id: index + 1,
      body: template.body,
      title: template.title,
      templateType: template.templateType,
      properties: template.properties,
      format: template.format,
      name: template.name,
      version: template.version,
      status: template.isActive ? 'Active' : 'Inactive',
      updatedDate: template.createdDate ? this.formatDate(template.createdDate.time) : "",
      rowId: template.id,
      isSystem: template.isSystem,
      isSelected: template.isSelected,
    }));

    return (
      <div className="dashboard clearfix">
        <div className="headertitle">Sub-Templates</div>
        <div className="padd clearfix">
          <div className="subtemp-icon">
            <Tooltip position="left" message="New Sub-Template.">
              <button className="document-icn" onClick={this.addNewSubTemplate}>
                &nbsp;
              </button>
            </Tooltip>
          </div>
          <DataTable
            headings={header}
            rows={data}
            rowClick={this.rowClickHandler}
            rowSelected={this.rowSelected}
          />
        </div>
        <PopUp visible={popupVisibility} title={popupTitle}>
          <div>
            <div className="col-lg-4 col-md-4 col-sm-6">
              <div className="subtemp-type">
                <span>Sub-Template Type</span>
                <Select
                  value={this.state.selectedOption}
                  onChange={this.changSubTemplateType}
                  options={this.state.options}
                  isSearchable={false}
                  isDisabled={this.state.selectDisbaled}
                />
              </div>
              {selectDisbaled && (
                <div className="subtemp-type" style={{ color: '#00a7f8' }}>
                  <span>Template Name</span>
                  {subTemplateName}
                </div>
              )}
              {!selectDisbaled && (
                <TextView
                  name="subTemplateName"
                  placeholder="Template Name"
                  type="text"
                  showError={showErrors}
                  text={subTemplateName}
                  onFieldChanged={this.handleFieldChanged('subTemplateName')}
                  errorText={this.errorFor('subTemplateName')}
                />
              )}
            </div>
            <div className="col-lg-8 col-md-8 col-sm-6">
              <h3>Template Body</h3>
              <div
                className={
                  this.state.showEmailRaw ? 'editor showEmailRaw' : 'editor'
                }
              >
                <CustomToolbar onClickRaw={this.handleClickShowRawEmail} />
                <ReactQuill
                  value={subTemplateText}
                  ref={el => {
                    this.reactQuillRef = el;
                  }}
                  modules={this.modules}
                  formats={this.formats}
                  onChange={this.handleChange}
                  onBlur={this.handleFocus}
                  className="information-holder"
                />
                <textarea
                  className="raw-email-editor"
                  onChange={e => this.handleChangeEmailRaw(e.target.value)}
                  value={this.state.rawEmailHtml}
                />
              </div>
              <div className="btnpreview pull-right">
                <Button
                  type="button"
                  buttonSize="btn-sm"
                  buttonType="btn-outline"
                  clickHandler={this.cancelTemplate}
                  text="Cancel"
                />
                <Button
                  type="button"
                  buttonSize="btn-sm"
                  disabled={showErrors}
                  clickHandler={this.saveTemplate}
                  text="Save"
                />
              </div>
            </div>
          </div>
        </PopUp>
      </div>
    );
  }
}

SubTemplate.propTypes = {
  updateSelectedTemplate: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  subTemplate: subTemplate(),
});

const mapDispatchToProps = dispatch => ({
  fetchTemplateList: () => {
    dispatch(fetchTemplateList());
  },
  storeNewTemplate: payload => {
    dispatch(storeNewTemplate(payload));
  },
  updateSelectedTemplate: templateId => {
    dispatch(updateSelectedTemplate(templateId));
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'subtemplate', reducer });
const withSaga = injectSaga({ key: 'subtemplate', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SubTemplate);
