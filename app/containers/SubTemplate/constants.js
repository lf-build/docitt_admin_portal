/**
 * Created by sigma on 25/2/19.
 */
export const FETCH_ALL_TEMPLATE = 'lendfoundry/subtemplate/FETCH_ALL_TEMPLATE';
export const UPDATE_ALL_TEMPLATE = 'lendfoundry/subtemplate/UPDATE_ALL_TEMPLATE';
export const UPDATE_EMAIL_TEMPLATE =
  'lendfoundry/subtemplate/UPDATE_EMAIL_TEMPLATE';
export const ADD_NEW_EMAIL_TEMPLATE =
  'lendfoundry/subtemplate/ADD_NEW_EMAIL_TEMPLATE';
export const REVERT_DEFAULT_EMAIL_TEMPLATE =
  'lendfoundry/subtemplate/REVERT_DEFAULT_EMAIL_TEMPLATE';
export const UPDATE_INFORMATION_TEMPLATE =
  'lendfoundry/subtemplate/UPDATE_INFORMATION_TEMPLATE';
export const UPDATE_SMS_TEMPLATE = 'lendfoundry/subtemplate/UPDATE_SMS_TEMPLATE';
export const SELECT_TEMPLATE_STATUS =
  'lendfoundry/subtemplate/SELECT_TEMPLATE_STATUS';

export const ADD_NEW_SUB_TEMPLATE = 'lendfoundry/subtemplate/ADD_NEW_SUB_TEMPLATE';
export const STORE_NEW_TEMPLATE = 'lendfoundry/subtemplate/STORE_NEW_TEMPLATE';
export const UPDATE_TEMPLATE = 'lendfoundry/template/UPDATE_TEMPLATE';
export const API_ERROR = 'lendfoundry/template/API_ERROR';
