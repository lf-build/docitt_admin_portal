/**
 * Created by sigma on 13/2/19.
 */
/**
 * Asynchronously loads the component for NotFoundPage
 */
import loadable from '@loadable/component';

export default loadable(() => import('./index'));
