/**
 * Created by sigma on 22/11/18.
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAuthorised = state => state.get('authorised', initialState);

const errorLogin = () =>
  createSelector(selectAuthorised, selectAuthorised =>
    selectAuthorised.get('error'),
  );

const errorLoginMessage = () =>
  createSelector(selectAuthorised, selectAuthorised =>
    selectAuthorised.get('errorMessage'),
  );

const btnStatus = () =>
  createSelector(selectAuthorised, selectAuthorised =>
    selectAuthorised.get('btnDisabled'),
  );

const isAuthorised = () =>
  createSelector(selectAuthorised, selectAuthorised =>
    selectAuthorised.get('isAuthorised'),
  );

export {
  selectAuthorised,
  errorLogin,
  errorLoginMessage,
  btnStatus,
  isAuthorised,
};
