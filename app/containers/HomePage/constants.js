export const USER_LOGIN = 'lendfoundry/Homepage/USER_LOGIN';

export const USER_LOGIN_SUCCESS = 'lendfoundry/Homepage/USER_LOGIN_SUCCESS';

export const USER_LOGIN_FAILURE = 'lendfoundry/Homepage/USER_LOGIN_FAILURE';

export const USER_LOGGED_OUT = 'lendfoundry/Homepage/USER_LOGGED_OUT';

export const USER_LOG_OUT = 'lendfoundry/Homepage/USER_LOG_OUT';

export const LOGIN_BUTTON_DISABLE = 'lendfoundry/Homepage/LOGIN_BUTTON_DISABLE';
