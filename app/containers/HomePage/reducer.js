/**
 * Created by sigma on 22/11/18.
 */
import { fromJS } from 'immutable';

import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGGED_OUT,
  LOGIN_BUTTON_DISABLE,
  USER_LOG_OUT,
} from './constants';

// The initial state of the App
const isAuthorised = sessionStorage.getItem('isAuthorised');
export const initialState = fromJS({
  error: false,
  errorMessage: {},
  btnDisabled: false,
  isAuthorised: !!isAuthorised,
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_BUTTON_DISABLE:
      return state.set('btnDisabled', true);

    case USER_LOGIN_FAILURE:
      return state
        .set('errorMessage', action.errorMsg)
        .set('error', true)
        .set('btnDisabled', false);

    case USER_LOGIN_SUCCESS:
      sessionStorage.setItem('token', action.response.token);
      sessionStorage.setItem('isAuthorised', true);
      return state
        .set('loading', false)
        .set('isAuthorised', true)
        .set('btnDisabled', false);

    case USER_LOGGED_OUT:
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('isAuthorised');
      return state.set('isAuthorised', false).set('loading', false);

    case USER_LOG_OUT:
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('isAuthorised');
      return state.set('isAuthorised', false).set('loading', false);

    default:
      return state;
  }
}

export default homePageReducer;
