import {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOG_OUT,
  LOGIN_BUTTON_DISABLE,
} from './constants';

export function login(userName, password) {
  return {
    type: USER_LOGIN,
    userName,
    password,
  };
}

export function loginSuccess(resposnse) {
  return {
    type: USER_LOGIN_SUCCESS,
    response: resposnse,
  };
}

export function loginFailure(error) {
  return {
    type: USER_LOGIN_FAILURE,
    errorMsg: error,
  };
}

export function disableLoginBtn() {
  return {
    type: LOGIN_BUTTON_DISABLE,
  };
}

export function userLogOut() {
  return {
    type: USER_LOG_OUT,
  };
}
