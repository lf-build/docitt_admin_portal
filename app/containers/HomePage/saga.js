import { call, put, select, takeLatest } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';

import { postRequest } from 'utils/request';
import { loginSuccess, loginFailure } from './actions';
import { USER_LOGIN } from './constants';

export function* makeUserLogin(payload) {
  let base = '';
  let url = '';
  if (process.env.NODE_ENV === 'production') {
    base =
      window.location.origin || 'http://localhost:9003/';
    url = '/security/login/';
  } else {
    base = yield select(serviceUrl('security'));
    url = '/login/';
  }

  const request = {
    username: payload.userName,
    password: payload.password,
    portal: 'admin-portal',
  };

  try {
    const config = yield call(postRequest, base, url, request);
    yield put(loginSuccess(config));
  } catch (err) {
    yield put(loginFailure(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* userLogin() {
  yield takeLatest(USER_LOGIN, makeUserLogin);
}
