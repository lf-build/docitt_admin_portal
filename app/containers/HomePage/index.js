/*
 * HomePage
 *
 * This is the first thing users Login of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import {Redirect} from 'react-router-dom';

import ErrorMessage from 'components/ErrorMessage';
import {userLoggedOut} from 'containers/HeartBeat/actions';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import TextView from 'components/TextView';
import Button from 'components/Button';
import update from 'immutability-helper';
import {run, ruleRunner} from 'Validation/ruleRunner';
import {required, validEmail} from 'Validation/rules';
import {login, disableLoginBtn} from './actions';
import {
  errorLogin,
  errorLoginMessage,
  isAuthorised,
  btnStatus,
} from './selectors';
import reducer from './reducer';
import saga from './saga';

const fieldValidations = [
  ruleRunner('userName', 'Username', validEmail),
  ruleRunner('password', 'Password', required),
];

export class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.handleFieldChanged = this.handleFieldChanged.bind(this);
    this.handleSubmitClicked = this.handleSubmitClicked.bind(this);
    this.errorFor = this.errorFor.bind(this);
    this.state = {
      showErrors: false,
      validationErrors: {},
    };
    // reset login status
    this.props.dispatch(userLoggedOut());
  }

  componentWillMount() {
    // Run validations on initial state
    this.setState({validationErrors: run(this.state, fieldValidations)});
  }

  errorFor(field) {
    return this.state.validationErrors[field] || '';
  }

  handleFieldChanged(field) {
    return e => {
      const newState = update(this.state, {
        [field]: {$set: e.target.value},
      });
      newState.validationErrors = run(newState, fieldValidations);
      this.setState(newState);
    };
  }

  handleSubmitClicked() {
    this.setState({showErrors: true});
    if (
      (Object.keys(this.state.validationErrors).length === 0 &&
      this.state.validationErrors.constructor === Object) === false
    )
      return null;

    // Validation Passed
    this.props.dispatch(disableLoginBtn());
    this.props.dispatch(login(this.state.userName, this.state.password));
  }

  render() {
    if (this.props.isAuthorised) {
      return <Redirect to="/dashboard" push/>;
    }
    return (
      <div className="flex-wrap">
        <div className="login">
          <h1 className="text-center style-scope security-login">
            Welcome Back
          </h1>
          <p className="text-center style-scope security-login">
            We offer one intuitive place to manage and upload all your mortgage
            information.
          </p>

          {this.props.error && (
            <ErrorMessage errorMessages={this.props.errorMessage}/>
          )}
          <TextView
            name="username"
            placeholder="Username"
            showError={this.state.showErrors}
            type="text"
            text={this.props.userName}
            onFieldChanged={this.handleFieldChanged('userName')}
            errorText={this.errorFor('userName')}
          />
          <TextView
            name="password"
            placeholder="Password"
            showError={this.state.showErrors}
            type="password"
            text={this.props.password}
            onFieldChanged={this.handleFieldChanged('password')}
            errorText={this.errorFor('password')}
          />
          <Button
            type="button"
            buttonSize="btn-block"
            disabled={this.props.btnDisabled}
            clickHandler={this.handleSubmitClicked}
            text="Login"
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  error: errorLogin(),
  errorMessage: errorLoginMessage(),
  btnDisabled: btnStatus(),
  isAuthorised: isAuthorised(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

const withReducer = injectReducer({key: 'authorised', reducer});
const withSaga = injectSaga({key: 'authorised', saga});

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
