/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import Alert from 'react-s-alert';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import HomePage from 'containers/HomePage';
import Dashboard from 'containers/Dashboard';
import Header from 'components/Header';
import LeftNavigation from 'components/LeftNavigation';
import PrivateRoute from 'components/PrivateRoute';
import HeartBeat from 'containers/HeartBeat';
import Colors from 'containers/Colors/Loadable';
import Fonts from 'containers/Fonts/Loadable';
import Graphics from 'containers/Graphics/Loadable';
import Template from 'containers/Template/Loadable';
import SubTemplate from 'containers/SubTemplate/Loadable';
import Configuration from 'containers/Configuration';
import { validateToken, logoutUser } from 'containers/HeartBeat/actions';
import { isAuthorised } from 'containers/HomePage/selectors';
import { configurationloaded, logoPresent, logoDisplay, logoDetails } from 'containers/Configuration/selectors';
import reducer from 'containers/HeartBeat/reducer';
import saga from 'containers/HeartBeat/saga';
import Button from 'components/Button';
import GlobalStyle from '../../global-styles';

class App extends React.Component {
  componentDidMount() {
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    this.props.logoutUser();
  }

  render() {
    if (this.props.isAuthorised === false) {
      <Redirect to="/" push />;
    }
    if (this.props.configurationloaded) {
      this.props.validateToken();
    }

    return (
      <div className="full-height">
        <Configuration />
        {this.props.configurationloaded && (
          <div>
            <Header logoPresent={this.props.logoPresent} logoDetails={this.props.logoDetails} logoDisplay={this.props.logoDisplay} isAuthorised={this.props.isAuthorised}>
              <Button
                type="button"
                buttonSize="btn-small"
                clickHandler={this.handleLogout}
                text="Logout"
              />
            </Header>
            <HeartBeat isAuthorised={this.props.isAuthorised} />
            <Router>
              <div className="full-height">
                {this.props.isAuthorised && <LeftNavigation />}
                <Route exact strict path="/" component={HomePage} />
                <PrivateRoute
                  exact
                  strict
                  path="/dashboard"
                  component={Dashboard}
                />
                <PrivateRoute exact strict path="/colors" component={Colors} />
                <PrivateRoute exact strict path="/fonts" component={Fonts} />
                <PrivateRoute
                  exact
                  strict
                  path="/graphics"
                  component={Graphics}
                />
                <PrivateRoute
                  exact
                  strict
                  path="/template"
                  component={Template}
                />
                <PrivateRoute
                  exact
                  strict
                  path="/sub-template"
                  component={SubTemplate}
                />
              </div>
            </Router>
          </div>
        )}
        <GlobalStyle />
        <Alert
          stack={{ limit: 3 }}
          timeout={5000}
          position="top-right"
          effect="jelly"
          offset={100}
        />
      </div>
    );
  }
}
App.propTypes = {
  logoutUser: PropTypes.func,
  validateToken: PropTypes.func,
  isAuthorised: PropTypes.bool,
  configurationloaded: PropTypes.bool,
  logoPresent: PropTypes.bool,
  logoDisplay: PropTypes.bool,
  logoDetails: PropTypes.object,
};
const mapStateToProps = createStructuredSelector({
  isAuthorised: isAuthorised(),
  configurationloaded: configurationloaded(),
  logoPresent: logoPresent(),
  logoDisplay: logoDisplay(),
  logoDetails: logoDetails(),
});

const mapDispatchToProps = dispatch => ({
  validateToken: () => {
    const token = sessionStorage.getItem('token');
    if (!token || token === '') {
      // User not yet signed in
      return;
    }
    dispatch(validateToken());
  },
  logoutUser: () => {
    dispatch(logoutUser());
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'heartBeat', reducer });
const withSaga = injectSaga({ key: 'heartBeat', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(App);
