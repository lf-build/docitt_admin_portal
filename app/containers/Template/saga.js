/**
 * Created by sigma on 25/2/19.
 */
import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { postRequest, getRequest, putRequest } from 'utils/request';
import { templates } from './selectors';
import { showErrorMessage } from 'components/ErrorMessage';

import {
  FETCH_ALL_TEMPLATE,
  UPDATE_EMAIL_TEMPLATE,
  REVERT_DEFAULT_EMAIL_TEMPLATE,
  UPDATE_INFORMATION_TEMPLATE,
  UPDATE_SMS_TEMPLATE,
} from './constants';

import {
  apiFailure,
  storeTemplateList,
  storeNewTemplate,
  updateTemplateStatus,
  reinStateDefault,
} from './actions';

export function* getAllTemplate() {
  const base = yield select(serviceUrl('template'));
  const url = '/all';

  try {
    const allTemplate = yield call(getRequest, base, url);
    yield put(storeTemplateList(allTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* updateEmailTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  const allTemplates = yield select(templates());

  let payload = updatedValue.originalTemplate
  payload.body = updatedValue.body
  payload.title = updatedValue.title
  payload.isActive = updatedValue.status

  let pervAvailTemp = allTemplates.filter(e => e.name === payload.name)
  pervAvailTemp.sort((a, b) => parseFloat(a.version) - parseFloat(b.version));
  const highestVersion = pervAvailTemp.pop();
  const newVersion = (parseFloat(highestVersion.version) + 0.1).toFixed(1);
  payload.version = newVersion;

  try {
    //Check if the new template is active then deactivate previous activated template
    if (updatedValue.status) {
      const previousActivatedTemplate = allTemplates.filter(e => e.isActive && e.name === payload.name)
      if (previousActivatedTemplate.length > 0) {
        const deactivateUrl = [previousActivatedTemplate[0].name, previousActivatedTemplate[0].version, previousActivatedTemplate[0].format, 'deactivate'].join("/");
        const deactivatedTemplate = yield call(postRequest, base, "/" + deactivateUrl, {})
        yield put(updateTemplateStatus(previousActivatedTemplate[0].id))
      }
    }
    const newlyAddedTemplate = yield call(postRequest, base, url, payload);
    yield put(storeNewTemplate(newlyAddedTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* updateInfoTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  const allTemplates = yield select(templates());

  let payload = updatedValue.originalTemplate
  payload.body = updatedValue.body
  payload.isActive = updatedValue.status

  let pervAvailTemp = allTemplates.filter(e => e.name === payload.name)
  pervAvailTemp.sort((a, b) => parseFloat(a.version) - parseFloat(b.version));
  const highestVersion = pervAvailTemp.pop();
  const newVersion = (parseFloat(highestVersion.version) + 0.1).toFixed(1);
  payload.version = newVersion;

  try {
    //Check if the new template is active then deactivate previous activated template
    if (updatedValue.status) {
      const previousActivatedTemplate = allTemplates.filter(e => e.isActive && e.name === payload.name)
      if (previousActivatedTemplate.length > 0) {
        const deactivateUrl = [previousActivatedTemplate[0].name, previousActivatedTemplate[0].version, previousActivatedTemplate[0].format, 'deactivate'].join("/");
        const deactivatedTemplate = yield call(postRequest, base, "/" + deactivateUrl, {})
        yield put(updateTemplateStatus(previousActivatedTemplate[0].id))
      }
    }
    const newlyAddedTemplate = yield call(postRequest, base, url, payload);
    yield put(storeNewTemplate(newlyAddedTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* updateSmsTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  const allTemplates = yield select(templates());

  let payload = updatedValue.originalTemplate
  payload.body = updatedValue.body
  payload.isActive = updatedValue.status

  let pervAvailTemp = allTemplates.filter(e => e.name === payload.name)
  pervAvailTemp.sort((a, b) => parseFloat(a.version) - parseFloat(b.version));
  const highestVersion = pervAvailTemp.pop();
  const newVersion = (parseFloat(highestVersion.version) + 0.1).toFixed(1);
  payload.version = newVersion;

  try {
    //Check if the new template is active then deactivate previous activated template
    if (updatedValue.status) {
      const previousActivatedTemplate = allTemplates.filter(e => e.isActive && e.name === payload.name)
      if (previousActivatedTemplate.length > 0) {
        const deactivateUrl = [previousActivatedTemplate[0].name, previousActivatedTemplate[0].version, previousActivatedTemplate[0].format, 'deactivate'].join("/");
        const deactivatedTemplate = yield call(postRequest, base, "/" + deactivateUrl, {})
        yield put(updateTemplateStatus(previousActivatedTemplate[0].id))
      }
    }
    const newlyAddedTemplate = yield call(postRequest, base, url, payload);
    yield put(storeNewTemplate(newlyAddedTemplate));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* revertTemplate(updatedValue) {
  const base = yield select(serviceUrl('template'));
  const url = '/all';
  const allTemplates = yield select(templates());

  try {
    let previousActivatedTemplate = allTemplates.filter(e => e.name === updatedValue.originalTemplate.name && e.version == "1.0")
    if (previousActivatedTemplate.length === 0) {
      previousActivatedTemplate = allTemplates.filter(e => e.name === updatedValue.originalTemplate.name && e.version == "1.1")
    }

    if (previousActivatedTemplate) {
      const activateUrl = [previousActivatedTemplate[0].name, previousActivatedTemplate[0].version, previousActivatedTemplate[0].format, 'activate'].join("/");
      yield call(postRequest, base, "/" + activateUrl, {})
      yield put(reinStateDefault(previousActivatedTemplate[0].name, previousActivatedTemplate[0].version))
    }
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* templateEndpoints() {
  yield all([
    takeLatest(FETCH_ALL_TEMPLATE, getAllTemplate),
    takeLatest(UPDATE_EMAIL_TEMPLATE, updateEmailTemplate),
    takeLatest(REVERT_DEFAULT_EMAIL_TEMPLATE, revertTemplate),
    takeLatest(UPDATE_INFORMATION_TEMPLATE, updateInfoTemplate),
    takeLatest(UPDATE_SMS_TEMPLATE, updateSmsTemplate),
  ]);
}