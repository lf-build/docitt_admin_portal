/**
 * Created by sigma on 25/2/19.
 */
import {
  FETCH_ALL_TEMPLATE,
  UPDATE_ALL_TEMPLATE,
  UPDATE_EMAIL_TEMPLATE,
  ADD_NEW_EMAIL_TEMPLATE,
  REVERT_DEFAULT_EMAIL_TEMPLATE,
  UPDATE_INFORMATION_TEMPLATE,
  UPDATE_SMS_TEMPLATE,
  UPDATE_TEMPLATE_STATUS,
  SELECT_TEMPLATE_STATUS,
  REINSTATE_DEFAULT_TEMPLATE,
  API_ERROR,
} from './constants';

export function fetchTemplateList() {
  return {
    type: FETCH_ALL_TEMPLATE,
  };
}

export function storeTemplateList(templateList) {
  return {
    type: UPDATE_ALL_TEMPLATE,
    list: templateList,
  };
}

export function updateEmailTemplate(
  selectedEmailTemplate,
  emailText,
  emailSubject,
  emailDefaultStatus,
) {
  return {
    type: UPDATE_EMAIL_TEMPLATE,
    originalTemplate: selectedEmailTemplate,
    body: emailText,
    title: emailSubject,
    status: emailDefaultStatus,
  };
}
export function updateInformationTemplate(
  selectedInfoTemplate,
  infoText,
  infoDefaultStatus,
) {
  return {
    type: UPDATE_INFORMATION_TEMPLATE,
    originalTemplate: selectedInfoTemplate,
    body: infoText,
    status: infoDefaultStatus,
  };
}

export function updateSmsTemplate(
  selectedSmsTemplate,
  smsText,
  smsDefaultStatus,
) {
  return {
    type: UPDATE_SMS_TEMPLATE,
    originalTemplate: selectedSmsTemplate,
    body: smsText,
    status: smsDefaultStatus,
  };
}

export function revertEmailTemplate(selectedEmailTemplate) {
  return {
    type: REVERT_DEFAULT_EMAIL_TEMPLATE,
    originalTemplate: selectedEmailTemplate,
  };
}

export function storeNewTemplate(template) {
  return {
    type: ADD_NEW_EMAIL_TEMPLATE,
    newTemplate: template,
  };
}

export function updateTemplateStatus(id) {
  return {
    type: UPDATE_TEMPLATE_STATUS,
    templateId: id,
  };
}

export function updateSelectedTemplate(id) {
  return {
    type: SELECT_TEMPLATE_STATUS,
    templateId: id,
  };
}

export function reinStateDefault(template, templateVersion) {
  return {
    type: REINSTATE_DEFAULT_TEMPLATE,
    templateName: template,
    version: templateVersion,
  };
}

export function apiFailure(err) {
  return {
    type: API_ERROR,
    error: err,
  };
}
