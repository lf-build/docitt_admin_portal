/**
 * Created by sigma on 13/2/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import 'react-confirm-alert/src/react-confirm-alert.css';
import 'react-quill/dist/quill.snow.css';
import './template.css';
import { confirmAlert } from 'react-confirm-alert';

import Button from 'components/Button';
import Switch from 'components/SwitchView';
import TextView from 'components/TextView';
import DataTable from 'components/DataTable';
import PopUp from 'components/PopUp';
import ReactQuill from 'react-quill';
import { tenantVariables } from 'containers/Configuration/selectors';
import saga from './saga';
import reducer from './reducer';
import {
  fetchTemplateList,
  updateEmailTemplate,
  revertEmailTemplate,
  updateInformationTemplate,
  updateSmsTemplate,
  updateSelectedTemplate,
} from './actions';

import { templates, subTemplate } from './selectors';


const CustomToolbar = ({onClickRaw}) => (
  <div id="custom-toolbar">
    <button onClick={onClickRaw}>[ Source ]</button>
  </div>
)

class Template extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      templateTypeFilter: [{value: '', label: 'All'}, {value: 'email', label: 'Email'}, {
        value: 'information',
        label: 'Information'
      }, {value: 'sms', label: 'Sms'}],
      statusFilter: [{value: '', label: 'All'}, {value: 'active', label: 'Active'}, {
        value: 'inactive',
        label: 'Inactive'
      }],
      selectedEmailTemplate: {},
      emailPopupVisibility: false,
      revertEmailToDefaultValue: false,
      showErrors: false,
      emailText: '',
      emailFromAddress: [],
      emailTemplateVariable: [],
      emailBlurIndex: 0,
      emailSubject: '',
      emailPopupTitle: '',
      emailDefaultStatus: true,
      showEmailRaw: false,
      rawEmailHtml: '',
      informationPopupVisibility: false,
      selectedInformationTemplate: {},
      informationText: '',
      informationBlurIndex: 0,
      revertInformationToDefaultValue: false,
      informationDefaultStatus: true,
      showInformationRaw: false,
      rawInformationHtml: '',
      informationPopupTitle: '',
      informationTemplateVariable: [],
      smsPopupVisibility: false,
      selectedSmsTemplate: {},
      revertSmsToDefaultValue: false,
      smsText: '',
      smsFromAddress: [],
      smsTemplateVariable: [],
      smsBlurIndex: 0,
      smsSubject: '',
      smsPopupTitle: '',
      smsDefaultStatus: true,
      showSmsRaw: false,
      rawSmsHtml: '',
      counter: 0,
      flag: false,
      isDisabled: true,
    };
    this.quillRef = null;
    this.reactQuillRef = null;

    this.quillInfoRef = null;
    this.reactQuillInfoRef = null;

    this.quillSmsRef = null;
    this.reactQuillSmsRef = null;
  }

  componentDidMount() {
    this.props.fetchTemplateList();
    this.attachQuillRefs();
  }

  attachQuillRefs = () => {
    if (typeof this.reactQuillRef.getEditor !== 'function') return;
    this.quillRef = this.reactQuillRef.getEditor();

    if (typeof this.reactQuillInfoRef.getEditor !== 'function') return;
    this.quillInfoRef = this.reactQuillInfoRef.getEditor();

    if (typeof this.reactQuillSmsRef.getEditor !== 'function') return;
    this.quillSmsRef = this.reactQuillSmsRef.getEditor();
  };

  modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ color: [] }, { background: [] }],
      [{ list: 'ordered' }],
      [{ align: [] }],
      [{ indent: '-1' }, { indent: '+1' }],
      ['link', 'image'],
    ],
  };

  formats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'color',
    'background',
    'list',
    'bullet',
    'align',
    'indent',
    'link',
    'image',
  ];

  handleClickShowRawEmail = (value) => {
    const isEditingRaw = this.state.showEmailRaw;
    this.setState({showEmailRaw: !isEditingRaw})
    this.syncViewsEmail(isEditingRaw)
  }

  syncViewsEmail = fromRaw => {
    if (fromRaw) this.setState({emailText: this.state.rawEmailHtml})
    else this.setState({rawEmailHtml: this.state.emailText})
  }

  handleChangeEmailRaw (html) {
    this.setState({ rawEmailHtml: html })
  }

  handleClickShowRawInformation = (value) => {
    const isEditingRaw = this.state.showInformationRaw;
    this.setState({showInformationRaw: !isEditingRaw})
    this.syncViewsInformation(isEditingRaw)
  }

  syncViewsInformation = fromRaw => {
    if (fromRaw) this.setState({informationText: this.state.rawInformationHtml})
    else this.setState({rawInformationHtml: this.state.informationText})
  }

  handleChangeInformationRaw (html) {
    this.setState({ rawInformationHtml: html })
  }

  rowClickHandler = rowData => {
    this.props.updateSelectedTemplate(rowData.rowId);
  };

  rowSelected = rowData => {
    if (rowData.templateType === 'email') {
      const bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(rowData.body)[1];
      this.setState({
        selectedEmailTemplate: rowData,
        emailPopupVisibility: true,
        showEmailRaw: false,
        emailText: bodyHtml,
        emailTemplateVariable: rowData.properties.templateVariables ? rowData.properties.templateVariables : '',
        emailSubject: rowData.title,
        emailPopupTitle: `${rowData.templateType} >> ${rowData.name} >> ${rowData.version}`,
      });
    }
    if (rowData.templateType === 'information') {
      const bodyHtml = /<body.*?>([\s\S]*)<\/body>/.exec(rowData.body)[1];
      this.setState({
        selectedInformationTemplate: rowData,
        informationPopupVisibility: true,
        showInformationRaw: false,
        informationTemplateVariable: rowData.properties.templateVariables ? rowData.properties.templateVariables : '',
        informationText: bodyHtml,
        informationPopupTitle: `${rowData.templateType} >> ${rowData.name} >> ${rowData.version}`,
      });
    }

    if (rowData.templateType === 'sms') {
      this.setState({
        selectedSmsTemplate: rowData,
        smsPopupVisibility: true,
        smsTemplateVariable: rowData.properties.templateVariables ? rowData.properties.templateVariables : '',
        smsText: rowData.body,
        smsPopupTitle: `${rowData.templateType} >> ${rowData.name} >> ${rowData.version}`,
      });
    }
  };

  handleFieldChanged = value => e => {
    this.setState({ emailSubject: e.target.value });
  };

  handleChange = (value) => {
    if(this.state.counter > 1)
    {
      this.setState({
        flag: true,
        emailText: value,
        isDisabled: false,
      })
    }
    else{
      this.setState({
        counter: this.state.counter +=1
      })
    }
  };

  handleFocus = previousRange => {
    this.setState({ emailBlurIndex: previousRange.index });
  };

  handleInfoChange = value => {
    if(this.state.counter > 1){
      this.setState({
        flag: true,
        informationText: value,
        isDisabled: false,
      })
    }
    else{
      this.setState({
        counter: this.state.counter +=1
      })
    }
  };

  handleInfoFocus = previousRange => {
    this.setState({ informationBlurIndex: previousRange.index });
  };

  handleSmsChange = value => {
    if(this.state.counter > 1){
      this.setState({
        flag: true,
        smsText: value,
        isDisabled: false,
      })
    }
    else{
      this.setState({
        counter: this.state.counter +=1
      })
    }
  };

  handleSmsFocus = previousRange => {
    this.setState({ smsBlurIndex: previousRange.index });
  };

  insertSubTemplateEmailText = (name, version, format) => {
    this.setState({
      flag: true,
      isDisabled: false
    })
    this.quillRef.insertText(
      this.state.emailBlurIndex,
      `@@##${name}##${version}##${format}##@@`,
    );
  };

  insertEmailText = insertValue => {
    this.setState({
      flag: true,
      isDisabled: false
    })
    this.quillRef.insertText(this.state.emailBlurIndex, `{{${insertValue}}}`);
  };

  insertSmsText = insertValue => {
    this.setState({
      flag:true,
      isDisabled: false,
    })
    this.quillSmsRef.insertText(this.state.smsBlurIndex, `{{${insertValue}}}`);
  };

  insertSubTemplateSmsText = (name, version, format) => {
    this.setState({
      flag:true,
      isDisabled: false,
    })
    this.quillSmsRef.insertText(
      this.state.smsBlurIndex,
      `@@##${name}##${version}##${format}##@@`,
    );
  };

  insertInformationText = insertValue => {
    this.setState({
      flag: true,
      isDisabled: false,
    })
    this.quillInfoRef.insertText(
      this.state.informationBlurIndex,
      `{{${insertValue}}}`,
    );
  };

  insertSubTemplateInformationText = (name, version, format) => {
    this.setState({
      flag: true,
      isDisabled: false,
    })
    this.quillInfoRef.insertText(
      this.state.informationBlurIndex,
      `@@##${name}##${version}##${format}##@@`,
    );
  };

  changeEmailStatus = () => {
    this.setState({ emailDefaultStatus: !this.state.emailDefaultStatus });
  };

  changeInformationStatus = () => {
    this.setState({
      informationDefaultStatus: !this.state.informationDefaultStatus,
    });
  };

  changeSmsStatus = () => {
    this.setState({ smsDefaultStatus: !this.state.smsDefaultStatus });
  };

  saveEmailTemplate = () => {
    const content = this.state.showEmailRaw ? this.state.rawEmailHtml : this.state.emailText;
    const emailHtml = `<!DOCTYPE html><html lang="en"><head><title>Collaborate to Docitt loan process</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"></head><body>${
      content
      }</body></html>`;
    this.props.updateEmailTemplate(
      this.state.selectedEmailTemplate,
      emailHtml,
      this.state.emailSubject,
      this.state.emailDefaultStatus,
    );
    this.hideEmailPopup();
  };

  saveInfoTemplate = () => {
    const content = this.state.showInformationRaw ? this.state.rawInformationHtml : this.state.informationText;
    const InfoHtml = `<!DOCTYPE html><html lang="en"><head><title>Collaborate to Docitt loan process</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"></head><body><div class="privacypolicy clearfix">${
      content
    }</div></body></html>`;
    this.props.updateInformationTemplate(
      this.state.selectedInformationTemplate,
      InfoHtml,
      this.state.informationDefaultStatus,
    );
    this.hideInformationPopup();
  };

  saveSmsTemplate = () => {
    const tmp = document.createElement('DIV');
    tmp.innerHTML = this.state.smsText;
    const smsHtml = tmp.textContent || tmp.innerText || '';
    this.props.updateSmsTemplate(
      this.state.selectedSmsTemplate,
      smsHtml,
      this.state.smsDefaultStatus,
    );
    this.hideSmsPopup();
  };

  revertEmailDefault = () => {
    this.props.revertEmailTemplate(this.state.selectedEmailTemplate);
    this.hideEmailPopup();
  };

  revertInformationlDefault = () => {
    this.props.revertEmailTemplate(this.state.selectedInformationTemplate);
    this.hideInformationPopup();
  };

  revertSmsDefault = () => {
    this.props.revertEmailTemplate(this.state.selectedSmsTemplate);
    this.hideSmsPopup();
  };

  cancelEmailTemplate = () => {
    if(this.state.flag === false){
      this.hideEmailPopup()
    }
    else{
    confirmAlert({
      title: 'Are you sure you want to cancel?',
      message: 'All your changes will not be save.',
      buttons: [
        {
          label: 'No',
        },
        {
          label: 'Yes',
          onClick: () => this.hideEmailPopup(),
        },
      ],
    });}
  };

  hideEmailPopup = () => {
    this.setState({ emailPopupVisibility: false ,
      emailText: '',
      flag: false,
      counter: 0,
      isDisabled: true
    });
  };

  cancelInfoTemplate = () => {
    if(this.state.flag === false){
      this.hideInformationPopup()
    }
    else{
    confirmAlert({
      title: 'Are you sure you want to cancel?',
      message: 'All your changes will not be save.',
      buttons: [
        {
          label: 'No',
        },
        {
          label: 'Yes',
          onClick: () => this.hideInformationPopup(),
        },
      ],
    });}
  };

  hideInformationPopup = () => {
    this.setState({ informationPopupVisibility: false,
      informationText: '', 
      flag: false, counter: 0,  
      isDisabled: true,});
  };

  cancelSmsTemplate = () => {
    if(this.state.flag === false){
      this.hideSmsPopup()
    }
    else{
    confirmAlert({
      title: 'Are you sure you want to cancel?',
      message: 'All your changes will not be save.',
      buttons: [
        {
          label: 'No',
        },
        {
          label: 'Yes',
          onClick: () => this.hideSmsPopup(),
        },
      ],
    });}
  };

  hideSmsPopup = () => {
    this.setState({ smsPopupVisibility: false,
      smsText: '' ,
      flag: false,
      counter: 0,
      isDisabled: true,});
  };

  formatDate = (timeStamp) => {
    var fullDate =  new Date(timeStamp)
    var month = ('0' + (fullDate.getMonth() + 1)).slice(-2);
    var date = ('0' + fullDate.getDate()).slice(-2);
    var year = fullDate.getFullYear();
    return month + '.' + date + '.' + year;
  }

  render() {
    const { templates, subTemplate, tenantVariables } = this.props;
    const {
      emailPopupVisibility,
      emailTemplateVariable,
      emailPopupTitle,
      emailDefaultStatus,
      informationTemplateVariable,
      informationPopupVisibility,
      revertInformationToDefaultValue,
      informationPopupTitle,
      informationText,
      informationDefaultStatus,
      smsPopupVisibility,
      smsPopupTitle,
      smsDefaultStatus,
      smsTemplateVariable,
      templateTypeFilter,
      statusFilter
    } = this.state;

    const header = [
      { header: '#', name: 'id' },
      { header: 'Type', name: 'templateType', searchable: true, filter: true, filterValue: templateTypeFilter, sortable: true},
      { header: 'Template Name', name: 'name', searchable: true, sortable: true},
      { header: 'Version', name: 'version' },
      { header: 'Status', name: 'status', dynamicClassName: true, filter: true, filterValue: statusFilter, },
      { header: 'Created On', name: 'updatedDate' },
    ];

    const data = templates.map((template, index) => ({
      id: index + 1,
      body: template.body,
      title: template.title,
      templateType: template.templateType,
      properties: template.properties,
      format: template.format,
      name: template.name,
      version: template.version,
      status: template.isActive ? 'Active' : 'Inactive',
      updatedDate: template.createdDate ? this.formatDate(template.createdDate.time) : "",
      rowId: template.id,
      isSystem: template.isSystem,
      isSelected: template.isSelected,
    }));

    return (
      <div className="dashboard clearfix">
        <div className="headertitle">Text</div>
        <div className="padd clearfix">
          <DataTable
            headings={header}
            rows={data}
            rowClick={this.rowClickHandler}
            rowSelected={this.rowSelected}
          />

          <PopUp visible={emailPopupVisibility} title={emailPopupTitle}>
            <div>
              <div className="col-lg-12 revert-default">
                <span className="blue ">Reinstate this Default</span>
                <Switch
                  name="revertEmailDefault"
                  valueChanged={this.revertEmailDefault}
                  value={this.state.revertEmailToDefaultValue}
                />
              </div>
              <div className="col-lg-4 col-md-4 col-sm-6">
                <TextView
                  name="mailSubject"
                  placeholder="Mail Subject"
                  type="text"
                  showError={this.state.showErrors}
                  text={this.state.emailSubject}
                  onFieldChanged={this.handleFieldChanged('mailSubject')}
                />
                <h5>Sub-Templates</h5>
                <div className="sub-templates">
                  {subTemplate.map(item => (
                    <div
                      key={item.id}
                      onClick={() =>
                        this.insertSubTemplateEmailText(
                          item.name,
                          item.version,
                          item.format,
                        )
                      }
                    >
                      {item.name}
                    </div>
                  ))}
                </div>

                <h5>Application Variables</h5>
                <div className="sub-templates">
                  {Object.keys(emailTemplateVariable).map((item, index) => (
                    <div
                      key={index}
                      onClick={() =>
                        this.insertEmailText(emailTemplateVariable[item])
                      }
                    >
                      {item}
                    </div>
                  ))}
                </div>

                <h5>Tenant Specific Variables</h5>
                <div className="sub-templates">
                  {Object.keys(tenantVariables).map((item, index) => (
                    <div key={index} onClick={() => this.insertEmailText(item)}>
                      {item}
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-6">
                <h3>Template Body</h3>
                <div className={ this.state.showEmailRaw ? "editor showEmailRaw" : "editor"}>
                  <CustomToolbar onClickRaw={this.handleClickShowRawEmail}/>
                  <ReactQuill
                    value={this.state.emailText}
                    ref={el => {
                    this.reactQuillRef = el;
                  }}
                    modules={this.modules}
                    formats={this.formats}
                    onChange={this.handleChange}
                    onBlur={this.handleFocus}
                    className="email-holder"
                  />
                  <textarea
                    className="raw-email-editor"
                    onChange={(e) => this.handleChangeEmailRaw(e.target.value)}
                    value={this.state.rawEmailHtml}
                  />
                </div>
                <div className="status">
                  Status{' '}
                  <Switch
                    name="emailDefaultStatus"
                    valueChanged={this.changeEmailStatus}
                    value={emailDefaultStatus}
                  />
                </div>

                <div className="btnpreview pull-right">
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    buttonType="btn-outline"
                    clickHandler={this.cancelEmailTemplate}
                    text="Cancel"
                  />
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    disabled = {this.state.isDisabled}
                    clickHandler={this.saveEmailTemplate}
                    text="Save"
                  />
                </div>
              </div>
            </div>
          </PopUp>

          <PopUp
            visible={informationPopupVisibility}
            title={informationPopupTitle}
          >
            <div>
              <div className="clearfix revert-default">
                <div className="pull-right">
                  <span className="blue ">Reinstate this Default</span>
                  <Switch
                    name="revertInformationDefault"
                    valueChanged={this.revertInformationlDefault}
                    value={revertInformationToDefaultValue}
                  />
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-6">
                <h5>Sub-Templates</h5>
                <div className="sub-templates">
                  {subTemplate.map(item => (
                    <div
                      key={item.id}
                      onClick={() =>
                        this.insertSubTemplateInformationText(
                          item.name,
                          item.version,
                          item.format,
                        )
                      }
                    >
                      {item.name}
                    </div>
                  ))}
                </div>

                <h5>Application Variables</h5>
                <div className="sub-templates">
                  {informationTemplateVariable &&
                    Object.keys(informationTemplateVariable).map(
                      (item, index) => (
                        <div
                          key={index}
                          onClick={() =>
                            this.insertInformationText(
                              emailTemplateVariable[item],
                            )
                          }
                        >
                          {item}
                        </div>
                      ),
                    )}
                </div>

                <h5>Tenant Specific Variables</h5>
                <div className="sub-templates">
                  {Object.keys(tenantVariables).map((item, index) => (
                    <div
                      key={index}
                      onClick={() => this.insertInformationText(item)}
                    >
                      {item}
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-6">
                <h3>Template Body</h3>
                <div className={ this.state.showInformationRaw ? "editor showInfoRaw" : "editor"}>
                  <CustomToolbar onClickRaw={this.handleClickShowRawInformation}/>
                  <ReactQuill
                    value={informationText}
                    ref={el => {
                    this.reactQuillInfoRef = el;
                  }}
                    modules={this.modules}
                    formats={this.formats}
                    onChange={this.handleInfoChange}
                    onBlur={this.handleInfoFocus}
                    className="information-holder"
                  />
                  <textarea
                    className="raw-info-editor"
                    onChange={(e) => this.handleChangeInformationRaw(e.target.value)}
                    value={this.state.rawInformationHtml}
                  />
                </div>
                <div className="status">
                  Status{' '}
                  <Switch
                    name="informationDefaultStatus"
                    valueChanged={this.changeInformationStatus}
                    value={informationDefaultStatus}
                  />
                </div>

                <div className="btnpreview pull-right">
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    buttonType="btn-outline"
                    clickHandler={this.cancelInfoTemplate}
                    text="Cancel"
                  />
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    disabled = {this.state.isDisabled}
                    clickHandler={this.saveInfoTemplate}
                    text="Save"
                  />
                </div>
              </div>
            </div>
          </PopUp>

          <PopUp visible={smsPopupVisibility} title={smsPopupTitle}>
            <div>
              <div className="clearfix revert-default">
                <div className="pull-right">
                  <span className="blue ">Reinstate this Default</span>
                  <Switch
                    name="revertSmsDefault"
                    valueChanged={this.revertSmsDefault}
                    value={this.state.revertSmsToDefaultValue}
                  />
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-6">
                <h5>Sub-Templates</h5>
                <div className="sub-templates">
                  {subTemplate.map(item => (
                    <div
                      key={item.id}
                      onClick={() =>
                        this.insertSubTemplateSmsText(
                          item.name,
                          item.version,
                          item.format,
                        )
                      }
                    >
                      {item.name}
                    </div>
                  ))}
                </div>

                <h5>Application Variables</h5>
                <div className="sub-templates">
                  {smsTemplateVariable &&
                    Object.keys(smsTemplateVariable).map((item, index) => (
                      <div
                        key={index}
                        onClick={() =>
                          this.insertSmsText(smsTemplateVariable[item])
                        }
                      >
                        {item}
                      </div>
                    ))}
                </div>

                <h5>Tenant Specific Variables</h5>
                <div className="sub-templates">
                  {Object.keys(tenantVariables).map((item, index) => (
                    <div key={index} onClick={() => this.insertSmsText(item)}>
                      {item}
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-lg-8 col-md-8 col-sm-6">
                <h3>Template Body</h3>
                  <ReactQuill
                    value={this.state.smsText}
                    ref={el => {
                    this.reactQuillSmsRef = el;
                  }}
                    onChange={this.handleSmsChange}
                    onBlur={this.handleSmsFocus}
                    className="sms-holder"
                  />
                <div className="status">
                  Status{' '}
                  <Switch
                    name="smsDefaultStatus"
                    valueChanged={this.changeSmsStatus}
                    value={smsDefaultStatus}
                  />
                </div>

                <div className="btnpreview pull-right">
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    buttonType="btn-outline"
                    clickHandler={this.cancelSmsTemplate}
                    text="Cancel"
                  />
                  <Button
                    type="button"
                    buttonSize="btn-sm"
                    disabled = {this.state.isDisabled}
                    clickHandler={this.saveSmsTemplate}
                    text="Save"
                  />
                </div>
              </div>
            </div>
          </PopUp>
        </div>
      </div>
    );
  }
}

Template.propTypes = {
  templates: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  subTemplate: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  tenantVariables: PropTypes.object,
  fetchTemplateList: PropTypes.func,
  updateEmailTemplate: PropTypes.func,
  revertEmailTemplate: PropTypes.func,
  updateInformationTemplate: PropTypes.func,
  updateSmsTemplate: PropTypes.func,
  updateSelectedTemplate: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  templates: templates(),
  subTemplate: subTemplate(),
  tenantVariables: tenantVariables(),
});

const mapDispatchToProps = dispatch => ({
  fetchTemplateList: () => {
    dispatch(fetchTemplateList());
  },
  updateEmailTemplate: (
    selectedEmailTemplate,
    emailText,
    emailSubject,
    emailDefaultStatus,
  ) => {
    dispatch(
      updateEmailTemplate(
        selectedEmailTemplate,
        emailText,
        emailSubject,
        emailDefaultStatus,
      ),
    );
  },
  revertEmailTemplate: selectedEmailTemplate => {
    dispatch(revertEmailTemplate(selectedEmailTemplate));
  },
  updateInformationTemplate: (
    selectedEmailTemplate,
    emailText,
    emailDefaultStatus,
  ) => {
    dispatch(
      updateInformationTemplate(
        selectedEmailTemplate,
        emailText,
        emailDefaultStatus,
      ),
    );
  },
  updateSmsTemplate: (selectedSmsTemplate, smsText, smsDefaultStatus) => {
    dispatch(updateSmsTemplate(selectedSmsTemplate, smsText, smsDefaultStatus));
  },
  updateSelectedTemplate: templateId => {
    dispatch(updateSelectedTemplate(templateId));
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'template', reducer });
const withSaga = injectSaga({ key: 'template', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Template);
