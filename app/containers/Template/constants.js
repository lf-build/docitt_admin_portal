/**
 * Created by sigma on 25/2/19.
 */
export const FETCH_ALL_TEMPLATE = 'lendfoundry/template/FETCH_ALL_TEMPLATE';
export const UPDATE_ALL_TEMPLATE = 'lendfoundry/template/UPDATE_ALL_TEMPLATE';
export const UPDATE_EMAIL_TEMPLATE =
  'lendfoundry/template/UPDATE_EMAIL_TEMPLATE';
export const ADD_NEW_EMAIL_TEMPLATE =
  'lendfoundry/template/ADD_NEW_EMAIL_TEMPLATE';
export const REVERT_DEFAULT_EMAIL_TEMPLATE =
  'lendfoundry/template/REVERT_DEFAULT_EMAIL_TEMPLATE';
export const UPDATE_INFORMATION_TEMPLATE =
  'lendfoundry/template/UPDATE_INFORMATION_TEMPLATE';
export const UPDATE_SMS_TEMPLATE = 'lendfoundry/template/UPDATE_SMS_TEMPLATE';
export const UPDATE_TEMPLATE_STATUS =
  'lendfoundry/template/UPDATE_TEMPLATE_STATUS';
export const SELECT_TEMPLATE_STATUS =
  'lendfoundry/template/SELECT_TEMPLATE_STATUS';
export const REINSTATE_DEFAULT_TEMPLATE = 'lendfoundry/template/REINSTATE_DEFAULT_TEMPLATE';
export const API_ERROR = 'lendfoundry/template/API_ERROR';
