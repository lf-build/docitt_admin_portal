/**
 * Created by sigma on 25/2/19.
 */
import { fromJS } from 'immutable';
import {
  UPDATE_ALL_TEMPLATE,
  ADD_NEW_EMAIL_TEMPLATE,
  UPDATE_TEMPLATE_STATUS,
  SELECT_TEMPLATE_STATUS,
  REINSTATE_DEFAULT_TEMPLATE,
  API_ERROR,
} from './constants';

export const initialState = fromJS({
  buttonEnable: true,
  templateList: [],
  subTemplateList: [],
  errorMessage: '',
});

function templateReducer(state = initialState, action) {
  switch (action.type) {
    case API_ERROR:
      return state.setIn(['errorMessage'], fromJS(action.error));

    case UPDATE_ALL_TEMPLATE:
      const selectedTemplate = action.list
        .filter(item => item.isSystem)
        .map(template => {
          template.isSelected = false;
          return template;
        });
      return state
        .setIn(['templateList'], selectedTemplate)
        .setIn(['subTemplateList'], action.list.filter(item => !item.isSystem));

    case ADD_NEW_EMAIL_TEMPLATE:
      const templateList = state.get('templateList').slice();
      templateList.splice(templateList.length , 0, action.newTemplate)
      return state.setIn(['templateList'], templateList);

    case SELECT_TEMPLATE_STATUS:
      const templatedSelected = state.getIn(['templateList']).map(item => {
        if (item.id === action.templateId) item.isSelected = true;
        else if (item.isSelected === true) item.isSelected = false;
        return item;
      });
      return state.setIn(['templateList'], templatedSelected);

    case UPDATE_TEMPLATE_STATUS:
      const templateIndex = state.getIn(['templateList']).map(item => {
        if (item.id === action.templateId) item.isActive = false;
        return item;
      });
      return state.setIn(['templateList'], templateIndex);

    case REINSTATE_DEFAULT_TEMPLATE:
      const updatedTemplateList = state.getIn(['templateList']).map(item => {
        if (item.name === action.templateName && item.version === action.version) item.isActive = true;
        else if (item.name === action.templateName && item.isActive) item.isActive = false;
        return item;
      });
      return state.setIn(['templateList'], updatedTemplateList);

    default:
      return state;
  }
}

export default templateReducer;
