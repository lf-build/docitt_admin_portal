/**
 * Created by sigma on 15/1/19.
 */
import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { getRequest, postRequest } from 'utils/request';
import { previewFontFamily, defaultFont } from './selectors';
import { showMessageSuccess, showErrorMessage } from 'components/ErrorMessage';
import {
  FETCH_ALL_FONTS,
  SAVE_FONT_FAMILY,
  REVERT_DEFAULT_FONT_FAMILY,
} from './constants';
import {
  apiFailure,
  updateFontFamiliesList,
  updateDefaultFont,
  currentAppliedFont,
} from './actions';

export function* fetchAllFonts() {
  const base = yield select(serviceUrl('theme'));
  const url = '/masterfontfamily/all';

  try {
    const allFontFamilies = yield call(getRequest, base, url);
    yield put(updateFontFamiliesList(allFontFamilies));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* getDefaultFont() {
  const base = yield select(serviceUrl('theme'));
  const url = '/masterfontfamily/default';

  try {
    const fontDefault = yield call(getRequest, base, url);
    if(fontDefault)
    yield put(updateDefaultFont(fontDefault));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* currentApplied() {
  const base = yield select(serviceUrl('theme'));
  const url = '/tenantfontfamily';

  try {
    const selectedFont = yield call(getRequest, base, url);
    yield put(currentAppliedFont(selectedFont));
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* changeFontFamily() {
  const base = yield select(serviceUrl('theme'));
  const url = '/tenantfontfamily';
  const generateCssUrl = '/theme/generate/css';
  const appliedFontFamily = yield select(previewFontFamily());
  const payload = {
    name: appliedFontFamily.name,
    sourceFontUrl: appliedFontFamily.sourceFontUrl,
    fontWeight: appliedFontFamily.fontWeight,
  };
  try {
    const selectedFont = yield call(postRequest, base, url, payload);
    yield put(currentAppliedFont(selectedFont));
    yield call(postRequest, base, generateCssUrl, {});
    yield call(showMessageSuccess, 'Font saved and applied successfully.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export function* defaultFontFamily() {
  const base = yield select(serviceUrl('theme'));
  const url = '/tenantfontfamily';
  const generateCssUrl = '/theme/generate/css';
  const appliedFontFamily = yield select(defaultFont());
  const payload = {
    name: appliedFontFamily.name,
    sourceFontUrl: appliedFontFamily.sourceFontUrl,
    fontWeight: appliedFontFamily.fontWeight,
  };
  try {
    const selectedFont = yield call(postRequest, base, url, payload);
    yield put(currentAppliedFont(selectedFont));
    yield call(postRequest, base, generateCssUrl, {});
    yield call(showMessageSuccess, 'Font reverted to default.');
  } catch (err) {
    yield call(showErrorMessage,err);
    yield put(apiFailure(err));
  }
}

export default function* makeApiCalls() {
  yield all([
    takeLatest(FETCH_ALL_FONTS, fetchAllFonts),
    takeLatest(FETCH_ALL_FONTS, getDefaultFont),
    takeLatest(FETCH_ALL_FONTS, currentApplied),
    takeLatest(SAVE_FONT_FAMILY, changeFontFamily),
    takeLatest(REVERT_DEFAULT_FONT_FAMILY, defaultFontFamily),
  ]);
}
