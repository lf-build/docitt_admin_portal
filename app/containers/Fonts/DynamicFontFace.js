/**
 * Created by sigma on 29/1/19.
 */
import React from 'react';

class DynamicFontFace extends React.PureComponent {
  constructor (props) {
    super(props)
  }

  render() {
    const {
      fontFamily
    } = this.props;
    return (
      <div>
      <div className="fontdes" style={{fontFamily: `${fontFamily}`}}>
        {this.props.children}
      </div>
      </div>
    )
  }
}

export default DynamicFontFace;