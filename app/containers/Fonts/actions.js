/**
 * Created by sigma on 15/1/19.
 */
import {
  FETCH_ALL_FONTS,
  STORE_ALL_FONTS,
  DEFAULT_FONT,
  APPLIED_FONT,
  UPDATE_FONT_TYPE_SELECTION,
  UPDATE_FONT_FAMILY_SEARCH,
  UPDATE_FONT_FAMILY_SELECTION,
  PREVIEW_FONT_FAMILY,
  CLEAR_FONT_FAMILY_SELECTION,
  SAVE_FONT_FAMILY,
  REVERT_DEFAULT_FONT_FAMILY,
  API_ERROR,
} from './constants';

export function fetchAllFonts() {
  return {
    type: FETCH_ALL_FONTS,
  };
}
export function updateFontFamiliesList(fontFamilyList) {
  return {
    type: STORE_ALL_FONTS,
    list: fontFamilyList,
  };
}

export function updateDefaultFont(font) {
  return {
    type: DEFAULT_FONT,
    defaultFont: font,
  };
}

export function currentAppliedFont(font) {
  return {
    type: APPLIED_FONT,
    fontInuse: font,
  };
}

export function updateFontType(fontType) {
  return {
    type: UPDATE_FONT_TYPE_SELECTION,
    selection: fontType,
  };
}

export function apiFailure(err) {
  return {
    type: API_ERROR,
    error: err,
  };
}

export function updateSearchValue(searchKeyword) {
  return {
    type: UPDATE_FONT_FAMILY_SEARCH,
    searchKeyword,
  };
}

export function updateFontFamilySelection(familyName) {
  return {
    type: UPDATE_FONT_FAMILY_SELECTION,
    familyName,
  };
}

export function previewFont() {
  return {
    type: PREVIEW_FONT_FAMILY,
  };
}

export function clearFont() {
  return {
    type: CLEAR_FONT_FAMILY_SELECTION,
  };
}

export function saveFont() {
  return {
    type: SAVE_FONT_FAMILY,
  };
}

export function revertDefaultFont() {
  return {
    type: REVERT_DEFAULT_FONT_FAMILY,
  };
}
