/**
 * Created by sigma on 15/1/19.
 */
import { fromJS } from 'immutable';
import {
  STORE_ALL_FONTS,
  APPLIED_FONT,
  DEFAULT_FONT,
  UPDATE_FONT_TYPE_SELECTION,
  UPDATE_FONT_FAMILY_SEARCH,
  UPDATE_FONT_FAMILY_SELECTION,
  PREVIEW_FONT_FAMILY,
  CLEAR_FONT_FAMILY_SELECTION,
  API_ERROR,
} from './constants';

export const initialState = fromJS({
  fontFamiliesList: [],
  filteredFamilies: [],
  fontType: ['Serif', 'Sans Serif', 'Display', 'Handwriting', 'Monospace'],
  selectedFontType: [],
  currentApplied: {},
  selectedFont: {},
  previewFont: {},
  defaultFont: {},
  filteredFamiliesCount: 0,
  errorMessage: '',
  fontFamilyNameSearch: '',
});

function fontReducer(state = initialState, action) {
  switch (action.type) {
    case STORE_ALL_FONTS:
      /* //filtering all the fonts that has weight of 300/400/500/700

       const filteredFamilyList = action.list.filter((fontFamily) => {
       fontFamily.fontWeight.includes(['300', '400', '500', '700'])
       });
       console.log(filteredFamilyList); */
      return state.setIn(['fontFamiliesList'], fromJS(action.list));

    case DEFAULT_FONT:
      return state.setIn(['defaultFont'], fromJS(action.defaultFont));

    case APPLIED_FONT:
      return state
        .setIn(['currentApplied'], fromJS(action.fontInuse))
        .setIn(['previewFont'], action.fontInuse)
        .setIn(['selectedFont'], action.fontInuse);

    case UPDATE_FONT_TYPE_SELECTION:
      const filteredFamilies = state
        .get('fontFamiliesList')
        .toJS()
        .filter(
          fontFamily =>
            action.selection.includes(fontFamily.category) &&
            fontFamily.name
              .toLowerCase()
              .includes(state.get('fontFamilyNameSearch').toLowerCase()),
        );
      return state
        .setIn(['selectedFontType'], action.selection)
        .setIn(['filteredFamilies'], fromJS(filteredFamilies))
        .set('filteredFamiliesCount', filteredFamilies.length);

    case UPDATE_FONT_FAMILY_SEARCH:
      const filteredFamily = state
        .get('fontFamiliesList')
        .toJS()
        .filter(
          fontFamily =>
            state.get('selectedFontType').includes(fontFamily.category) &&
            fontFamily.name
              .toLowerCase()
              .includes(action.searchKeyword.toLowerCase()),
        );
      return state
        .setIn(['fontFamilyNameSearch'], action.searchKeyword)
        .setIn(['filteredFamilies'], fromJS(filteredFamily))
        .set('filteredFamiliesCount', filteredFamily.length);

    case UPDATE_FONT_FAMILY_SELECTION:
      return state.set(
        'selectedFont',
        state
          .get('fontFamiliesList')
          .toJS()
          .find(fontFamily => fontFamily.name === action.familyName),
      );

    case PREVIEW_FONT_FAMILY:
      return state.set('previewFont', state.get('selectedFont'));

    case CLEAR_FONT_FAMILY_SELECTION:
      return state
        .setIn(['selectedFontType'], [])
        .setIn(['filteredFamilies'], fromJS([]))
        .set('previewFont', state.get('defaultFont').toJS())
        .set('selectedFont', state.get('defaultFont').toJS())
        .set('fontFamilyNameSearch', '')
        .set('filteredFamiliesCount', 0);

    case API_ERROR:
      return state.setIn(['errorMessage'], fromJS(action.error));

    default:
      return state;
  }
}

export default fontReducer;
