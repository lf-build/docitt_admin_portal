/**
 * Created by sigma on 15/1/19.
 */
export const FETCH_ALL_FONTS = 'lendfoundry/fonts/FETCH_ALL_FONTS';
export const STORE_ALL_FONTS = 'lendfoundry/fonts/STORE_ALL_FONTS';
export const DEFAULT_FONT = 'lendfoundry/fonts/DEFAULT_FONT';
export const APPLIED_FONT = 'lendfoundry/fonts/APPLIED_FONT';
export const UPDATE_FONT_TYPE_SELECTION =
  'lendfoundry/fonts/UPDATE_FONT_TYPE_SELECTION';
export const UPDATE_FONT_FAMILY_SEARCH =
  'lendfoundry/fonts/UPDATE_FONT_FAMILY_SEARCH';
export const UPDATE_FONT_FAMILY_SELECTION =
  'lendfoundry/fonts/UPDATE_FONT_FAMILY_SELECTION';
export const PREVIEW_FONT_FAMILY = 'lendfoundry/fonts/PREVIEW_FONT_FAMILY';
export const CLEAR_FONT_FAMILY_SELECTION =
  'lendfoundry/fonts/CLEAR_FONT_FAMILY_SELECTION';
export const SAVE_FONT_FAMILY = 'lendfoundry/fonts/SAVE_FONT_FAMILY';
export const REVERT_DEFAULT_FONT_FAMILY =
  'lendfoundry/fonts/REVERT_DEFAULT_FONT_FAMILY';
export const API_ERROR = 'lendfoundry/fonts/API_ERROR';
