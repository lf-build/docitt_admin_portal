/**
 * Created by sigma on 15/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

import './fonts.css';
import '@trendmicro/react-radio/dist/react-radio.min.css';
import Switch from 'components/SwitchView';
import { RadioButton, RadioGroup } from '@trendmicro/react-radio';
import { Checkbox, CheckboxGroup } from 'components/Checkbox';
import TextView from 'components/TextView';
import Button from 'components/Button';
import DynamicFontFace from './DynamicFontFace';
import {
  fetchAllFonts,
  updateFontType,
  updateSearchValue,
  updateFontFamilySelection,
  previewFont,
  clearFont,
  saveFont,
  revertDefaultFont,
} from './actions';
import {
  allFonts,
  fontTypes,
  selectedFontType,
  fontFamilyNameSearch,
  filteredFamiliesCount,
  defaultFont,
  previewFontFamily,
  selectedFont,
  currentApplied,
  defaultFamilySelected,
} from './selectors';
import saga from './saga';
import reducer from './reducer';

class Fonts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showErrors: false,
    };
    this.fontTypeChanged = this.fontTypeChanged.bind(this);
    this.handleFieldChanged = this.handleFieldChanged.bind(this);
    this.selectFont = this.selectFont.bind(this);
    this.handlePreview = this.handlePreview.bind(this);
    this.revertDefaultFont = this.revertDefaultFont.bind(this);
    this.revertToPreviousSelection = this.revertToPreviousSelection.bind(this);
    this.applyFont = this.applyFont.bind(this);
  }

  fontTypeChanged(fontType) {
    this.props.updateFontType(fontType);
  }

  handleFieldChanged() {
    return e => {
      this.props.updateSearchValue(e.target.value);
    };
  }

  selectFont(value) {
    this.props.updateFontFamilySelection(value);
  }

  handlePreview() {
    if (this.props.previewFontFamily.name !== this.props.selectedFont.name) {
      this.props.previewFont();
    }
  }

  revertDefaultFont() {
    if (!this.props.defaultFamilySelected) {
      this.props.revertDefaultFont();
    }
  }

  revertToPreviousSelection() {
    this.props.clearFont();
  }

  applyFont() {
    if (this.props.currentApplied !== this.props.previewFontFamily.name) {
      this.props.saveFont();
    }
  }

  componentDidMount() {
    this.props.fetchAllFonts();
  }

  render() {
    const {
      fontTypes,
      allFonts,
      count,
      defaultFont,
      currentApplied,
      defaultFamilySelected,
      previewFontFamily,
      selectedFontType,
    } = this.props;
    return (
      <div className="dashboard clearfix">
        <div className="headertitle">Fonts</div>
        <div className="padd clearfix">
          <div className="revert clearfix">
            <p className="pull-left">
              Select the typeface that best suits your brand
            </p>
            <div className="pull-right">
              <span className="blue ">Default</span>
              <Switch
                valueChanged={this.revertDefaultFont}
                value={defaultFamilySelected}
              />
            </div>
          </div>

          <div className="fontwrpper clearfix">
            <div className="clmn1">
              <h3>Font Type</h3>

              <div className="radio">
                <RadioGroup
                  value={defaultFamilySelected ? defaultFont.name : ''}
                  name="defaultFont"
                  onChange={value => this.revertDefaultFont(value)}
                >
                  <RadioButton value={defaultFont.name}>
                    <div>
                      Default Font
                      <br />
                      {defaultFont.name}
                    </div>
                  </RadioButton>
                </RadioGroup>
              </div>

              <h5>Font Type</h5>
              <CheckboxGroup
                value={selectedFontType}
                name="fontType"
                onChange={this.fontTypeChanged}
              >
                {fontTypes.map((name, index) => (
                  <Checkbox key={index} value={name}>
                    <div className="fontfam">{name}</div>
                  </Checkbox>
                ))}
              </CheckboxGroup>

              <div className="searchfont">
                <TextView
                  name="fontFamilyName"
                  showError={this.state.showErrors}
                  type="text"
                  text={this.props.fontFamilyName}
                  placeholder="Search Font Type"
                  onFieldChanged={this.handleFieldChanged('fontFamily')}
                />
                <i className="df df-search" />
              </div>
            </div>

            <div className="clmn2">
              <h3>Font families ({count})</h3>
              <div className="radio appliedfont">
                <label>
                  Applied Font
                  <br />
                  {currentApplied}
                </label>
              </div>
              <div className="mid">
                <RadioGroup
                  name="fontFamily"
                  onChange={value => this.selectFont(value)}
                >
                  {allFonts.map(font => (
                    <RadioButton key={font.id} value={font.name}>
                      <div className="radio">
                        <label className="fontname">{font.name}</label>
                        <p className="fontauth">{font.designers.join(', ')}</p>
                        <DynamicFontFace fontFamily={font.name}>
                          Welcome to Docitt the number one source for mortgage
                          lending.
                        </DynamicFontFace>
                      </div>
                    </RadioButton>
                  ))}
                </RadioGroup>
              </div>
            </div>

            <div className="clmn3">
              <h3>Example Preview</h3>
              <div
                className="rightpanel clearfix"
                style={{ fontFamily: `${previewFontFamily.name}` }}
              >
                <h3>Loan Details</h3>
                <p>Please give us some details about the loan</p>
                <div className="form-group">
                  <label className="form-label">Purchase Price ($)</label>
                  <span>$650,000</span>
                </div>
                <div className="clearfix">
                  <div className="form-group col">
                    <label className="form-label">Down Payment ($)</label>
                    <span>$0</span>
                  </div>
                  <div className="midcol">&nbsp;</div>
                  <div className="form-group col">
                    <label className="form-label" />
                    <span>(%)</span>
                  </div>
                </div>
                <div className="form-group">
                  <label className="form-label">Loan Amount</label>
                  <span>$0</span>
                </div>
                <div className="pull-left backbtn">
                  <span className="df df-back" /> &nbsp; BACK
                </div>
                <div className="pull-right">
                  <button type="button" className="btn btn-xs">
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="fontwrpper clearfix">
            <div className="clmn1">&nbsp;</div>
            <div className="clmn2 mtop30 text-center">
              <Button
                type="button"
                buttonSize="btn-sm"
                buttonType="btn-outline"
                clickHandler={this.handlePreview}
                text="PREVIEW"
              />
            </div>
            <div className="clmn3 btns">
              <div className="btnpreview">
                <Button
                  type="button"
                  buttonSize="btn-sm"
                  buttonType="btn-outline"
                  clickHandler={this.revertToPreviousSelection}
                  text="Clear"
                />
                <Button
                  type="button"
                  buttonSize="btn-sm"
                  clickHandler={this.applyFont}
                  text="APPLY STYLE"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Fonts.propTypes = {
  fetchAllFonts: PropTypes.func,
  updateFontType: PropTypes.func,
  updateSearchValue: PropTypes.func,
  previewFont: PropTypes.func,
  clearFont: PropTypes.func,
  saveFont: PropTypes.func,
  revertDefaultFont: PropTypes.func,
  updateFontFamilySelection: PropTypes.func,
  fontFamilyNameSearch: PropTypes.string,
  currentApplied: PropTypes.string,
  defaultFamilySelected: PropTypes.bool,
  filteredFamiliesCount: PropTypes.number,
  count: PropTypes.number,
  allFonts: PropTypes.array,
  selectedFontType: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  defaultFont: PropTypes.object,
  previewFontFamily: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  selectedFont: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
};

const mapStateToProps = createStructuredSelector({
  allFonts: allFonts(),
  fontTypes: fontTypes(),
  fontFamilyName: fontFamilyNameSearch(),
  count: filteredFamiliesCount(),
  defaultFont: defaultFont(),
  previewFontFamily: previewFontFamily(),
  selectedFont: selectedFont(),
  currentApplied: currentApplied(),
  selectedFontType: selectedFontType(),
  defaultFamilySelected: defaultFamilySelected(),
});

const mapDispatchToProps = dispatch => ({
  fetchAllFonts: () => {
    dispatch(fetchAllFonts());
  },
  updateFontType: fontType => {
    dispatch(updateFontType(fontType));
  },
  updateSearchValue: SearchKeyword => {
    dispatch(updateSearchValue(SearchKeyword));
  },
  updateFontFamilySelection: familyName => {
    dispatch(updateFontFamilySelection(familyName));
  },
  previewFont: () => {
    dispatch(previewFont());
  },
  clearFont: () => {
    dispatch(clearFont());
  },
  saveFont: () => {
    dispatch(saveFont());
  },
  revertDefaultFont: () => {
    dispatch(revertDefaultFont());
  },
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'fonts', reducer });
const withSaga = injectSaga({ key: 'fonts', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Fonts);
