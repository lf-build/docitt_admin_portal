/**
 * Created by sigma on 15/1/19.
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectFonts = state => state.get('fonts', initialState);

const allFonts = () =>
  createSelector(selectFonts, fontsState =>
    fontsState.getIn(['filteredFamilies']).toJS(),
  );

const defaultFont = () =>
  createSelector(selectFonts, fontsState =>
    fontsState.get('defaultFont').toJS(),
  );

const fontTypes = () =>
  createSelector(selectFonts, fontsState => fontsState.get('fontType'));

const selectedFont = () =>
  createSelector(selectFonts, fontsState => fontsState.get('selectedFont'));

const selectedFontType = () =>
  createSelector(selectFonts, fontsState => fontsState.get('selectedFontType'));

const previewFontFamily = () =>
  createSelector(selectFonts, fontsState => fontsState.get('previewFont'));

const fontFamilyNameSearch = () =>
  createSelector(selectFonts, fontsState =>
    fontsState.get('fontFamilyNameSearch'),
  );

const filteredFamiliesCount = () =>
  createSelector(selectFonts, fontsState =>
    fontsState.get('filteredFamiliesCount'),
  );

const defaultFamilySelected = () =>
  createSelector(
    selectFonts,
    fontsState =>
      fontsState.getIn(['defaultFont', 'name']) ===
      fontsState.getIn(['currentApplied', 'name']),
  );

const currentApplied = () =>
  createSelector(selectFonts, fontsState =>
    fontsState.getIn(['currentApplied', 'name']),
  );

export {
  selectFonts,
  allFonts,
  defaultFont,
  selectedFontType,
  fontTypes,
  previewFontFamily,
  selectedFont,
  fontFamilyNameSearch,
  filteredFamiliesCount,
  defaultFamilySelected,
  currentApplied,
};
