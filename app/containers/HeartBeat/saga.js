/**
 * Created by sigma on 23/11/18.
 */

import { call, put, select, takeLatest, all, takeEvery } from 'redux-saga/effects';
import { serviceUrl } from 'containers/Configuration/selectors';
import { getRequest } from 'utils/request';
import { userLogOut } from 'containers/HomePage/actions';
import { VALIDATE_TOKEN, USER_LOG_OUT } from './constants';
import { tokenValid, logoutUser, userLoggedOut } from './actions';

export function* validateToken() {
  const base = yield select(serviceUrl('security'));
  const url = '/is-authorized/';
  try {
    yield call(getRequest, base, url);
    yield put(tokenValid());
  } catch (err) {
    yield put(logoutUser());
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* tokenValidator() {
  yield all([
    takeLatest(VALIDATE_TOKEN, validateToken),
    takeEvery(USER_LOG_OUT, logout),
  ]);
}

export function* logout() {
  const base = yield select(serviceUrl('security'));
  const url = '/logout/';
  try {
    yield call(getRequest, base, url);
    yield put(userLoggedOut());
    yield put(userLogOut());
  } catch (err) {
    yield put(userLogOut());
    yield put(userLoggedOut());
  }
}
