/**
 * Created by sigma on 23/11/18.
 */
export const VALIDATE_TOKEN = 'lendfoundry/HeartBeat/VALIDATE_TOKEN';

export const TOKEN_VALID = 'lendfoundry/HeartBeat/TOKEN_VALID';

export const TOKEN_INVALID = 'lendfoundry/HeartBeat/TOKEN_INVALID';

export const USER_LOG_OUT = 'lendfoundry/HeartBeat/USER_LOG_OUT';

export const USER_LOGGED_OUT = 'lendfoundry/HeartBeat/USER_LOGGED_OUT';
