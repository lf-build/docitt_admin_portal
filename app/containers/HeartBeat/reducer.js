/**
 * Created by sigma on 22/11/18.
 */
import { fromJS } from 'immutable';

import { USER_LOGGED_OUT } from 'containers/HomePage/constants';

// The initial state of the App
const isAuthorised = sessionStorage.getItem('isAuthorised');
export const initialState = fromJS({
  isAuthorised: !!isAuthorised,
});

function heartBeatReducer(state = initialState, action) {
  switch (action.type) {
    case USER_LOGGED_OUT:
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('isAuthorised');
      return state.set('isAuthorised', false).set('loading', false);

    default:
      return state;
  }
}

export default heartBeatReducer;
