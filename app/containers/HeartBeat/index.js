/**
 * Created by sigma on 26/11/18.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IdleTimer from 'react-idle-timer';
import { createStructuredSelector } from 'reselect';
import {
  AuthHeartBeatIntervalInSeconds,
  IdleTimeoutInMinutes,
} from 'containers/Configuration/selectors';
import { validateToken, logoutUser } from './actions';

export class HeartBeat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startOnMount: false,
      timeout: 10 * 60 * 1000,
      debounce: 300,
      isAuthorisedInterval: 30 * 1000,
    };
    this.onIdle = this.onIdle.bind(this);
  }

  componentDidMount() {
    if (this.props.isAuthorised === true) this.initiateHeartBeat();
    else this.stopHeartBeat();
  }

  initiateHeartBeat() {
    const intervalId = setInterval(
      this.timer.bind(this),
      this.state.isAuthorisedInterval,
    );
    this.idleTimer.reset();
    // store intervalId in the state so it can be accessed later:
    this.setState({ intervalId });
  }

  stopHeartBeat() {
    if (this.props.isAuthorised === true) {
      this.props.logoutUser();
      this.state.intervalId ? clearInterval(this.state.intervalId) : '';
    }
    else {
      this.state.intervalId ? clearInterval(this.state.intervalId) : '';
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.isAuthorised !== prevProps.isAuthorised &&
      this.props.isAuthorised === true
    )
      this.initiateHeartBeat();
    else if (
      this.props.isAuthorised !== prevProps.isAuthorised &&
      this.props.isAuthorised === false
    )
      this.stopHeartBeat();
  }

  timer() {
    this.props.validateToken();
  }

  componentWillUnmount() {
    this.stopHeartBeat();
  }

  render() {
    return (
      <IdleTimer
        ref={ref => {
          this.idleTimer = ref;
        }}
        element={document}
        onIdle={this.onIdle}
        debounce={this.state.debounce}
        startOnMount={this.state.startOnMount}
        stopOnIdle
        timeout={this.state.timeout}
      />
    );
  }

  onIdle() {
    console.log('last active', this.idleTimer.getLastActiveTime());
    // call logout service and clear storage
    this.stopHeartBeat();
  }
}

HeartBeat.propTypes = {
  isAuthorised: PropTypes.bool,
  logoutUser: PropTypes.func,
  validateToken: PropTypes.func,
}

const mapStateToProps = createStructuredSelector({
  AuthHeartBeatIntervalInSeconds: AuthHeartBeatIntervalInSeconds(),
  IdleTimeoutInMinutes: IdleTimeoutInMinutes(),
});

const mapDispatchToProps = dispatch => ({
  validateToken: () => {
    // Validate token(if server deems it's valid token)
    dispatch(validateToken());
  },
  logoutUser: () => {
    dispatch(logoutUser());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HeartBeat);
