/**
 * Created by sigma on 23/11/18.
 */
import { USER_LOGGED_OUT } from 'containers/HomePage/constants';
import {
  VALIDATE_TOKEN,
  TOKEN_VALID,
  TOKEN_INVALID,
  USER_LOG_OUT,
} from './constants';

export function validateToken() {
  return {
    type: VALIDATE_TOKEN,
  };
}

export function tokenValid() {
  return {
    type: TOKEN_VALID,
  };
}

export function logoutUser() {
  return {
    type: USER_LOG_OUT,
  };
}

export function userLoggedOut() {
  return {
    type: USER_LOGGED_OUT,
  };
}
