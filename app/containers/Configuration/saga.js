/**
 * Get configurations related to portal
 */

import { call, put, takeLatest } from 'redux-saga/effects';
import { getRequest } from 'utils/request';
import { LOAD_CONFIGURATION } from './constants';
import { configLoaded, configLoadingError, configLogo } from './actions';

/**
 * Github repos request/response handler
 */
export function* getConfiguration() {
  let requestBase = '';
  if (process.env.NODE_ENV === 'production') {
    requestBase = window.location.origin;
  } else {
    requestBase = 'http://docitt.dev.lendfoundry.com:9004';
  }
  const requestURL = '/configuration/admin-portal';

  try {
    const config = yield call(getRequest, requestBase, requestURL);
    yield put(configLoaded(config));
  } catch (err) {
    yield put(configLoadingError(err));
  }
}

export function* getLogo() {
  let requestBase = '';
  if (process.env.NODE_ENV === 'production') {
    requestBase = window.location.origin;
  } else {
    requestBase = 'http://docitt.dev.lendfoundry.com:9004';
  }
  const url = '/theme/logo/default';

  try {
    const logoDetails = yield call(getRequest, requestBase, url);
    if (!logoDetails)
      logoDetails.isDefault = false;

    logoDetails.logoDisplay = true;

    yield put(configLogo(logoDetails));
  } catch (err) {
    const logoDetails = {isDefault : false, logoDisplay: true};
    yield put(configLogo(logoDetails));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* getConfigurationData() {
  yield takeLatest(LOAD_CONFIGURATION, getConfiguration);
  yield takeLatest(LOAD_CONFIGURATION, getLogo);
}
