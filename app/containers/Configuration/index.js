/*
 * Configuration
 *
 * This is where configuration is getting called
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { getConfiguration } from './actions';
import { errorLoadingConfig } from './selectors';
import reducer from './reducer';
import saga from './saga';

export class Configuration extends React.Component {
  componentDidMount() {
    this.props.dispatch(getConfiguration());
  }

  render() {
    return <h1>{this.props.error}</h1>;
  }
}

const mapStateToProps = createStructuredSelector({
  error: errorLoadingConfig(),
});

const withConnect = connect(
  mapStateToProps,
  null,
);

const withReducer = injectReducer({ key: 'config', reducer });
const withSaga = injectSaga({ key: 'config', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Configuration);
