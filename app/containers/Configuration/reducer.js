/*
 * ConfigReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import { CONFIGURATION_LOADED, CONFIGURATION_LOAD_ERROR, LOGO_UPDATED } from './constants';

// The initial state of the App
export const initialState = fromJS({
  loading: true,
  configurationloaded: false,
  error: false,
  portalConfig: {},
  logoPresent: false,
  logoDisplay: false,
  logoDetails: {},
});

function configReducer(state = initialState, action) {
  switch (action.type) {
    case CONFIGURATION_LOADED:
      return state
        .set('portalConfig', fromJS(action.config))
        .set('loading', false)
        .set('configurationloaded', true);

    case CONFIGURATION_LOAD_ERROR:
      const err = `${action.error} configuration.`;
      return state.set('error', err).set('loading', false);

    case LOGO_UPDATED:
      return state.set('logoPresent', action.logoDetails.isDefault)
        .set('logoDetails', action.logoDetails)
        .set('logoDisplay', action.logoDetails.logoDisplay);

    default:
      return state;
  }
}

export default configReducer;
