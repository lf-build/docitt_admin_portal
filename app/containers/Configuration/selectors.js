import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectConfig = state => state.get('config', initialState);

const errorLoadingConfig = () =>
  createSelector(selectConfig, selectConfig => selectConfig.get('error'));

const serviceUrl = serviceName =>
  createSelector(selectConfig, serviceConfig =>
    serviceConfig.getIn(['portalConfig', 'services', serviceName]),
  );

const configurationloaded = () =>
  createSelector(selectConfig, configSelect =>
    configSelect.get('configurationloaded'),
  );

const AuthHeartBeatIntervalInSeconds = () =>
  createSelector(selectConfig, selectConfig =>
    selectConfig.get('portalConfig'),
  );

const IdleTimeoutInMinutes = () =>
  createSelector(selectConfig, selectConfig =>
    selectConfig.get('portalConfig'),
  );

const tenantVariables = () =>
  createSelector(selectConfig, serviceConfig =>
    serviceConfig.getIn(['portalConfig', 'tenantVariables']).toJS(),
  );

const logoPresent = () =>
  createSelector(selectConfig, serviceConfig =>
    serviceConfig.get('logoPresent'),
  );

const logoDisplay = () =>
  createSelector(selectConfig, serviceConfig =>
    serviceConfig.get('logoDisplay'),
  );

const logoDetails = () =>
  createSelector(selectConfig, serviceConfig =>
    serviceConfig.get('logoDetails'),
  );

export {
  selectConfig,
  errorLoadingConfig,
  configurationloaded,
  AuthHeartBeatIntervalInSeconds,
  IdleTimeoutInMinutes,
  serviceUrl,
  tenantVariables,
  logoPresent,
  logoDisplay,
  logoDetails,
};
