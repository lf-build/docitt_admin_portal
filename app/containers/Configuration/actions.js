/**
 * Created by sigma on 20/11/18.
 */
import {
  LOAD_CONFIGURATION,
  CONFIGURATION_LOADED,
  CONFIGURATION_LOAD_ERROR,
  LOGO_UPDATED,
} from './constants';

export function getConfiguration() {
  return {
    type: LOAD_CONFIGURATION,
  };
}

export function configLoaded(config) {
  return {
    type: CONFIGURATION_LOADED,
    config,
  };
}

export function configLoadingError(error) {
  return {
    type: CONFIGURATION_LOAD_ERROR,
    error,
  };
}

export function configLogo(logoDetails) {
  return {
    type: LOGO_UPDATED,
    logoDetails,
  };
}
