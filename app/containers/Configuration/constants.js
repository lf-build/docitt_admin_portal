/**
 * Created by sigma on 20/11/18.
 */

export const LOAD_CONFIGURATION =
  'lendfoundry/Configuration/LOAD_CONFIGURATION';

export const CONFIGURATION_LOADED =
  'lendfoundry/Configuration/CONFIGURATION_LOADED';

export const CONFIGURATION_LOAD_ERROR =
  'lendfoundry/Configuration/CONFIGURATION_LOAD_ERROR';

export const LOGO_UPDATED =
  'lendfoundry/Configuration/LOGO_UPDATED';
