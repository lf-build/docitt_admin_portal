import { createGlobalStyle } from 'styled-components';
import './font-icons.css';

const GlobalStyle = createGlobalStyle`
*, html{box-sizing:border-box}

html, body { height:100%;}

body {font-family: 'Roboto', sans-serif;font-size:16px;font-weight:400;background:#f0f3f4}

p{ margin:0;}
ul,li{list-style:none; margin:0; padding:0;}
h1,h2,h3,h4,h5,h6{font-weight:normal; margin:0; padding:0;}
.no-pad { padding:0;}

.clearfix:after{
content: " ";
visibility: hidden;
display: block;
height: 0;
clear: both;
}

::-webkit-input-placeholder {color:#777;opacity:.8}
:-moz-placeholder {color:#777;opacity:.8}
::-moz-placeholder {color:#777;opacity:.8}
:-ms-input-placeholder {color:#777;opacity:.8}

a{color:#00A7F8;}

.grey{color:#6E6E6E;}
.blue{color:#00A7F8;}

.mtop0{margin-top:0px !important}
.mtop20{margin-top:20px}
.mtop30{margin-top:30px}
.mtop40{margin-top:40px}
.mtop50{margin-top:50px}
.mtop60{margin-top:60px}

.ptop10{padding-top:10px}

.border-right {border-right: 1px solid #95989A}
.border-left {border-left: 1px solid #95989A}
.btn{background:#EBEBEB;color: #fff;padding:12px 20px;font-size: 16px;line-height: 16px;font-weight:500;border:0;border-radius:4px;box-shadow:none;}
.btn:active{box-shadow:none;}

.btn-sm{font-size:14px;padding:10px 15px;}
.btn-xs{font-size:14px;padding:8px 40px;}
.btn-outline,
.btn-outline:focus,
.btn-outline:hover{background:transparent;border:2px solid #95989a;color:#95989A;outline:0;}
.btn-outline-primary{background:#fff; border-color:#00A7F8;color:#00A7F8;}
.btn-outline-secondary{background:#EBEBEB; border-color:#EBEBEB;color:#FFF;}

.btn-primary,
.btn-primary:hover,
.btn-primary.active:focus,
.btn-primary.active:hover{background: #00A7F8 !important;border:2px solid #00A7F8 !important;color: #fff !important;}

.clear-buttons{ padding-top:30px; text-align:right;}
.clear-buttons .btn{min-width:120px;}
.clear-buttons .btn-outline{ margin-right:6px;}

.btnpreview{margin-top:30px;}
.btnpreview .btn{padding:10px 15px;font-size:13px;margin:0;margin-right:10px}

.radio-group{ margin-top: 30px;}
.radio-group span {color: #00a7f8; display: block; font-size: 16px; margin-bottom:15px;}
.radio {margin:0; margin-bottom:20px; display:block;}
.radio input[type="radio"] {display: none;}
.radio input[type="radio"] + label {margin:0;padding:0;color: #B2B2B2;font-weight: normal;}
.radio input[type="radio"] + label span {display: inline-block;width:25px; height: 25px;vertical-align: middle; cursor: pointer; -moz-border-radius: 50%;border-radius: 50%;}
.radio input[type="radio"] + label span {background-color: #fff; border: 1px solid #bbb;}
.radio input[type="radio"]:checked + label{color:#777}
.radio input[type="radio"]:checked + label span {background-color: #00A7F8;border: 1px solid #bbb;box-shadow:inset 0px 0px 0px 2px #fff;}
.radio input[type="radio"] + label span,
.radio input[type="radio"]:checked + label span { -webkit-transition: background-color 0.4s linear; -o-transition: background-color 0.4s linear; -moz-transition: background-color 0.4s linear; transition: background-color 0.4s linear;margin:0; margin-right: 7px;}

.form-group {position: relative;margin-bottom: 30px;width:100%;}
.form-group .error{color:#f00;font-size:13px;}

.form-label {position: absolute;top: 0;padding: 0;margin: 0;transition: all 200ms;opacity: 0.5;font-size:16px;font-weight:normal;}
.form-control{height:inherit;padding: 4px 0;color: #555;border: 0;border-bottom:1px solid #ccc;background:transparent;border-radius: 0;box-shadow:none;}
.form-control:focus {border-color: transparent;box-shadow:none;border-bottom:1px solid #00A7F8;}
.form-control:focus + .form-label,
.form-control:valid + .form-label{font-size: 75%;transform: translate3d(0, -100%, 0);opacity: 1;}

/*switch on-off*/
.onoffswitch {
    position: relative;
	width:65px;
    -webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select: none;
}
.onoffswitch-checkbox {display: none;}
.onoffswitch-label {
    display: block;
	overflow: hidden;
	cursor: pointer;
    border: 2px solid #95989A;
	border-radius: 30px;
	margin:0;
}
.onoffswitch-inner {
    display: block; width: 200%; margin-left: -100%;
    -moz-transition: margin 0.3s ease-in 0s; -webkit-transition: margin 0.3s ease-in 0s;
    -o-transition: margin 0.3s ease-in 0s; transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before,
.onoffswitch-inner:after {
    display: block;
	float: left;
	width: 50%;
	height: 26px;
	padding: 0;
    font-size:14px;
	font-weight:normal;
	line-height: 28px;
	color: white;
    border-radius: 30px;
}
.onoffswitch-inner:before {
    content: "YES";
    padding-left:6px;
    color: #909090;
}
.onoffswitch-inner:after {
    content: "NO";
    padding-right:10px;
	color: #909090;
    text-align: right;
}
.onoffswitch-switch {
    display: block;
	width:22px; height:22px;
	margin: 0px;
    background: #00A7F8;
    border-radius:50%;
    position: absolute;
	top:4px;
	bottom: 0;
	right: 38px;
    -moz-transition: all 0.3s ease-in 0s;
	-webkit-transition: all 0.3s ease-in 0s;
    -o-transition: all 0.3s ease-in 0s;
	transition: all 0.3s ease-in 0s;
}

.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 5px;
}

/*header nav bar*/
.navbar-fixed-top {z-index: 9999;border:0;}
.navbar-fixed-top .navbar-collapse {border: 0;}

.navbar-toggle {margin-top:15px;}
.navbar-toggle .icon-bar {background: #fff;}

.navbar-right{margin:0 !important;display:flex;align-items:center;}
.navbar-right a{text-decoration: none;}
.navbar-right div{float:left;}
.navbar-right .df{color:#ccc;font-size: 30px;padding:0 15px;    display: inline-flex;
    align-items: center;height:60px;}
.navbar-right .df-bell{font-size: 34px;}
.navbar-right .df-no-user{width:50px;font-size: 50px;margin:0 10px;padding:0;}

/*Menu drop down*/
.menu-bar{
  display: inline-block;
}
.menu-bar-item{
  position: relative;
  display: inline-flex;
  height:60px;
  align-items:center;
}
.menu-bar-item a{
  text-decoration: none;
  color: #ccc;
  display:inline-flex;
  align-items: center;
}
.menu-bar-item .df-angle-down-o{
  font-size:18px;
}
.menu-bar-item .active{
background: #58595B;
}
.mega-menu{
  display: none;
}
.mega-menu.true{
  display: block;
}
.mega-menu-content{
background: #58595B;
color: #fff;
padding:0;
position: absolute;
top:60px;
right:0;
}
.mega-menu-content .btn{background:transparent !important;border:0 !important;padding:0;color:#fff}
.mega-menu-content a{display:block;background:#58595B;color:#fff;padding:15px 20px;border-bottom:1px solid #fff;text-decoration:none;}
.mega-menu-content span{display:block;}

/*tabs*/
.tabs {border:0;margin:0;}
.tabs__labels{overflow:hidden;}
.tabs__labels li {border:0;margin:0;width: 50%;float:left;}
.tabs__labels button{display:block;margin:0;padding:10px 0;border:1px solid transparent;border-bottom:1px solid #fff;background:#f0f0f0;
    border-radius:0;color:#6E6E6E; font-weight:400;text-align:center;outline:0;text-decoration: none;width:100%;}
.tabs__labels .active,
.tabs__labels .active:focus,
.tabs__labels .active:hover {border:1px solid #95989A;border-bottom:transparent;background:#fff;color:#00A7F8}
.tabs .tabs__content{margin:0;padding:30px;border:0; border-left:1px solid #95989A;border-right:1px solid #95989A; margin-top:-1px;}

.back-btn{ margin:20px 0}
.back-btn a{ display:inline-block;line-height:30px;vertical-align:top;}
.back-btn .df{float:left;font-size:20px; margin-right:10px; font-size:30px;}

.backbtn{margin-top:12px;}
.backbtn .df{font-size:30px; width:20px; height:20px; line-height:20px; margin-right:15px; color:#95989A;}
.backbtn .df:before{line-height:20px; vertical-align:middle}
.backbtn a{font-size:28px;color:#95989A;}
.backbtn a:hover,
.backbtn a:focus,
.backbtn a:hover .df,
.backbtn a:focus .df{color:#00A7F8;}

.sidebar {
left:0;
list-style: none;
height: 100%;
margin: 0;
overflow-x:hidden;  
overflow-y: auto;
padding: 0;
width: 250px;
position: fixed;
top:60px;
z-index: 1000;
background:#95989a;
}
.sidebar-toggle {position: absolute;padding: 9px 10px;top:6px;right: 13px;background-color: transparent;
background-image: none;border: 1px solid transparent;border-radius: 4px;z-index: 9999;}
.sidebar-toggle .icon-bar {display: block;width: 22px;height: 3px;margin:5px 0;border-radius: 1px;background: #125071;}

.sidebar .back{ background:#fff;color:#95989A; padding:20px;display:block;border:0;font-size:18px;line-height:20px;text-decoration:none;}
.sidebar .back .df{float:left;margin-right:15px; font-size:22px; width:22px; height:22px; line-height:22px;}
.sidebar .back:hover,
.sidebar .back:focus,
.sidebar .back:hover .df,
.sidebar .back:focus .df{color:#00A7F8;}

.metismenu{border-top:1px solid #909395;font-family: 'Roboto', sans-serif;}
.metismenu li{float:none;background:#95989a;border-bottom:1px solid #fff;}
.metismenu li a{color:#fff;padding:15px 20px !important;display:block;font-size:16px;font-family: 'Roboto', sans-serif;
height:inherit !important;line-height:16px !important;text-decoration:none;text-shadow: none;}
.metismenu ul li ul li{background:#58595b;border-bottom:1px solid #4f5052;}
.metismenu li a.active{background:#00A7F8;}
.metismenu li a.has-active-child{color:#fff;}

.metismenu li .metismenu-container{height:0;overflow:hidden;clear:both;}
.metismenu li .metismenu-container.visible{height:auto;padding:0;}
.metismenu li .metismenu-container.visible li a{padding-left:35px !important;}
.metismenu-state-icon{line-height:normal !important;}

.flexbox-fix {display: none !important}

.container {width: 80%;}
#app{width:100%;height:100%;padding:0;}

.header{background:#006699;float:none;margin:0;min-height:60px;}
.logo { float:left; margin:10px 0; margin-left:20px}
.logo img{width:240px; max-height:50px}

.flex-wrap{display:flex;justify-content:center;align-items:center;width:100%; height:100vh;background:#fff;}
.full-height{width:100%;height:100%;}

.login{width:335px;margin-top:60px;}
.login h1 {color: #1268AD;font-weight: 700;font-size: 32px;line-height: 32px;margin: 0 0 10px;}
.login p {color: #688b9c;line-height: 20px;margin: 0 0 50px;}
.login .btn{padding:17px 0;}

.backbtn{margin-top:12px;}
.backbtn .df{font-size:30px; width:20px; height:20px; line-height:20px; margin-right:15px; color:#95989A;}
.backbtn .df:before{line-height:20px; vertical-align:middle}
.backbtn a{font-size:28px;color:#95989A;}
.backbtn a:hover,
.backbtn a:focus,
.backbtn a:hover .df,
.backbtn a:focus .df{color:#00A7F8;}

.dashboard{padding:120px 30px 0 30px;margin-left:250px;}
.dashboard .padd{padding:30px;background:#fff;}
.dashboard .headertitle{padding:20px; background:#00A7F8; color:#fff;}
.dashboard .headertitle span{ display:inline-block;float: left;font-size:30px;width:30px; height:30px; line-height:30px; margin-right:20px;margin-top: -4px;}
.dashboard .nav-tabs{ margin:0;}
.dashboard .heading{color:#58595B;font-size:16px; margin-bottom:15px;}
.dashboard .tab-content{ margin-top:40px;}
.dashboard .sortby{padding-top:8px;}
.dashboard .text-right{ padding-top:10px;}
.dashboard .text-right a{color:#777;}

@media only screen and (min-height:800px){
.btn-bottom{ position:fixed;margin:0;left:0;bottom:0;}
}

@media only screen and (max-width:1367px){
.container {width:94%;}
}
@media only screen and (max-width:1169px){
.dashboard .text-right {padding-top:40px;}
}
@media only screen and (max-width:1025px){
.container {width:100%;}
}

@media only screen and (max-width:1023px){
.modal-content {width:50%;}
}

@media only screen and (max-width:768px) {
.sldebar-nav {
left: 220px;
margin-left: -220px;
width: 220px;
} 
.sldebar-toggle {left:170px;}
}
@media only screen and (max-width:800px){
.dashboard {margin: 0 15px;margin-top: 60px;}
.dashboard .sortby{padding:0;padding-top:8px;}
}

@media only screen and (max-width:767px){
.navbar-collapse.in {overflow-y: inherit;}
	
.navbar-collapse {padding:0;text-align: center;}

.navbar-nav > li {display: inline-block;vertical-align: top;}
.navbar-nav .open a:hover{background-color:transparent;}
.navbar-nav .open .dropdown-menu{width: 280px;position: absolute;}
.navbar-nav .open .dropdown-menu.prfle{width:auto;}
.navbar-nav .open .dropdown-menu > li > a {padding: 5px 0;}

.nav-tabs {margin-bottom:30px;}

.modal-content {width:70%;}

.sidebar-toggle{ display:none;}
.sidebar-nav{left: 0;margin: 0;overflow: inherit;width: 100%;position: relative;}
.sidebar .navbar-collapse{ display:block;}
.sidebar .navbar-nav > li {display: block;}

.sidebar .profile .pic,
.sidebar .profile .pic img {width:160px;}
.sidebar .profile .icn-chat{right:28px;}

.dashboard{margin:20px; margin-bottom:0;}
.dashboard .padd {padding:20px;}
}

@media only screen and (max-width:479px){
.modal-content {width:90%; padding:20px;}

.dashboard .col-xs-12 {padding:0;}	
.dashboard .col-xs-5,
.dashboard .col-xs-7{width:100%;}
}

@media only screen and (max-width:380px){
.nav-tabs > li > a{padding: 10px 5px;font-size: 12px;}
.dropdown.open .dropdown-menu.notifications {left: -110px;}
.dropdown.open .dropdown-menu.helpdrop {left:0;}

.login{padding:0 15px;}

.dashboard{margin:20px 15px; margin-bottom:0;}
}

@media only screen and (max-width:330px){
.dropdown.open .dropdown-menu.helpdrop {left:-100%;}
}
`;

export default GlobalStyle;
