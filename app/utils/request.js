/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      throw error;
    }

    return data;
  });
}

export function getRequest(base, url, payload) {
  const fullPath = base + url;
  return request(fullPath, payload, 'GET');
}

export function postRequest(base, url, payload) {
  const fullPath = base + url;
  return request(fullPath, payload, 'POST');
}

export function putRequest(base, url, payload = '') {
  const fullPath = base + url;
  return request(fullPath, payload, 'PUT');
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @param  {string}           method type
 *
 * @return {object}           The response data
 */
export function request(url, payload, requestType) {
  return fetch(url, {
    method: requestType,
    cache: 'no-cache',
    body: JSON.stringify(payload),
    headers: authHeader(requestType),
  }).then(handleResponse);
}

export function fileUploadRequest (base, url, payload) {
  const fullPath = base + url;
  return formDataRequest(fullPath, payload, 'POST');
}

export function formDataRequest(url, payload, requestType) {
  var formData = new FormData();
  formData.append('file', payload);
  const authHeaders = authHeader(requestType);
  delete authHeaders['Content-Type']
  return fetch(url, {
    method: requestType,
    cache: 'no-cache',
    body: formData,
    headers: authHeaders,
  }).then(handleResponse);
}

export function authHeader(requestType) {
  // Check if body is sent
  const sendBody = ['POST', 'PUT', 'DELETE'];
  // return authorization header with jwt token
  const isAuthorised = sessionStorage.getItem('isAuthorised');
  const token = sessionStorage.getItem('token');
  const contentType =
    sendBody.indexOf(requestType) !== -1 ? 'application/json' : '';
  if (isAuthorised && token) {
    return {
      'Content-Type': contentType,
      Authorization: `Bearer ${token}`,
    };
  }
  return { 'Content-Type': contentType };
}
