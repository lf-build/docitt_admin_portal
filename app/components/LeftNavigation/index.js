/**
 * Created by sigma on 18/12/18.
 */
import React from 'react';
import MetisMenu from 'react-metismenu';
import RouterLink from 'react-metismenu-router-link';

export default class LeftNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: [
        {
          label: 'Build',
          to: 'dashboard',
        },
        {
          label: 'Design',
          content: [
            {
              label: 'Colors',
              to: 'colors',
            },
            {
              label: 'Fonts',
              to: 'fonts',
            },
            {
              label: 'Graphics',
              to: 'graphics',
            },
          ],
        },
        {
          label: 'Text',
          content: [
            {
              label: 'Template List',
              to: 'template',
            },
            {
              label: 'Sub Template',
              to: 'sub-template',
            },
          ],
        },
      ],
    };
  }

  render() {
    return (
      <div className="sidebar">
        <a href="#" className="back">
          <span className="df df-back" /> Dashboard
        </a>
        <button
          type="button"
          href="#menu-toggle"
          className="sidebar-toggle"
          id="menu-toggle"
        >
          <span className="sr-only">Toggle sidebar</span>
          <span className="icon-bar" />
          <span className="icon-bar" />
          <span className="icon-bar" />
        </button>
        <MetisMenu
          content={this.state.menus}
          activeLinkFromLocation
          LinkComponent={RouterLink}
          iconNamePrefix="df df-"
          iconNameStateVisible="angle-down-o"
          iconNameStateHidden="angle-up-o"
        />
      </div>
    );
  }
}
