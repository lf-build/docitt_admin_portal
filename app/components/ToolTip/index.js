/**
 * Created by sigma on 31/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import './tooltip.css';

class Tooltip extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      displayTooltip: false
    }
    this.hideTooltip = this.hideTooltip.bind(this)
    this.showTooltip = this.showTooltip.bind(this)
  }

  hideTooltip() {
    this.setState({displayTooltip: false})
  }

  showTooltip() {
    this.setState({displayTooltip: true})
  }

  render() {
    const {message, position, children} = this.props;

    return (
      <span className='tooltip'
            onMouseLeave={this.hideTooltip}
      >
        {this.state.displayTooltip &&
        <div className={`tooltip-bubble tooltip-${position}`}>
          <div className='tooltip-message'>{message}</div>
        </div>
        }
        <span
          className='tooltip-trigger'
          onMouseOver={this.showTooltip}
        >
          {children}
        </span>
      </span>
    )
  }
}

Tooltip.propTypes = {
  message: PropTypes.string,
  position: PropTypes.string,
}

export default Tooltip;