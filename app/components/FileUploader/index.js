/**
 * Created by sigma on 30/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box',
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%',
};

class FileUploader extends React.Component {
  constructor() {
    super();
    this.removeFile = this.removeFile.bind(this);
    this.dropzoneRef = React.createRef();
  }

  showDialog() {
    this.dropzoneRef.current.open();
  }

  onDrop = (files, rejected) => {
    if (rejected.length > 0) {
      if (rejected[0].size > this.props.maxFileSize) {
        Alert.error('File size greater that expected');
      } else {
        Alert.error('Invalid File type');
      }
    } else {
      this.props.onFileUploadHandler(files);
    }
  };

  removeFile(file) {
    const array = [...this.props.files];
    const index = array.indexOf(file);
    if (index !== -1) {
      this.props.onRemoveFileHandler(index);
      URL.revokeObjectURL(file.preview);
    }
  }

  componentWillUnmount() {
    // Make sure to revoke the data uris to avoid memory leaks
    // this.props.files.forEach(file => URL.revokeObjectURL(file.preview))
  }

  render() {
    const { files, allowedFileType, children, maxFileSize } = this.props;

    const thumbs = files.map(file => (
      <div style={thumb} key={file.name}>
        {/* <span onClick={() => this.removeFile(file)}>X</span> */}
        <div style={thumbInner}>
          <img alt={file.name} src={file.preview} style={img} />
        </div>
      </div>
    ));

    let uploadClass = 'file-upload';
    if (files.length > 0) {
      uploadClass += ' hide-icon';
    }
    return (
      <section>
        <Dropzone
          ref={this.dropzoneRef}
          accept={allowedFileType || 'image/png'}
          multiple={false}
          maxSize={maxFileSize || 2000000}
          onDrop={this.onDrop}
        >
          {({ getRootProps, getInputProps }) => (
            <div className={uploadClass} {...getRootProps()}>
              <input {...getInputProps()} />
              <p>{children || 'Drop files here'}</p>
            </div>
          )}
        </Dropzone>
        {!!files.length && <aside style={thumbsContainer}>{thumbs}</aside>}
      </section>
    );
  }
}

FileUploader.propTypes = {
  files: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onFileUploadHandler: PropTypes.func,
  onRemoveFileHandler: PropTypes.func,
};

export default FileUploader;
