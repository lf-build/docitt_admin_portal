/**
 * Button Component
 * Created by sigma on 22/11/18.
 */

import React from 'react';
import PropTypes from 'prop-types';

const TYPES = {
  PRIMARY: 'btn-primary',
  OUTLINE: 'btn-outline',
  WARNING: 'warning',
  DANGER: 'danger',
  SUCCESS: 'success',
};

const SIZES = {
  SMALL: 'btn-xs',
  MEDIUM: 'btn-sm',
  LARGE: 'btn-block',
  XTRALARGE: 'btn-lg',
};
// eslint-disable-next-line react/prefer-stateless-function
export default class Button extends React.Component {
  render() {
    return (
      <button
        type={this.props.type}
        disabled={this.props.disabled}
        onClick={this.props.clickHandler}
        className={`btn ${
          this.props.buttonType ? this.props.buttonType : TYPES.PRIMARY
        } ${this.props.buttonSize ? this.props.buttonSize : SIZES.MEDIUM}`}
      >
        {this.props.text}
      </button>
    );
  }
}
Button.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};
