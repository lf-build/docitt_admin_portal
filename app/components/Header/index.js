/**
 * Button Component
 * Created by sigma on 22/11/18.
 */

import React from 'react';
import PropTypes from 'prop-types';
import Logo from 'components/Logo';

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      clicked: false,
    };
  }

  componentWillMount() {
    document.addEventListener('click', this.handleOutsideClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.isAuthorised === true &&
      this.props.isAuthorised === false
    ) {
      this.setState({
        clicked: false,
      });
    }
  }

  handleClick = () => {
    this.setState({ clicked: !this.state.clicked });
  };

  handleOutsideClick = event => {
    if (this.refs.megaMenu && !this.refs.megaMenu.contains(event.target)) {
      this.setState({
        clicked: false,
      });
    }
  };

  render() {
    const { isAuthorised, children, logoPresent, logoDisplay, logoDetails } = this.props;
    return (
      <nav className="header navbar navbar-fixed-top" role="navigation">
        <div className="navbar-header">
          <Logo icon="white" logoPresent={logoPresent} logoDisplay={logoDisplay} logoDetails={logoDetails} />
        </div>
        <div className="navbar-right">
          {isAuthorised && (
            <div>
              <div style={{ display: 'none' }}>
                {' '}
                <a
                  href="javascript:;"
                  className="df df-help"
                  data-toggle="dropdown"
                />
                <a
                  href="javascript:;"
                  className="df df-bell"
                  data-toggle="dropdown"
                />
              </div>
              <div className="menu-bar">
                <div className="menu-bar-item" ref="megaMenu">
                  <a
                    className={this.state.clicked === true ? 'active' : ''}
                    href="javascript:void(0)"
                    onClick={this.handleClick}
                  >
                    <span className="df df-no-user img-circle" />
                    Susanna <i className="df df-angle-down-o" />
                  </a>
                  <div className={`${'mega-menu' + ' '}${this.state.clicked}`}>
                    <div className="mega-menu-content">
                      <ul>
                        <li>
                          <a href="javascript:void(0)">
                            Susanna
                            <span>susanna@gmail.com</span>
                          </a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">{children}</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Settings</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  isAuthorised: PropTypes.bool,
  children: PropTypes.element,
};

export default Header;
