/**
 * Created by sigma on 26/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';

export default class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected,
    };
  }

  handleClick(index, event) {
    event.preventDefault();
    this.setState({
      selected: index,
    });
  }

  labels(child, index) {
    const activeClass = this.state.selected === index ? 'active' : '';
    return (
      <li key={index}>
        <button
          className={activeClass}
          onClick={this.handleClick.bind(this, index)}
          type="button"
        >
          {child.props.label}
        </button>
      </li>
    );
  }

  renderTitles() {
    return (
      <ul className="tabs__labels">
        {this.props.children.map(this.labels.bind(this))}
      </ul>
    );
  }

  renderContent() {
    return (
      <div className="tabs__content">
        {this.props.children[this.state.selected]}
      </div>
    );
  }

  render() {
    return (
      <div className="tabs">
        {this.renderTitles()}
        {this.renderContent()}
      </div>
    );
  }
}
Tabs.propTypes = {
  selected: PropTypes.number.isRequired,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element])
    .isRequired,
};
