/**
 * Created by sigma on 11/1/19.
 */
import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-s-alert';

export default class ErrorMessage extends React.PureComponent {
  render() {
    return (
      <div className="alert alert-danger">
        <ul><li>{this.props.errorMessages}</li></ul>
      </div>
    );
  }
}
ErrorMessage.propTypes = {
  errorMessages: PropTypes.any.isRequired,
};

export const showMessageSuccess = (msg = 'Changes applied successfully') => {
  Alert.success(msg);
};

export const showErrorMessage = (msg = 'Oops! Something went wrong, please try again') =>{
  Alert.error(`${msg}`)
}