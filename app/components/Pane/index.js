/**
 * Created by sigma on 26/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';

// eslint-disable-next-line react/prefer-stateless-function
export default class Pane extends React.Component {
  render() {
    return <div>{this.props.children}</div>;
  }
}
Pane.propTypes = {
  children: PropTypes.element.isRequired,
};
