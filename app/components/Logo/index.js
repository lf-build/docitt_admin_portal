/**
 * Created by sigma on 27/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';
import LogoIcon from './logo.svg';
import LogoGreyIcon from './logo-grey.svg';
import logoDefault from './logo-default.png';

function Logo(props) {
  let icon = "";
  if(props.logoPresent)
  icon = props.icon === 'grey' ? props.logoDetails.badgeLogoUrl : props.logoDetails.logoUrl;
  else
    icon = logoDefault

  return (
    <a className="logo" href="#" title="Admin">
      {props.logoDisplay && <img src={icon} alt="Docitt" />}
    </a>
  );
}
Logo.propTypes = {
  icon: PropTypes.string.isRequired,
};
export default Logo;
