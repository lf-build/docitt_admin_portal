/**
 * Created by sigma on 20/2/19.
 */
import React from 'react';

import './popup.css';

export default class PopUp extends React.Component {

  render() {
    const {visible, children, title} = this.props
    return (
      <div className="popup-container">
        <div className={`modal fade centered-modal ${visible ? ' active in ' : ''}`} id="myModal">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">{title}</h4>
              </div>

              <div className="modal-body">
                {children}
              </div>

            </div>
          </div>
        </div>
        {visible &&
        <div className="modal-backdrop fade in"></div>
        }
      </div>
    );
  }
}