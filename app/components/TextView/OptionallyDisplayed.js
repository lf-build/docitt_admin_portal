import React from 'react';
import PropTypes from 'prop-types';

export default class OptionallyDisplayed extends React.Component {
  render() {
    return this.props.display === true ? (
      <span className="error">{this.props.children}</span>
    ) : null;
  }
}
OptionallyDisplayed.propTypes = {
  display: PropTypes.bool.isRequired,
};
