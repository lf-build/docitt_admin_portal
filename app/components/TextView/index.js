/**
 * Text Box Component
 */

import React from 'react';
import PropTypes from 'prop-types';
import OptionallyDisplayed from './OptionallyDisplayed';

export default class TextField extends React.Component {
  constructor(props) {
    super(props);
    this.shouldDisplayError = this.shouldDisplayError.bind(this);
  }

  shouldDisplayError() {
    return this.props.showError && this.props.errorText !== '';
  }

  render() {
    return (
      <div className="form-group">
        <input
          id={this.props.name}
          type={this.props.type || 'text'}
          value={this.props.text}
          onChange={this.props.onFieldChanged}
          className="form-control"
          autoComplete="off"
          required
        />
        {this.props.placeholder && (
          <label className="form-label" htmlFor={this.props.name}>
            {this.props.placeholder}
          </label>
        )}
        <OptionallyDisplayed display={this.shouldDisplayError()}>
          {this.props.errorText}
        </OptionallyDisplayed>
      </div>
    );
  }
}

TextField.propTypes = {
  showError: PropTypes.bool.isRequired,
  onFieldChanged: PropTypes.func.isRequired,
};
