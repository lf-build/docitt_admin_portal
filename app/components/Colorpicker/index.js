/**
 * Created by sigma on 19/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { CompactPicker } from 'react-color';

export default class ColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayColorPicker: false,
      color: {
        width: '90px',
        height: '70px',
        background: this.props.defaultColor || '#1268AD',
      },
      swatch: {
        background: '#fff',
        display: 'block',
        cursor: 'pointer',
      },
      popover: {
        position: 'absolute',
        zIndex: '2',
      },
      cover: {
        position: 'fixed',
        top: '0',
        right: '0',
        bottom: '0',
        left: '0',
      },
      defaultColor: [
        'ffffff',
        '#000002',
        '#f0efdc',
        '#204d87',
        '#4480c8',
        '#c34845',
        '#97bf49',
        '#8461aa',
        '#3dabca',
        '#ff8d24',
        '#f2f2f2',
        '#848484',
        '#e2dcc1',
        '#bdd7fd',
        '#d9e3f0',
        '#f6dcdf',
        '#ecf2d7',
        '#e9dfe7',
        '#c9edf4',
        '#ffe8cf',
        '#dadada',
        '#575757',
        '#c3c090',
        '#85b5ee',
        '#b4cde8',
        '#e7b9b3',
        '#d5e6b4',
        '#cdbfdb',
        '#afdde4',
        '#ffd2ac',
        '#c1c1c1',
        '#404040',
        '#948a4f',
        '#508dca',
        '#92b1e0',
        '#e19296',
        '#c3d992',
        '#b3a2bf',
        '#8dccdf',
        '#f6c284',
        '#9f9f9f',
        '#292929',
        '#494526',
        '#0d3463',
        '#305e98',
        '#97352f',
        '#779429',
        '#5c4575',
        '#287e9a',
        '#e96a00',
        '#818181',
        '#0a0a0a',
        '#1c1a0a',
        '#0a2643',
        '#213f63',
        '#69221e',
        '#51621d',
        '#403053',
        '#1e5764',
        '#9b4500',
      ],
    };
    this.handleClick = this.handleClick.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.defaultColor !== state.color.background) {
      return {
        color: {
          width: '90px',
          height: '70px',
          background: props.defaultColor,
        },
      };
    }

    // Return null to indicate no change to state.
    return null;
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = color => {
    this.props.onColorChanged(color, this.props.shade);
  };

  render() {
    return (
      <div className="colorbox">
        <div style={this.state.swatch} onClick={this.handleClick}>
          <div style={this.state.color} />
          <i className="df df-angle-down-o"></i>
        </div>
        {this.state.displayColorPicker ? (
          <div style={this.state.popover}>
            <i className="df df-angle-up-o dfup"></i>
            <div style={this.state.cover} onClick={this.handleClose} />
            <CompactPicker
              colors={this.state.defaultColor}
              onChange={this.handleChange}
            />
          </div>
        ) : null}
      </div>
    );
  }
}
ColorPicker.propTypes = {
  onColorChanged: PropTypes.func.isRequired,
  shade: PropTypes.string,
};