/**
 * Created by sigma on 28/12/18.
 */
import React from 'react';
import PropTypes from 'prop-types';

export default class Switch extends React.Component {
  onFieldChanged(e) {
    this.props.valueChanged(e);
  }

  render() {
    const {name} = this.props;
    return (
      <div className="onoffswitch">
        <input
          type="checkbox"
          name={name ? name : 'onoffswitch'}
          className="onoffswitch-checkbox"
          id={name ? name : 'onoffswitch'}
          checked={this.props.value}
          onChange={this.onFieldChanged.bind(this)}
        />
        <label className="onoffswitch-label" htmlFor={name ? name : 'onoffswitch'}>
          <span className="onoffswitch-inner" />
          <span className="onoffswitch-switch" />
        </label>
      </div>
    );
  }
}
Switch.propTypes = {
  valueChanged: PropTypes.func.isRequired,
  value: PropTypes.bool.isRequired,
};