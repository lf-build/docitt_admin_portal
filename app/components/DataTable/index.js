/**
 * Created by sigma on 13/2/19.
 */
import React from 'react';
import TextView from 'components/TextView';
import Select from 'react-select';
import Cell from './cell';
import './dataTable.css';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
};

export default class DataTable extends React.Component {
  constructor(props) {
    super(props);
    const { pageLimit = 10, pageNeighbours = 0 } = this.props;
    const totalRecords = this.props.rows.length;

    this.pageLimit = typeof pageLimit === 'number' ? pageLimit : 30;
    this.totalRecords = typeof totalRecords === 'number' ? totalRecords : 0;
    this.pageNeighbours =
      typeof pageNeighbours === 'number'
        ? Math.max(0, Math.min(pageNeighbours, 2))
        : 0;

    this.totalPages = Math.ceil(this.totalRecords / this.pageLimit);
    this.state = {
      currentPage: 1,
      currentVisibleRows: [],
      searchText: '',
      rows: props.rows,
      selectedOption: { value: 10, label: '10' },
      filters: [],
      options: [
        { value: 10, label: '10' },
        { value: 20, label: '20' },
        { value: 30, label: '30' },
        { value: 50, label: '50' },
      ],
    };
  }

  componentDidMount() {
    // this.gotoPage(1);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.rows !== this.props.rows && this.props.rows.length > 0) {
      const totalRecords = this.props.rows.length;

      //--------------------------------------------------
      const { headings } = this.props;
      let index = 1;
      const sorting = headings.filter(item => item.sortable)

      if(sorting.length === 1)
      {
        sorting.map(item =>{
          if(Object.values(item)[1] === "templateType"){
            this.props.rows.sort((obj1, obj2) =>{
              return Object.values(obj1)[3].toLowerCase() >  Object.values(obj2)[3].toLowerCase() ? 1 : -1
            })
          }
          else if(Object.values(item)[1] === "name"){
            this.props.rows.sort((obj1, obj2) =>{
              return Object.values(obj1)[6].toLowerCase() >  Object.values(obj2)[6].toLowerCase() ? 1 : -1
            })
          }
          else if(Object.values(item)[1] === "status"){
            this.props.rows.sort((obj1, obj2) =>{
              return Object.values(obj1)[8].toLowerCase() >  Object.values(obj2)[8].toLowerCase() ? 1 : -1
            })
          }
        })
      }
      else if(sorting.length >=2){
        this.props.rows.sort((obj1,obj2) =>{
          if(Object.values(obj1)[3] === Object.values(obj2)[3]){
            return Object.values(obj1)[6].toLowerCase() >  Object.values(obj2)[6].toLowerCase() ? 1 : -1
          }
          else{
            return Object.values(obj1)[3].toLowerCase() >  Object.values(obj2)[3].toLowerCase() ? 1 : -1
          }
        })
      }

      this.props.rows.map(item =>{
        item.id = index++
      })

      //------------------------------------------------------

      this.totalRecords = typeof totalRecords === 'number' ? totalRecords : 0;

      this.totalPages = Math.ceil(this.totalRecords / this.pageLimit);
      this.gotoPage(this.state.currentPage);
    }
  }

  gotoPage = (page, rows = this.props.rows) => {
    const {headings} = this.props;
    const {searchText, filters} = this.state;
    let dataSet = rows;
    const currentPage = Math.max(1, Math.min(page, this.totalPages));
    const offset = (currentPage - 1) * this.pageLimit;

    let currentVisibleRows = dataSet.slice(offset, offset + this.pageLimit);

    if (filters.length > 0) {
      const filteredData = dataSet.filter(item => {
        let isFound = true;
        filters.forEach(key => {
          isFound =
            isFound &&
            (item[Object.keys(key)[0]] && key[Object.keys(key)[0]] && item[Object.keys(key)[0]].toLowerCase() === key[Object.keys(key)[0]].toLowerCase());
        });
        return isFound;
      });
      currentVisibleRows = filteredData.slice(offset, offset + this.pageLimit);
      this.totalPages = Math.ceil(filteredData.length / this.pageLimit);
      dataSet = filteredData;
    }

    const searchKeyList = headings.filter(item => item.searchable);
    if (searchKeyList.length > 0 && searchText.length >= 0) {
      const filteredData = dataSet.filter(item => {
        let isFound = false;
        searchKeyList.forEach(key => {
          isFound =
            isFound ||
            (item[key.name] && item[key.name].toLowerCase().includes(searchText.toLowerCase()));
        });
        return isFound;
      });
      currentVisibleRows = filteredData.slice(offset, offset + this.pageLimit);
      this.totalPages = Math.ceil(filteredData.length / this.pageLimit);
    }

    let paginationPanel = false;
    let rightButtonEnable = true;
    let leftButtonEnable = false;
    if (this.totalPages > 0) {
      paginationPanel = true;
      if (currentPage > 1) {
        leftButtonEnable = true;
      }
      if (currentPage === this.totalPages) {
        rightButtonEnable = false;
      }
    }

    this.setState({
      currentPage,
      currentVisibleRows,
      paginationPanel,
      leftButtonEnable,
      rightButtonEnable,
      totalPages: this.totalPages,
    });
    return true;
  };

  handleClick = page => evt => {
    evt.preventDefault();
    this.gotoPage(page);
  };

  handleMoveLeft = evt => {
    evt.preventDefault();
    if (this.state.currentPage !== 1) {
      this.gotoPage(this.state.currentPage - this.pageNeighbours * 2 - 1);
    }
  };

  handleMoveRight = evt => {
    evt.preventDefault();
    this.gotoPage(this.state.currentPage + this.pageNeighbours * 2 + 1);
  };

  rowClicked = rowData => {
    if (this.props.rowClick) this.props.rowClick(rowData);
  };

  rowDblClicked = rowData => {
    if (this.props.rowSelected) this.props.rowSelected(rowData);
  };

  renderHeadingRow = (cell, cellIndex) => {
    const { headings } = this.props;

    const filter = cell.filter ? (
      <Select onChange={this.filterColumnData(cell.name)} isSearchable={false}
              options={cell.filterValue} />
    ) : '';

    return <Cell key={`heading-${cellIndex}`} content={cell.header} header child={filter} />;
  };

  renderRow = (row, rowIndex) => {
    const { headings } = this.props;

    return (
      <tr
        key={`row-${rowIndex}`}
        className={row.isSelected ? 'row-selected' : ''}
        onClick={() => this.rowClicked(row)}
        onDoubleClick={() => this.rowDblClicked(row)}
      >
        {headings.map((cellKey, cellIndex) => (
          <Cell
            key={`${rowIndex}-${cellIndex}`}
            content={row[cellKey.name]}
            className={cellKey.dynamicClassName}
          />
        ))}
      </tr>
    );
  };

  changePageLimit = value => {
    this.setState({ selectedOption: value });
    this.pageLimit = value.value;
    this.totalPages = Math.ceil(this.totalRecords / this.pageLimit);
    this.gotoPage(1);
  };

  filterColumnData = name => value => {
    let existingFilter = this.state.filters;
    if (existingFilter && existingFilter.length > 0) {
      const index = existingFilter.findIndex(x => x[name]);
      if (index >= 0) {
        if (value.value)
          existingFilter[index][name] = value.value
        else
          existingFilter.splice(index, 1)
      }
      else {
        if (value.value)
          existingFilter.push({[name]: value.value})
      }
    }
    else {
      if (value.value)
        existingFilter.push({[name]: value.value})
    }

    this.setState({
      'filters': existingFilter
    })
    this.gotoPage(1);
  }

  handleFieldChanged = () => e => {
    this.setState({
      searchText: e.target.value,
    });
    setTimeout(() => {
      this.gotoPage(1);
    }, 0);
  };

  render() {
    const { headings, rows } = this.props;
    const {
      currentPage,
      totalPages,
      paginationPanel,
      leftButtonEnable,
      rightButtonEnable,
      currentVisibleRows,
    } = this.state;

    this.renderHeadingRow = this.renderHeadingRow.bind(this);
    this.renderRow = this.renderRow.bind(this);

    const theadMarkup = (
      <tr key="heading">{headings.map(this.renderHeadingRow)}</tr>
    );

    const tbodyMarkup = currentVisibleRows.map(this.renderRow);

    return (
      <div>
        <div className="filtering">
          <div className="search pull-left">
            <TextView
              name="searchText"
              showError={false}
              type="text"
              text={this.state.searchText}
              onFieldChanged={this.handleFieldChanged('searchText')}
            />
            <i className="fa fa-search" />
          </div>
          <div className="displayDropdown">
            <Select
              value={this.state.selectedOption}
              onChange={this.changePageLimit}
              options={this.state.options}
              isSearchable={false}
            />
            <span className="df df-line-bar" />
          </div>
        </div>

        <table className="table">
          <thead>{theadMarkup}</thead>
          <tbody>{tbodyMarkup}</tbody>
        </table>
        {paginationPanel && (
          <div className="nextprev pull-right">
            <a
              href="#"
              className={`df df-prev ${leftButtonEnable ? 'active' : ''}`}
              onClick={this.handleMoveLeft}
            />
            <span className="active">{currentPage}</span>
            <span>/</span>
            <span className="totalCount">{totalPages}</span>
            <a
              href="#"
              className={`df df-next ${rightButtonEnable ? 'active' : ''}`}
              onClick={this.handleMoveRight}
            />
          </div>
        )}
      </div>
    );
  }
}
