/**
 * Created by sigma on 13/2/19.
 */
import React from 'react';

export default function Cell({content, header, className, child}) {
  const cellMarkup = header ? (
    <th className="cell cell-header">{content} {child}</th>
  ) : (
    <td className={`cell ${className ? content : ''}`}>{content}</td>
  );

  return cellMarkup;
}
