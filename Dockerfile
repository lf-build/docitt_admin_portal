FROM node:8.12.0 as builder
WORKDIR /app
#ADD /package.json /app/package.json //Not required
ADD . /app/
RUN npm install --silent
RUN npm install react-scripts@1.1.1 -g --silent
RUN npm run build

#RUN ls /app  //For troubleshooting
#RUN ls /app/build  //For troubleshooting

FROM buildpack-deps:jessie-curl
RUN apt-get update
RUN apt-get install -y nginx-extras
RUN apt-get install -y luarocks
RUN luarocks install lua-cjson
RUN luarocks install lua-resty-string
RUN luarocks install redis-lua

ADD lua /etc/nginx/lua
ADD nginx.conf /etc/nginx/nginx.conf
ADD mapping.conf /config/mapping.conf

WORKDIR /home/app
COPY --from=builder /app/build/ .
#COPY ./build/ . //For troubleshooting if we want direct post build


#FROM node:8.12.0-slim //Not required
#WORKDIR /home/app //Not required
#COPY --from=builder /app/ . //Not required
#EXPOSE 3000 //Not required
#ENTRYPOINT npm run build && npm run start:prod //Not required

EXPOSE 5000

ENTRYPOINT \
    sed -i 's~CONFIG_HOST~'${CONFIGURATION_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~SECURITY_HOST~'${SECURITY_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~THEME_HOST~'${THEME_HOST}'~g' /etc/nginx/nginx.conf && \
    nginx -g 'daemon off;'