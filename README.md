# Docitt Admin Portal

## Requirements

You need node.js and npm installation on your environment and you can use your appropriate editor.

### Installation

The following command should be able to run after the installation to check current node and npm version.
Node version should be more than v8.0.0 &
npm should be more than v6.1.0

    $node --version
    v8.10.0

    $npm --version
    6.8.0

#### Node Installation on Linux

To install node you will need to run the following command

    $sudo apt-get update
    $sudo apt-get upgrade
    $sudo apt-get -y install nodejs

#### Finally update the version of npm

One last step that’s a good practice is to update npm. There’s a default version that came with the version of Node you just installed, but that version is commonly slightly behind the latest and greatest version of npm.

To get the most up-to-date npm, you can run the command:

    $sudo npm install npm --global

## Development

### Clone Repository
To Clone the Repository type the following in Terminal

    $git clone https://{bit-bucket-userName}@bitbucket.org/sisforce/docitt_admin_portal.git

Then change the Directory

    $cd docitt_admin_portal

### Install Project Dependancies
Before starting the project you need to install the dependencies. To install hit the following command in Terminal.

    $npm install

### Start the Development Environment
To start type the following command in Terminal

    $npm start

### Run on Browser
To run project on Browser visit to the following address

    http://localhost:3000